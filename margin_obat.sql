-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: Sep 20, 2021 at 04:13 PM
-- Server version: 8.0.13
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kasir_apotek_bunda_farma`
--

-- --------------------------------------------------------

--
-- Table structure for table `margin_obat`
--

CREATE TABLE `margin_obat` (
  `id_margin_obat` int(1) NOT NULL,
  `margin_upds` int(11) NOT NULL,
  `margin_resep` int(11) NOT NULL,
  `margin_relasi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `margin_obat`
--

INSERT INTO `margin_obat` (`id_margin_obat`, `margin_upds`, `margin_resep`, `margin_relasi`) VALUES
(1, 10, 10, 10);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `margin_obat`
--
ALTER TABLE `margin_obat`
  ADD PRIMARY KEY (`id_margin_obat`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `margin_obat`
--
ALTER TABLE `margin_obat`
  MODIFY `id_margin_obat` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
