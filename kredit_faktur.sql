-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: Nov 18, 2021 at 07:53 PM
-- Server version: 8.0.13
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kasir_apotek_bunda_farma`
--

-- --------------------------------------------------------

--
-- Table structure for table `kredit_faktur`
--

CREATE TABLE `kredit_faktur` (
  `id_kredit_faktur` int(11) NOT NULL,
  `id_kredit` int(11) NOT NULL,
  `nomor_faktur` varchar(100) NOT NULL,
  `tanggal_faktur` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kredit_faktur`
--

INSERT INTO `kredit_faktur` (`id_kredit_faktur`, `id_kredit`, `nomor_faktur`, `tanggal_faktur`) VALUES
(1, 1, 'KRD-00000000001', '2021-11-18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kredit_faktur`
--
ALTER TABLE `kredit_faktur`
  ADD PRIMARY KEY (`id_kredit_faktur`),
  ADD KEY `id_kredit` (`id_kredit`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kredit_faktur`
--
ALTER TABLE `kredit_faktur`
  MODIFY `id_kredit_faktur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `kredit_faktur`
--
ALTER TABLE `kredit_faktur`
  ADD CONSTRAINT `kredit_faktur_ibfk_1` FOREIGN KEY (`id_kredit`) REFERENCES `kredit` (`id_kredit`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
