-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: Aug 21, 2022 at 08:46 AM
-- Server version: 8.0.19
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kasir_apotek_bunda_farma`
--

-- --------------------------------------------------------

--
-- Table structure for table `dokter`
--

CREATE TABLE `dokter` (
  `id_dokter` int NOT NULL,
  `nama_dokter` varchar(100) NOT NULL,
  `status_delete` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dokter`
--

INSERT INTO `dokter` (`id_dokter`, `nama_dokter`, `status_delete`) VALUES
(1, 'dr. OZ', 0),
(2, 'dr. James Andrews', 0);

-- --------------------------------------------------------

--
-- Table structure for table `golongan_obat`
--

CREATE TABLE `golongan_obat` (
  `id_golongan_obat` int NOT NULL,
  `nama_golongan` varchar(50) NOT NULL,
  `status_delete` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `golongan_obat`
--

INSERT INTO `golongan_obat` (`id_golongan_obat`, `nama_golongan`, `status_delete`) VALUES
(1, 'Obat Bebas', 0),
(3, 'Obat Keras', 0);

-- --------------------------------------------------------

--
-- Table structure for table `jam_shift`
--

CREATE TABLE `jam_shift` (
  `id_jam_shift` int NOT NULL,
  `jam_awal` time NOT NULL,
  `jam_akhir` time NOT NULL,
  `ket_shift` varchar(25) NOT NULL,
  `status_delete` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jam_shift`
--

INSERT INTO `jam_shift` (`id_jam_shift`, `jam_awal`, `jam_akhir`, `ket_shift`, `status_delete`) VALUES
(1, '08:00:00', '15:00:00', 'Jam Pagi', 0),
(2, '15:01:00', '22:00:00', 'Jam Sore', 0),
(5, '22:01:00', '07:59:00', 'Jam Malam', 0);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_obat`
--

CREATE TABLE `jenis_obat` (
  `id_jenis_obat` int NOT NULL,
  `nama_jenis_obat` varchar(100) NOT NULL,
  `status_delete` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_obat`
--

INSERT INTO `jenis_obat` (`id_jenis_obat`, `nama_jenis_obat`, `status_delete`) VALUES
(1, 'Tablet', 0),
(2, 'Sirup', 0),
(3, 'Strip', 0),
(5, 'Salep', 0),
(6, 'Testers', 1);

-- --------------------------------------------------------

--
-- Table structure for table `kartu_stok`
--

CREATE TABLE `kartu_stok` (
  `id_kartu_stok` int NOT NULL,
  `nomor_stok` varchar(100) NOT NULL,
  `tanggal_pakai` date NOT NULL,
  `layanan` varchar(20) NOT NULL,
  `id_obat` int NOT NULL,
  `beli` int NOT NULL,
  `jual` int NOT NULL,
  `retur_barang` int NOT NULL,
  `saldo` int NOT NULL,
  `keterangan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kartu_stok`
--

INSERT INTO `kartu_stok` (`id_kartu_stok`, `nomor_stok`, `tanggal_pakai`, `layanan`, `id_obat`, `beli`, `jual`, `retur_barang`, `saldo`, `keterangan`) VALUES
(1, 'OBA-00000000015', '2021-10-20', 'Beli', 4, 2, 0, 0, 94, 'Pembelian'),
(2, 'OBA-00000000016', '2021-10-20', 'Beli', 4, 2, 0, 0, 96, 'Pembelian'),
(3, 'OBA-00000000017', '2021-10-20', 'Beli', 4, 2, 0, 0, 98, 'Pembelian'),
(4, 'OBA-00000000018', '2021-10-20', 'Beli', 4, 2, 0, 0, 100, 'Pembelian'),
(5, '-', '2021-10-22', 'UPDS', 4, 0, 10, 0, 60, 'Penjualan Kredit'),
(6, '-', '2021-10-22', 'UPDS', 4, 0, 10, 0, 50, 'Penjualan Kredit'),
(7, '-', '2021-10-22', 'UPDS', 7, 0, 10, 0, 250, 'Penjualan Kredit'),
(8, '-', '2021-10-22', 'UPDS', 7, 0, 10, 0, 240, 'Penjualan Kredit'),
(9, '-', '2021-10-22', 'UPDS', 7, 0, 10, 0, 230, 'Penjualan Kredit'),
(10, '-', '2021-10-22', 'UPDS', 7, 0, 10, 0, 220, 'Penjualan Kredit'),
(11, '-', '2021-10-22', 'UPDS', 4, 0, 10, 0, 40, 'Penjualan Kredit'),
(12, '-', '2021-10-22', 'UPDS', 7, 0, 10, 0, 210, 'Penjualan Kredit'),
(13, '1634888347', '2021-10-22', 'UPDS', 4, 0, 4, 0, 28, 'Penjualan'),
(14, '1634888399', '2021-10-22', 'UPDS', 4, 0, 4, 0, 24, 'Penjualan'),
(15, '-', '2021-10-22', 'UPDS', 4, 0, 4, 0, 20, 'Penjualan Kredit'),
(16, '-', '2021-10-22', 'UPDS', 7, 0, 10, 0, 200, 'Penjualan Kredit'),
(17, '1634888472', '2021-10-22', 'UPDS', 4, 0, 10, 0, 20, 'Penjualan'),
(18, '1634888472', '2021-10-22', 'UPDS', 4, 0, 10, 0, 20, 'Penjualan'),
(19, '1634888560', '2021-10-22', 'UPDS', 4, 0, 4, 0, 16, 'Penjualan'),
(20, '1634888560', '2021-10-22', 'UPDS', 4, 0, 4, 0, 12, 'Penjualan'),
(21, '1635420751', '2021-10-28', 'Resep', 4, 0, 1, 0, 11, 'Penjualan'),
(22, '1635420751', '2021-10-28', 'Resep', 4, 0, 1, 0, 10, 'Penjualan'),
(23, '1635497483', '2021-10-29', 'Resep', 4, 0, 1, 0, 9, 'Penjualan'),
(24, '1635497556', '2021-10-29', 'Resep', 4, 0, 1, 0, 8, 'Penjualan'),
(25, '1635497577', '2021-10-29', 'Resep', 4, 0, 1, 0, 7, 'Penjualan'),
(26, '1635498877', '2021-10-29', 'Resep', 4, 0, 1, 0, 6, 'Penjualan'),
(27, '1635499159', '2021-10-29', 'Resep', 4, 0, 1, 0, 5, 'Penjualan'),
(28, '1635499208', '2021-10-29', 'Resep', 4, 0, 1, 0, 4, 'Penjualan'),
(29, '1635499214', '2021-10-29', 'Resep', 4, 0, 1, 0, 3, 'Penjualan'),
(30, '1635499224', '2021-10-29', 'Resep', 4, 0, 1, 0, 2, 'Penjualan'),
(31, '1635513813', '2021-10-29', 'Resep', 7, 0, 1, 0, 199, 'Penjualan'),
(32, '1635513813', '2021-10-29', 'Resep', 40, 0, 1, 0, 799, 'Penjualan'),
(33, '1635513886', '2021-10-29', 'Resep', 7, 0, 1, 0, 198, 'Penjualan'),
(34, '1635513886', '2021-10-29', 'Resep', 40, 0, 1, 0, 798, 'Penjualan'),
(35, '1635513972', '2021-10-29', 'Resep', 7, 0, 1, 0, 197, 'Penjualan'),
(36, '1635513972', '2021-10-29', 'Resep', 40, 0, 1, 0, 797, 'Penjualan'),
(37, 'OBA-00000000019', '2021-11-01', 'Beli', 4, 1, 0, 0, 3, 'Pembelian'),
(38, 'KRD-00000000001', '2021-11-18', 'UPDS', 4, 0, 1, 0, 2, 'Penjualan Kredit'),
(39, 'OBA-00000000021', '2021-10-11', 'Beli', 4, 1, 0, 0, 3, 'Pembelian'),
(40, 'KRD-00000000002', '2021-12-14', 'UPDS', 5, 0, 10, 0, 12, 'Penjualan Kredit'),
(41, 'OBA-00000000022', '2021-12-16', 'Beli', 4, 10, 0, 0, 13, 'Pembelian'),
(42, 'KRD-00000000003', '2021-12-16', 'UPDS', 4, 0, 10, 0, 3, 'Penjualan Kredit'),
(43, 'KRD-00000000004', '2021-12-16', 'UPDS', 5, 0, 10, 0, 2, 'Penjualan Kredit'),
(44, 'KRD-00000000005', '2021-12-16', 'UPDS', 7, 0, 10, 0, 187, 'Penjualan Kredit'),
(45, 'KRD-00000000005', '2021-12-16', 'UPDS', 24, 0, 10, 0, 163, 'Penjualan Kredit'),
(46, 'KRD-00000000005', '2021-12-16', 'UPDS', 40, 0, 10, 0, 787, 'Penjualan Kredit'),
(47, 'KRD-00000000006', '2021-12-16', 'UPDS', 7, 0, 10, 0, 177, 'Penjualan Kredit'),
(48, 'KRD-00000000006', '2021-12-16', 'UPDS', 24, 0, 10, 0, 153, 'Penjualan Kredit'),
(49, 'KRD-00000000006', '2021-12-16', 'UPDS', 40, 0, 10, 0, 777, 'Penjualan Kredit'),
(50, '1639657846', '2021-12-16', 'UPDS', 7, 0, 10, 0, 167, 'Penjualan'),
(51, '1639657920', '2021-12-16', 'UPDS', 7, 0, 10, 0, 157, 'Penjualan'),
(52, '1639658033', '2021-12-16', 'UPDS', 7, 0, 10, 0, 147, 'Penjualan'),
(53, '1639658068', '2021-12-16', 'UPDS', 7, 0, 10, 0, 137, 'Penjualan'),
(54, '1639658068', '2021-12-16', 'UPDS', 9, 0, 5, 0, 32, 'Penjualan'),
(55, 'OBA-00000000023', '2021-12-17', 'Beli', 4, 10, 0, 0, 13, 'Pembelian'),
(56, 'OBA-00000000024', '2021-12-17', 'Beli', 4, 10, 0, 0, 23, 'Pembelian'),
(57, 'OBA-00000000025', '2022-03-30', 'Beli', 4, 1, 0, 0, 24, 'Pembelian'),
(58, 'OBA-00000000026', '2022-03-31', 'Beli', 4, 1, 0, 0, 25, 'Pembelian'),
(59, 'OBA-00000000027', '2022-04-04', 'Beli', 4, 10, 0, 0, 35, 'Pembelian'),
(60, '1649081711', '2022-04-04', 'UPDS', 4, 0, 10, 0, 25, 'Penjualan'),
(61, '1649081947', '2022-04-04', 'UPDS', 4, 0, 10, 0, 15, 'Penjualan'),
(62, '1649082389', '2022-04-04', 'UPDS', 4, 0, 10, 0, 5, 'Penjualan'),
(63, 'OBA-00000000028', '2022-04-06', 'Beli', 4, 10, 0, 0, 15, 'Pembelian'),
(64, 'OBA-00000000029', '2022-04-06', 'Beli', 4, 10, 0, 0, 25, 'Pembelian'),
(65, 'OBA-00000000030', '2022-04-06', 'Beli', 4, 10, 0, 0, 35, 'Pembelian'),
(66, '1649433477', '2022-04-08', 'Resep', 4, 0, 2, 0, 33, 'Penjualan'),
(67, '1649433477', '2022-04-08', 'Resep', 7, 0, 2, 0, 135, 'Penjualan'),
(68, '1649433477', '2022-04-08', 'Resep', 9, 0, 2, 0, 30, 'Penjualan'),
(69, '1649433669', '2022-04-09', 'Resep', 4, 0, 2, 0, 31, 'Penjualan'),
(70, '1649433669', '2022-04-09', 'Resep', 7, 0, 2, 0, 133, 'Penjualan'),
(71, '1649433669', '2022-04-09', 'Resep', 9, 0, 2, 0, 28, 'Penjualan'),
(72, '1649433725', '2022-04-09', 'Resep', 4, 0, 2, 0, 29, 'Penjualan'),
(73, '1649433725', '2022-04-09', 'Resep', 7, 0, 2, 0, 131, 'Penjualan'),
(74, '1649433725', '2022-04-09', 'Resep', 9, 0, 2, 0, 26, 'Penjualan'),
(75, '1649433735', '2022-04-09', 'Resep', 4, 0, 2, 0, 27, 'Penjualan'),
(76, '1649433735', '2022-04-09', 'Resep', 7, 0, 2, 0, 129, 'Penjualan'),
(77, '1649433735', '2022-04-09', 'Resep', 9, 0, 2, 0, 24, 'Penjualan'),
(78, '1649433995', '2022-04-09', 'Resep', 4, 0, 2, 0, 25, 'Penjualan'),
(79, '1649433995', '2022-04-09', 'Resep', 7, 0, 2, 0, 127, 'Penjualan'),
(80, '1649433995', '2022-04-09', 'Resep', 9, 0, 2, 0, 22, 'Penjualan'),
(81, '1649434095', '2022-04-09', 'Resep', 4, 0, 2, 0, 23, 'Penjualan'),
(82, '1649434095', '2022-04-09', 'Resep', 7, 0, 2, 0, 125, 'Penjualan'),
(83, '1649434095', '2022-04-09', 'Resep', 9, 0, 2, 0, 20, 'Penjualan'),
(84, '1649434253', '2022-04-09', 'Resep', 4, 0, 2, 0, 21, 'Penjualan'),
(85, '1649434253', '2022-04-09', 'Resep', 7, 0, 2, 0, 123, 'Penjualan'),
(86, '1649434253', '2022-04-09', 'Resep', 9, 0, 2, 0, 18, 'Penjualan'),
(87, '1649754163', '2022-04-12', 'Resep', 4, 0, 2, 0, 19, 'Penjualan'),
(88, '1649754163', '2022-04-12', 'Resep', 7, 0, 2, 0, 121, 'Penjualan'),
(89, '1649754364', '2022-04-12', 'Resep', 4, 0, 2, 0, 17, 'Penjualan'),
(90, '1649754364', '2022-04-12', 'Resep', 7, 0, 2, 0, 119, 'Penjualan'),
(91, '1649754394', '2022-04-12', 'Resep', 4, 0, 2, 0, 15, 'Penjualan'),
(92, '1649754394', '2022-04-12', 'Resep', 7, 0, 2, 0, 117, 'Penjualan'),
(93, '1649754409', '2022-04-12', 'Resep', 4, 0, 2, 0, 13, 'Penjualan'),
(94, '1649754409', '2022-04-12', 'Resep', 7, 0, 2, 0, 115, 'Penjualan'),
(95, '1649754626', '2022-04-12', 'Resep', 4, 0, 2, 0, 11, 'Penjualan'),
(96, '1649754626', '2022-04-12', 'Resep', 9, 0, 2, 0, 16, 'Penjualan'),
(97, '1649754626', '2022-04-12', 'Resep', 4, 0, 1, 0, 10, 'Penjualan'),
(98, '1649754626', '2022-04-12', 'Resep', 7, 0, 5, 0, 110, 'Penjualan'),
(99, '1649754909', '2022-04-12', 'Resep', 4, 0, 2, 0, 8, 'Penjualan'),
(100, '1649754909', '2022-04-12', 'Resep', 9, 0, 2, 0, 14, 'Penjualan'),
(101, '1649754909', '2022-04-12', 'Resep', 4, 0, 1, 0, 7, 'Penjualan'),
(102, '1649754909', '2022-04-12', 'Resep', 7, 0, 5, 0, 105, 'Penjualan'),
(103, '1649758419', '2022-04-12', 'Resep', 4, 0, 1, 0, 6, 'Penjualan'),
(104, '1649758419', '2022-04-12', 'Resep', 7, 0, 1, 0, 104, 'Penjualan'),
(105, '1649761430', '2022-04-12', 'Resep', 4, 0, 1, 0, 5, 'Penjualan'),
(106, '1649761430', '2022-04-12', 'Resep', 7, 0, 1, 0, 103, 'Penjualan'),
(107, '1649761430', '2022-04-12', 'Resep', 9, 0, 1, 0, 13, 'Penjualan'),
(108, '1649761430', '2022-04-12', 'Resep', 18, 0, 1, 0, 39, 'Penjualan'),
(109, '1649761508', '2022-04-12', 'Resep', 4, 0, 1, 0, 4, 'Penjualan'),
(110, '1649761508', '2022-04-12', 'Resep', 7, 0, 1, 0, 102, 'Penjualan'),
(111, '1649761508', '2022-04-12', 'Resep', 9, 0, 1, 0, 12, 'Penjualan'),
(112, '1649761508', '2022-04-12', 'Resep', 18, 0, 1, 0, 38, 'Penjualan'),
(113, '1649761644', '2022-04-12', 'Resep', 4, 0, 1, 0, 3, 'Penjualan'),
(114, '1649761644', '2022-04-12', 'Resep', 7, 0, 1, 0, 101, 'Penjualan'),
(115, '1649761644', '2022-04-12', 'Resep', 9, 0, 1, 0, 11, 'Penjualan'),
(116, '1649761644', '2022-04-12', 'Resep', 18, 0, 1, 0, 37, 'Penjualan'),
(117, '1649761671', '2022-04-12', 'Resep', 4, 0, 1, 0, 2, 'Penjualan'),
(118, '1649761671', '2022-04-12', 'Resep', 7, 0, 1, 0, 100, 'Penjualan'),
(119, '1649761671', '2022-04-12', 'Resep', 9, 0, 1, 0, 10, 'Penjualan'),
(120, '1649761671', '2022-04-12', 'Resep', 18, 0, 1, 0, 36, 'Penjualan'),
(121, '1649761693', '2022-04-12', 'Resep', 4, 0, 1, 0, 1, 'Penjualan'),
(122, '1649761693', '2022-04-12', 'Resep', 7, 0, 1, 0, 99, 'Penjualan'),
(123, '1649761693', '2022-04-12', 'Resep', 9, 0, 1, 0, 9, 'Penjualan'),
(124, '1649761693', '2022-04-12', 'Resep', 18, 0, 1, 0, 35, 'Penjualan'),
(125, '1649761875', '2022-04-12', 'Resep', 4, 0, 1, 0, 0, 'Penjualan'),
(126, '1649761875', '2022-04-12', 'Resep', 7, 0, 1, 0, 98, 'Penjualan'),
(127, '1649761875', '2022-04-12', 'Resep', 9, 0, 1, 0, 8, 'Penjualan'),
(128, '1649761875', '2022-04-12', 'Resep', 18, 0, 1, 0, 34, 'Penjualan'),
(129, '1649761942', '2022-04-12', 'Resep', 4, 0, 1, 0, -1, 'Penjualan'),
(130, '1649761942', '2022-04-12', 'Resep', 7, 0, 1, 0, 97, 'Penjualan'),
(131, '1649761942', '2022-04-12', 'Resep', 9, 0, 1, 0, 7, 'Penjualan'),
(132, '1649761942', '2022-04-12', 'Resep', 18, 0, 1, 0, 33, 'Penjualan'),
(133, 'OBA-00000000031', '2022-04-13', 'Beli', 4, 5, 0, 0, 4, 'Pembelian'),
(134, 'OBA-00000000031', '2022-04-13', 'Beli', 4, 5, 0, 0, 9, 'Pembelian'),
(135, 'OBA-00000000031', '2022-04-13', 'Beli', 41, 5, 0, 0, 95, 'Pembelian'),
(136, 'OBA-00000000032', '2022-04-13', 'Beli', 4, 5, 0, 0, 14, 'Pembelian'),
(137, 'OBA-00000000032', '2022-04-13', 'Beli', 4, 5, 0, 0, 19, 'Pembelian'),
(138, 'OBA-00000000032', '2022-04-13', 'Beli', 41, 5, 0, 0, 100, 'Pembelian'),
(139, 'OBA-00000000033', '2022-04-13', 'Beli', 4, 5, 0, 0, 24, 'Pembelian'),
(140, 'OBA-00000000033', '2022-04-13', 'Beli', 4, 5, 0, 0, 29, 'Pembelian'),
(141, 'OBA-00000000033', '2022-04-13', 'Beli', 41, 5, 0, 0, 105, 'Pembelian'),
(142, 'OBA-00000000034', '2022-04-13', 'Beli', 4, 5, 0, 0, 34, 'Pembelian'),
(143, 'OBA-00000000034', '2022-04-13', 'Beli', 4, 5, 0, 0, 39, 'Pembelian'),
(144, 'OBA-00000000034', '2022-04-13', 'Beli', 41, 5, 0, 0, 110, 'Pembelian'),
(145, 'OBA-00000000035', '2022-04-13', 'Beli', 4, 5, 0, 0, 44, 'Pembelian'),
(146, 'OBA-00000000035', '2022-04-13', 'Beli', 4, 5, 0, 0, 49, 'Pembelian'),
(147, 'OBA-00000000035', '2022-04-13', 'Beli', 41, 5, 0, 0, 115, 'Pembelian'),
(148, 'OBA-00000000036', '2022-04-13', 'Beli', 4, 5, 0, 0, 54, 'Pembelian'),
(149, 'OBA-00000000036', '2022-04-13', 'Beli', 4, 5, 0, 0, 59, 'Pembelian'),
(150, 'OBA-00000000036', '2022-04-13', 'Beli', 41, 5, 0, 0, 120, 'Pembelian'),
(151, 'OBA-00000000037', '2022-04-13', 'Beli', 4, 5, 0, 0, 64, 'Pembelian'),
(152, 'OBA-00000000037', '2022-04-13', 'Beli', 4, 5, 0, 0, 69, 'Pembelian'),
(153, 'OBA-00000000037', '2022-04-13', 'Beli', 41, 5, 0, 0, 125, 'Pembelian'),
(154, 'KRD-00000000007', '2022-04-18', 'UPDS', 4, 0, 10, 0, 59, 'Penjualan Kredit'),
(155, 'KRD-00000000008', '2022-04-19', 'UPDS', 43, 0, 10, 0, 90, 'Penjualan Kredit'),
(156, 'OBA-00000000038', '2022-04-19', 'Beli', 4, 10, 0, 0, 69, 'Pembelian'),
(157, '220530001', '2022-05-28', 'Resep', 4, 0, 10, 0, 59, 'Penjualan'),
(158, '220530001', '2022-05-28', 'Resep', 7, 0, 10, 0, 87, 'Penjualan'),
(159, '220530001', '2022-05-28', 'Resep', 9, 0, 2, 0, 5, 'Penjualan'),
(160, '220530001', '2022-05-28', 'Resep', 18, 0, 10, 0, 23, 'Penjualan'),
(161, '1654026006', '2022-06-01', 'UPDS', 4, 0, 5, 0, 54, 'Penjualan'),
(162, '220620001', '2022-06-01', 'UPDS', 4, 0, 5, 0, 49, 'Penjualan'),
(163, '220620002', '2022-06-01', 'UPDS', 4, 0, 5, 0, 44, 'Penjualan'),
(164, '220630001', '2022-06-02', 'Resep', 4, 0, 10, 0, 34, 'Penjualan'),
(165, '220630002', '2022-06-02', 'Resep', 4, 0, 10, 0, 24, 'Penjualan'),
(166, '220630003', '2022-06-02', 'Resep', 4, 0, 10, 0, 14, 'Penjualan'),
(167, '220630004', '2022-06-02', 'Resep', 4, 0, 10, 0, 4, 'Penjualan'),
(168, '220630005', '2022-06-02', 'Resep', 7, 0, 10, 0, 77, 'Penjualan'),
(169, '220620003', '2022-06-02', 'UPDS', 7, 0, 10, 0, 67, 'Penjualan Kredit'),
(170, '220610001', '2022-06-02', 'UPDS', 7, 0, 10, 0, 57, 'Penjualan Kredit'),
(171, '220630006', '2022-06-10', 'Resep', 7, 0, 10, 0, 47, 'Penjualan'),
(172, '220620003', '2022-06-10', 'UPDS', 7, 0, 10, 0, 37, 'Penjualan'),
(173, 'OBA-00000000001', '2022-06-10', 'Retur Barang', 7, 0, 0, 10, 47, 'Retur Barang'),
(174, '220620004', '2022-06-21', 'UPDS', 18, 0, 10, 0, 13, 'Penjualan'),
(175, '220620004', '2022-06-21', 'UPDS', 24, 0, 10, 0, 143, 'Penjualan'),
(176, '220620004', '2022-06-21', 'UPDS', 40, 0, 5, 0, 772, 'Penjualan'),
(177, 'OBA-00000000002', '2022-06-21', 'Retur Barang', 18, 0, 0, 5, 18, 'Retur Barang'),
(178, 'OBA-00000000002', '2022-06-21', 'Retur Barang', 24, 0, 0, 10, 153, 'Retur Barang'),
(179, 'OBA-00000000002', '2022-06-21', 'Retur Barang', 40, 0, 0, 5, 777, 'Retur Barang'),
(180, '220640001', '2022-06-21', 'Beli', 4, 5, 0, 0, 9, 'Pembelian'),
(181, '220640001', '2022-06-21', 'Beli', 9, 5, 0, 0, 10, 'Pembelian'),
(182, '220640001', '2022-06-21', 'Beli', 18, 5, 0, 0, 23, 'Pembelian'),
(183, '220640002', '2022-06-21', 'Beli', 4, 5, 0, 0, 14, 'Pembelian'),
(184, '220640002', '2022-06-21', 'Beli', 9, 5, 0, 0, 15, 'Pembelian'),
(185, '220640002', '2022-06-21', 'Beli', 18, 5, 0, 0, 28, 'Pembelian'),
(186, '220640003', '2022-06-21', 'Beli', 4, 5, 0, 0, 19, 'Pembelian'),
(187, '220640003', '2022-06-21', 'Beli', 9, 5, 0, 0, 20, 'Pembelian'),
(188, '220640003', '2022-06-21', 'Beli', 18, 5, 0, 0, 33, 'Pembelian'),
(189, '220640004', '2022-06-21', 'Beli', 4, 5, 0, 0, 24, 'Pembelian'),
(190, '220640004', '2022-06-21', 'Beli', 9, 5, 0, 0, 25, 'Pembelian'),
(191, '220640004', '2022-06-21', 'Beli', 18, 5, 0, 0, 38, 'Pembelian'),
(192, '220640005', '2022-06-21', 'Beli', 4, 5, 0, 0, 29, 'Pembelian'),
(193, '220640005', '2022-06-21', 'Beli', 9, 5, 0, 0, 30, 'Pembelian'),
(194, '220640005', '2022-06-21', 'Beli', 29, 5, 0, 0, 15, 'Pembelian'),
(195, '220640006', '2022-06-21', 'Beli', 4, 5, 0, 0, 34, 'Pembelian'),
(196, '220670001', '2022-06-22', 'Retur Barang', 18, 0, 0, 5, 43, 'Retur Barang'),
(197, '220660001', '2022-06-24', 'Beli', 5, 10, 0, 0, 12, 'Pembelian'),
(198, '220630007', '2022-06-24', 'Resep', 4, 0, 10, 0, 24, 'Penjualan'),
(199, '220630008', '2022-06-24', 'Resep', 4, 0, 10, 0, 14, 'Penjualan'),
(200, '220630009', '2022-06-24', 'Resep', 4, 0, 10, 0, 4, 'Penjualan'),
(201, '220630010', '2022-06-24', 'Resep', 4, 0, 10, 0, -6, 'Penjualan'),
(202, '220630011', '2022-06-24', 'Resep', 9, 0, 10, 0, 20, 'Penjualan'),
(203, '220630012', '2022-06-24', 'Resep', 18, 0, 10, 0, 33, 'Penjualan'),
(204, '220670002', '2022-06-25', 'Retur Barang', 18, 0, 0, 5, 38, 'Retur Barang'),
(205, '220670002', '2022-06-25', 'Retur Barang', 24, 0, 0, 5, 158, 'Retur Barang'),
(206, '220670002', '2022-06-25', 'Retur Barang', 40, 0, 0, 5, 782, 'Retur Barang'),
(207, '220670003', '2022-06-25', 'Retur Barang', 18, 0, 0, 5, 43, 'Retur Barang'),
(208, '220670003', '2022-06-25', 'Retur Barang', 24, 0, 0, 5, 163, 'Retur Barang'),
(209, '220670003', '2022-06-25', 'Retur Barang', 40, 0, 0, 5, 787, 'Retur Barang'),
(210, '220620005', '2022-06-25', 'UPDS', 5, 0, 10, 0, 2, 'Penjualan'),
(211, '220630013', '2022-06-25', 'Resep', 30, 0, 10, 0, 190, 'Penjualan'),
(212, '220630014', '2022-06-25', 'Resep', 30, 0, 10, 0, 180, 'Penjualan'),
(213, '220630015', '2022-06-25', 'Resep', 9, 0, 5, 0, 15, 'Penjualan'),
(214, '220630016', '2022-06-25', 'Resep', 30, 0, 10, 0, 170, 'Penjualan'),
(215, '220620006', '2022-06-27', 'UPDS', 9, 0, 2, 0, 13, 'Penjualan'),
(216, '220630017', '2022-06-27', 'Resep', 9, 0, 2, 0, 11, 'Penjualan'),
(217, '220670004', '2022-06-30', 'Retur Barang', 7, 0, 0, 5, 52, 'Retur Barang'),
(218, '220670005', '2022-06-30', 'Retur Barang', 7, 0, 0, 5, 57, 'Retur Barang'),
(219, '220730001', '2022-07-11', 'Resep', 5, 0, 2, 0, 0, 'Penjualan'),
(220, '220720001', '2022-07-11', 'UPDS', 7, 0, 10, 0, 47, 'Penjualan'),
(221, '220720001', '2022-07-11', 'UPDS', 24, 0, 10, 0, 153, 'Penjualan'),
(222, '220730002', '2022-07-11', 'Resep', 9, 0, 1, 0, 10, 'Penjualan'),
(223, '220730002', '2022-07-11', 'Resep', 18, 0, 1, 0, 42, 'Penjualan'),
(224, '220750001', '2022-07-21', 'Beli', 4, 10, 0, 0, 4, 'Pembelian'),
(225, '220750001', '2022-07-21', 'Beli', 5, 10, 0, 0, 10, 'Pembelian'),
(226, '220750002', '2022-07-21', 'Beli', 4, 10, 0, 0, 14, 'Pembelian'),
(227, '220750002', '2022-07-21', 'Beli', 5, 10, 0, 0, 20, 'Pembelian'),
(228, '220720002', '2022-07-22', 'UPDS', 4, 0, 10, 0, 4, 'Penjualan'),
(229, '220730003', '2022-07-22', 'Resep', 4, 0, 1, 0, 3, 'Penjualan'),
(230, '220730004', '2022-07-22', 'Resep', 4, 0, 1, 0, 2, 'Penjualan'),
(231, '220730005', '2022-07-22', 'Resep', 4, 0, 1, 0, 1, 'Penjualan'),
(232, '220730006', '2022-07-22', 'Resep', 4, 0, 1, 0, 0, 'Penjualan'),
(233, '220730007', '2022-07-22', 'Resep', 7, 0, 10, 0, 37, 'Penjualan'),
(234, '220730008', '2022-07-22', 'Resep', 7, 0, 10, 0, 27, 'Penjualan'),
(235, '220730009', '2022-07-22', 'Resep', 7, 0, 10, 0, 17, 'Penjualan'),
(236, '220730010', '2022-07-22', 'Resep', 7, 0, 10, 0, 7, 'Penjualan'),
(237, '220730011', '2022-07-22', 'Resep', 7, 0, 10, 0, -3, 'Penjualan'),
(238, '220730012', '2022-07-22', 'Resep', 7, 0, 10, 0, -13, 'Penjualan'),
(239, '220730013', '2022-07-22', 'Resep', 7, 0, 10, 0, -23, 'Penjualan'),
(240, '220730014', '2022-07-23', 'Resep', 18, 0, 10, 0, 32, 'Penjualan'),
(241, '220730015', '2022-07-23', 'Resep', 9, 0, 10, 0, 0, 'Penjualan'),
(242, '220730015', '2022-07-23', 'Resep', 18, 0, 10, 0, 22, 'Penjualan'),
(243, '220730015', '2022-07-23', 'Resep', 39, 0, 10, 0, 87, 'Penjualan'),
(244, '220760001', '2022-07-23', 'Beli', 4, 10, 0, 0, 10, 'Pembelian'),
(245, '220720003', '2022-07-23', 'UPDS', 4, 0, 5, 0, 5, 'Penjualan'),
(246, '220750003', '2022-07-24', 'Beli', 4, 10, 0, 0, 15, 'Pembelian'),
(247, '220750004', '2022-07-24', 'Beli', 4, 10, 0, 0, 25, 'Pembelian'),
(248, '220750005', '2022-07-24', 'Beli', 4, 10, 0, 0, 35, 'Pembelian'),
(249, '220710001', '2022-07-25', 'UPDS', 4, 0, 5, 0, 30, 'Penjualan Kredit'),
(250, '220770001', '2022-07-26', 'Retur Barang', 4, 0, 0, 5, 35, 'Retur Barang'),
(251, '220820001', '2022-08-01', 'UPDS', 4, 0, 1, 0, 34, 'Penjualan'),
(252, '220820002', '2022-08-01', 'UPDS', 4, 0, 1, 0, 33, 'Penjualan'),
(253, '220820003', '2022-08-01', 'UPDS', 4, 0, 1, 0, 32, 'Penjualan'),
(254, '220820004', '2022-08-01', 'UPDS', 18, 0, 1, 0, 21, 'Penjualan'),
(255, '220822084', '2022-08-01', 'UPDS', 18, 0, 1, 0, 20, 'Penjualan'),
(256, '220822085', '2022-08-01', 'UPDS', 18, 0, 1, 0, 19, 'Penjualan'),
(257, '220822086', '2022-08-01', 'UPDS', 4, 0, 1, 0, 31, 'Penjualan'),
(258, '220822087', '2022-08-01', 'UPDS', 4, 0, 1, 0, 30, 'Penjualan'),
(259, '220820001', '2022-08-01', 'UPDS', 4, 0, 1, 0, 29, 'Penjualan'),
(260, '220820002', '2022-08-01', 'UPDS', 4, 0, 1, 0, 28, 'Penjualan'),
(261, '220820001', '2022-08-01', 'UPDS', 4, 0, 1, 0, 27, 'Penjualan'),
(262, '220820002', '2022-08-01', 'UPDS', 4, 0, 1, 0, 26, 'Penjualan'),
(263, '220870001', '2022-08-18', 'Retur Barang', 4, 0, 0, 5, 31, 'Retur Barang'),
(264, '220870002', '2022-08-18', 'Retur Barang', 18, 0, 0, 10, 29, 'Retur Barang'),
(265, '220820003', '2022-08-18', 'UPDS', 4, 0, 10, 0, 21, 'Penjualan'),
(266, '220820004', '2022-08-18', 'UPDS', 4, 0, 1, 0, 20, 'Penjualan'),
(267, '220820005', '2022-08-18', 'UPDS', 4, 0, 1, 0, 19, 'Penjualan'),
(268, '220820006', '2022-08-18', 'UPDS', 4, 0, 1, 0, 18, 'Penjualan'),
(269, '220820007', '2022-08-18', 'UPDS', 4, 0, 3, 0, 15, 'Penjualan'),
(270, '220820008', '2022-08-18', 'UPDS', 4, 0, 3, 0, 12, 'Penjualan'),
(271, '220820009', '2022-08-18', 'UPDS', 4, 0, 3, 0, 9, 'Penjualan'),
(272, '220820010', '2022-08-18', 'UPDS', 4, 0, 3, 0, 6, 'Penjualan'),
(273, '220820011', '2022-08-18', 'UPDS', 5, 0, 3, 0, 17, 'Penjualan'),
(274, '220820012', '2022-08-18', 'UPDS', 5, 0, 3, 0, 14, 'Penjualan'),
(275, '220820013', '2022-08-18', 'UPDS', 5, 0, 3, 0, 11, 'Penjualan'),
(276, '220820014', '2022-08-18', 'UPDS', 4, 0, 3, 0, 3, 'Penjualan'),
(277, '220820015', '2022-08-18', 'UPDS', 18, 0, 2, 0, 27, 'Penjualan'),
(278, '220820016', '2022-08-18', 'UPDS', 18, 0, 2, 0, 25, 'Penjualan'),
(279, '220830006', '2022-08-21', 'Resep', 30, 0, 1, 0, 169, 'Penjualan'),
(280, '220830006', '2022-08-21', 'Resep', 39, 0, 1, 0, 86, 'Penjualan'),
(281, '220830007', '2022-08-21', 'Resep', 4, 0, 1, 0, 2, 'Penjualan'),
(282, '220830007', '2022-08-21', 'Resep', 18, 0, 1, 0, 24, 'Penjualan'),
(283, '220830007', '2022-08-21', 'Resep', 30, 0, 1, 0, 168, 'Penjualan'),
(284, '220830007', '2022-08-21', 'Resep', 41, 0, 1, 0, 124, 'Penjualan'),
(285, '220830007', '2022-08-21', 'Resep', 41, 0, 1, 0, 123, 'Penjualan'),
(286, '220830008', '2022-08-21', 'Resep', 4, 0, 1, 0, 1, 'Penjualan'),
(287, '220830008', '2022-08-21', 'Resep', 24, 0, 1, 0, 152, 'Penjualan'),
(288, '220830008', '2022-08-21', 'Resep', 40, 0, 1, 0, 786, 'Penjualan'),
(289, '220830008', '2022-08-21', 'Resep', 18, 0, 1, 0, 23, 'Penjualan'),
(290, '220830008', '2022-08-21', 'Resep', 29, 0, 1, 0, 14, 'Penjualan');

-- --------------------------------------------------------

--
-- Table structure for table `komposisi_obat`
--

CREATE TABLE `komposisi_obat` (
  `id_komposisi_obat` int NOT NULL,
  `id_obat` int NOT NULL,
  `nama_komposisi` varchar(70) NOT NULL,
  `takaran_komposisi` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `komposisi_obat`
--

INSERT INTO `komposisi_obat` (`id_komposisi_obat`, `id_obat`, `nama_komposisi`, `takaran_komposisi`) VALUES
(10, 30, 'OBH Combi', '-'),
(11, 29, '-', '-'),
(12, 24, '-', '-'),
(19, 32, '-', '-'),
(25, 39, 'Ivermectin', '12 mg'),
(29, 9, '-', '-'),
(30, 7, '-', '-'),
(31, 5, '-', '-'),
(34, 40, 'Paracetamol', '500 mg'),
(37, 42, '-', '-'),
(42, 44, '-', '-'),
(45, 45, '-', '-'),
(46, 45, 'oke', 'oke'),
(55, 46, '-', '-'),
(56, 4, '-', '-'),
(57, 41, 'Paracetamol', '600 mg'),
(59, 43, '-', '-'),
(60, 18, '-', '-');

-- --------------------------------------------------------

--
-- Table structure for table `kredit`
--

CREATE TABLE `kredit` (
  `id_kredit` int NOT NULL,
  `nama_pelanggan` varchar(100) NOT NULL,
  `nomor_telepon` varchar(12) NOT NULL,
  `alamat_pelanggan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kredit`
--

INSERT INTO `kredit` (`id_kredit`, `nama_pelanggan`, `nomor_telepon`, `alamat_pelanggan`) VALUES
(1, 'dr. James Andrew', '0888888888', ''),
(8, 'Ujang', '08855555', ''),
(10, 'Galuh', '08888899', 'Jl. Pasuruan'),
(11, 'Jajang Sutarman', '0888889999', 'Jl. Jajang'),
(12, 'Makhmud', '0888889999', 'Jl. Amukan Masa'),
(13, 'Okeh', '088888', '-');

-- --------------------------------------------------------

--
-- Table structure for table `kredit_det`
--

CREATE TABLE `kredit_det` (
  `id_kredit_det` int NOT NULL,
  `id_kredit_faktur` int NOT NULL,
  `id_obat` int NOT NULL,
  `id_supplier` int NOT NULL,
  `banyak_obat` int NOT NULL,
  `jenis_diskon` varchar(10) NOT NULL,
  `diskon` double NOT NULL,
  `sub_total` double NOT NULL,
  `tanggal_jatuh_tempo` date NOT NULL,
  `status_kredit` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kredit_det`
--

INSERT INTO `kredit_det` (`id_kredit_det`, `id_kredit_faktur`, `id_obat`, `id_supplier`, `banyak_obat`, `jenis_diskon`, `diskon`, `sub_total`, `tanggal_jatuh_tempo`, `status_kredit`, `created_at`, `updated_at`) VALUES
(1, 1, 4, 2, 0, 'persen', 0, 0, '2021-11-21', 1, '2021-11-18 21:44:43', '2021-11-19 00:16:15'),
(2, 2, 5, 2, 10, 'persen', 0, 24200, '2021-12-17', 0, '2021-12-14 13:45:39', '2021-12-14 13:45:39'),
(3, 3, 4, 4, 0, 'persen', 0, 0, '2021-12-19', 1, '2021-12-16 18:02:50', '2021-12-16 20:43:09'),
(4, 4, 5, 4, 10, 'persen', 0, 25000, '2021-12-19', 1, '2021-12-16 18:03:29', '2022-04-04 22:31:25'),
(5, 5, 7, 2, 10, 'persen', 0, 25000, '2021-12-19', 0, '2021-12-16 20:24:47', '2021-12-16 20:24:47'),
(6, 6, 24, 4, 10, 'persen', 0, 242000, '2021-12-19', 0, '2021-12-16 20:24:47', '2021-12-16 20:24:47'),
(7, 7, 40, 2, 0, 'persen', 0, 0, '2021-12-19', 1, '2021-12-16 20:24:47', '2021-12-16 21:15:52'),
(8, 8, 7, 2, 10, 'persen', 0, 25000, '2021-12-19', 0, '2021-12-16 20:28:15', '2021-12-16 20:28:15'),
(9, 8, 24, 4, 10, 'persen', 0, 242000, '2021-12-19', 0, '2021-12-16 20:28:15', '2021-12-16 20:28:15'),
(10, 8, 40, 2, 10, 'persen', 0, 49000, '2021-12-19', 0, '2021-12-16 20:28:15', '2021-12-16 20:28:15'),
(11, 9, 4, 4, 10, 'rupiah', 15598, 140382, '2022-04-21', 1, '2022-04-18 23:24:39', '2022-06-25 14:03:55'),
(12, 10, 43, 2, 10, 'rupiah', 0, 41518.44, '2022-04-22', 0, '2022-04-19 14:10:51', '2022-04-19 14:10:51'),
(13, 11, 7, 2, 10, 'rupiah', 0, 25000, '2022-06-05', 0, '2022-06-02 20:27:50', '2022-06-02 20:27:50'),
(14, 12, 7, 4, 10, 'rupiah', 0, 26640, '2022-06-05', 0, '2022-06-02 20:28:37', '2022-06-02 20:28:37'),
(15, 13, 4, 4, 5, 'rupiah', 0, 85080, '2022-07-28', 0, '2022-07-25 12:20:50', '2022-07-25 12:20:50');

--
-- Triggers `kredit_det`
--
DELIMITER $$
CREATE TRIGGER `kredit_event` AFTER INSERT ON `kredit_det` FOR EACH ROW BEGIN
    	UPDATE obat SET stok_obat = stok_obat - NEW.banyak_obat WHERE id_obat = NEW.id_obat;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `kredit_faktur`
--

CREATE TABLE `kredit_faktur` (
  `id_kredit_faktur` int NOT NULL,
  `id_kredit` int NOT NULL,
  `nomor_faktur` varchar(100) NOT NULL,
  `tanggal_faktur` date NOT NULL,
  `jam_transaksi` time NOT NULL,
  `id_jam_shift` int NOT NULL,
  `id_users` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kredit_faktur`
--

INSERT INTO `kredit_faktur` (`id_kredit_faktur`, `id_kredit`, `nomor_faktur`, `tanggal_faktur`, `jam_transaksi`, `id_jam_shift`, `id_users`) VALUES
(1, 1, 'KRD-00000000001', '2021-11-18', '00:00:00', 1, 1),
(2, 1, 'KRD-00000000002', '2021-12-14', '00:00:00', 1, 1),
(3, 13, 'KRD-00000000003', '2021-12-16', '00:00:00', 1, 1),
(4, 13, 'KRD-00000000004', '2021-12-16', '00:00:00', 1, 1),
(5, 1, 'KRD-00000000005', '2021-12-16', '00:00:00', 1, 1),
(6, 1, 'KRD-00000000005', '2021-12-16', '00:00:00', 1, 1),
(7, 1, 'KRD-00000000005', '2021-12-16', '00:00:00', 1, 1),
(8, 1, 'KRD-00000000006', '2021-12-16', '00:00:00', 1, 1),
(9, 8, 'KRD-00000000007', '2022-04-18', '00:00:00', 5, 1),
(10, 8, 'KRD-00000000008', '2022-04-19', '00:00:00', 1, 1),
(11, 1, '220620003', '2022-06-02', '00:00:00', 2, 1),
(12, 1, '220610001', '2022-06-02', '00:00:00', 2, 1),
(13, 8, '220710001', '2022-07-25', '12:20:50', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `margin_obat`
--

CREATE TABLE `margin_obat` (
  `id_margin_obat` int NOT NULL,
  `margin_upds` int NOT NULL,
  `margin_resep` int NOT NULL,
  `margin_relasi` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `margin_obat`
--

INSERT INTO `margin_obat` (`id_margin_obat`, `margin_upds`, `margin_resep`, `margin_relasi`) VALUES
(1, 10, 10, 20);

-- --------------------------------------------------------

--
-- Table structure for table `menu_user`
--

CREATE TABLE `menu_user` (
  `id_menu_user` int NOT NULL,
  `id_users` int NOT NULL,
  `menu_child` varchar(70) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `menu_parent` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu_user`
--

INSERT INTO `menu_user` (`id_menu_user`, `id_users`, `menu_child`, `menu_parent`) VALUES
(110, 15, 'jam-shift', 'data-master'),
(111, 15, 'data-pasien', 'data-master'),
(112, 15, 'data-dokter', 'data-master'),
(113, 15, 'data-ppn', 'data-master'),
(114, 15, 'data-obat', 'obat'),
(115, 15, 'margin-obat', 'obat'),
(116, 15, 'data-supplier-obat', 'obat'),
(117, 15, 'data-pabrik-obat', 'obat'),
(118, 15, 'data-jenis-obat', 'obat'),
(119, 15, 'data-golongan-obat', 'obat'),
(120, 15, 'kartu-stok', 'pembelian'),
(121, 15, 'history-beli', 'pembelian'),
(122, 15, 'penjualan', 'penjualan'),
(123, 15, 'racik-obat', 'penjualan'),
(124, 15, 'penjualan-relasi', 'penjualan'),
(125, 15, 'data-penjualan', 'penjualan'),
(126, 15, 'data-penjualan-racik-obat', 'penjualan'),
(127, 15, 'data-kredit', 'penjualan'),
(128, 15, 'retur-barang', 'penjualan'),
(129, 15, 'laporan-data', '-');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `obat`
--

CREATE TABLE `obat` (
  `id_obat` int NOT NULL,
  `kode_obat` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_jenis_obat` int NOT NULL,
  `nama_obat` varchar(70) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_golongan_obat` int NOT NULL,
  `id_pabrik_obat` int NOT NULL,
  `stok_obat` int NOT NULL,
  `satuan_obat` varchar(70) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `dosis_satuan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tanggal_expired` date NOT NULL,
  `harga_modal` double NOT NULL,
  `harga_modal_ppn` double NOT NULL,
  `hja_upds` double NOT NULL,
  `hja_resep` double NOT NULL,
  `hja_relasi` double NOT NULL,
  `tanggal_input` date NOT NULL,
  `status_delete` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `obat`
--

INSERT INTO `obat` (`id_obat`, `kode_obat`, `id_jenis_obat`, `nama_obat`, `id_golongan_obat`, `id_pabrik_obat`, `stok_obat`, `satuan_obat`, `dosis_satuan`, `tanggal_expired`, `harga_modal`, `harga_modal_ppn`, `hja_upds`, `hja_resep`, `hja_relasi`, `tanggal_input`, `status_delete`) VALUES
(4, 'OMF-TBT-00000000038', 1, 'Paramex', 1, 1, 1, 'Pcs', '-', '2020-09-10', 14180, 14180, 15598, 15598, 17016, '2022-04-13', 0),
(5, 'OMF-TBT-00000000037', 1, 'Bionix', 1, 1, 11, 'Pcs', '-', '2030-01-31', 2220, 2220, 2442, 2442, 2664, '2021-11-08', 0),
(7, 'OMF-TBT-00000000036', 1, 'Mixagrip', 1, 1, -23, 'Pcs', '-', '2025-05-25', 2000, 2220, 2442, 2442, 2664, '2021-11-08', 0),
(8, 'OMF-TBT-00000000004', 2, 'OBH Combi', 1, 1, 155, 'Pcs', '', '2018-09-19', 2000, 2200, 0, 0, 0, '2021-11-08', 1),
(9, 'OMF-TBT-00000000035', 1, 'Panadol Hijau', 1, 1, 0, 'Pcs', '-', '2018-09-27', 2220, 2220, 2442, 2442, 2664, '2021-11-08', 0),
(18, 'OMF-SRP-00000000027', 2, 'Bisoprolol', 1, 1, 23, 'Pcs', '-', '2019-01-17', 22200, 22200, 24420, 24420, 26640, '2022-06-30', 0),
(24, 'OMF-TBT-00000000026', 1, 'Mevinal', 1, 1, 152, 'Pcs', '-', '2019-02-01', 20000, 22200, 24420, 24420, 26640, '2021-11-08', 0),
(29, 'OMF-SRP-00000000026', 2, 'Puyer', 1, 1, 14, 'Pcs', '-', '2019-02-22', 11100, 11100, 12210, 12210, 13320, '2021-11-08', 0),
(30, 'OMF-SRP-00000000026', 2, 'OBH Combi', 1, 1, 168, 'Pcs', 'Umur 2 - 5 tahun : 3 x sehari 1 sendok takar (@ 5 ml). Umur 6 - 12 tahun : 3 x sehari 2 sendok takar (@10 ml)', '2018-09-19', 10000, 11100, 12210, 12210, 13320, '2021-11-08', 0),
(32, 'OMF-TBT-00000000032', 1, 'Obat Baru', 1, 1, 295, 'Pcs', 'Dewasa 3x1', '2021-07-31', 20000, 22200, 24420, 24420, 26640, '2021-11-08', 0),
(39, 'OMF-TBT-00000000034', 1, 'Ivermectin 12mg', 1, 1, 86, 'Pcs', 'Strongyloidiasis: dosis yang direkomendasikan adalah dosis tunggal 200 mcg/kgBB', '2021-08-02', 135300, 150183, 165201.3, 165201.3, 180219.6, '2021-11-08', 0),
(40, 'OMF-SRP-00000000039', 3, 'Biogesic 400 mg', 1, 1, 786, 'Pcs', '400 mg.  Dewasa: 1 - 2 kaplet, 3 sampai 4 kali per hari. Penggunaan maximum 8 kaplet per hari. Anak (7 - 12 tahun): 0.5 - 1 kaplet, 3 sampai 4 kali per hari. Penggunaan maximum 4 kaplet per hari.', '2022-11-12', 4000, 4440, 4884, 4884, 5328, '2021-11-08', 0),
(41, 'OMF-SRP-00000000039', 3, 'Alphamol 600 mg 10 Kaplet', 1, 1, 123, 'Pcs', 'Dewasa: 1-2 kaplet, 3-4 kali per hari. Penggunaan maximum 8 kaplet per hari. Anak 7-12 tahun: 0.5 - 1 kaplet, 3-4 kali per hari. Penggunaan maximum 4 kaplet per hari.', '2022-05-13', 15265, 16944, 18638.4, 18638.4, 20332.8, '2022-04-13', 0),
(42, 'OMF-SRP-00000000039', 3, 'Bioplacenton Gel 15 g', 1, 1, 200, 'Pcs', '-', '2021-11-15', 2000, 2200, 0, 0, 0, '2021-11-08', 1),
(43, 'OMF-SRP-00000000039', 2, 'Test', 1, 1, 90, 'Pcs', '-', '2021-08-15', 3117, 3459.87, 3805.857, 3805.857, 4151.844, '2022-04-19', 0),
(44, 'OMF-SRP-00000000121', 2, 'Test Lagi', 1, 1, 0, 'Dus', '-', '2021-08-18', 2000, 2200, 2420, 2420, 0, '2021-11-08', 1),
(45, 'OMF-SLP-00000000122', 5, 'Test Gan', 1, 1, 10, 'Pcs', '-', '2021-09-06', 10000, 11000, 12100, 12100, 13200, '2021-11-08', 0),
(46, 'OMF-SRP-00000000123', 2, 'Test Jagos', 1, 1, 100, 'pcs', '-', '2021-09-25', 10000, 11100, 12210, 12210, 13320, '2021-11-08', 0);

-- --------------------------------------------------------

--
-- Table structure for table `obat_detail`
--

CREATE TABLE `obat_detail` (
  `id_obat_detail` int NOT NULL,
  `id_obat` int NOT NULL,
  `id_supplier` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `obat_detail`
--

INSERT INTO `obat_detail` (`id_obat_detail`, `id_obat`, `id_supplier`) VALUES
(10, 8, 2),
(11, 8, 4),
(40, 30, 2),
(41, 29, 2),
(42, 29, 4),
(43, 24, 2),
(44, 24, 4),
(58, 32, 2),
(59, 32, 4),
(60, 32, 5),
(67, 39, 6),
(73, 9, 2),
(74, 9, 4),
(75, 7, 2),
(76, 7, 4),
(77, 5, 2),
(78, 5, 4),
(82, 40, 2),
(83, 40, 5),
(86, 42, 5),
(91, 44, 4),
(94, 45, 2),
(103, 46, 2),
(104, 4, 2),
(105, 4, 4),
(106, 41, 5),
(108, 43, 2),
(109, 18, 2),
(110, 18, 4);

-- --------------------------------------------------------

--
-- Table structure for table `pabrik_obat`
--

CREATE TABLE `pabrik_obat` (
  `id_pabrik_obat` int NOT NULL,
  `nama_pabrik` varchar(70) NOT NULL,
  `nomor_telepon_pabrik` varchar(30) NOT NULL,
  `alamat_pabrik` text NOT NULL,
  `status_delete` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pabrik_obat`
--

INSERT INTO `pabrik_obat` (`id_pabrik_obat`, `nama_pabrik`, `nomor_telepon_pabrik`, `alamat_pabrik`, `status_delete`) VALUES
(1, 'SANBE', '0', '-', 0),
(2, 'Kimia Farma', '0', '-', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pasien`
--

CREATE TABLE `pasien` (
  `id_pasien` int NOT NULL,
  `nama_pasien` varchar(70) NOT NULL,
  `nomor_telepon_pasien` varchar(20) NOT NULL,
  `alamat_pasien` text NOT NULL,
  `status_delete` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pasien`
--

INSERT INTO `pasien` (`id_pasien`, `nama_pasien`, `nomor_telepon_pasien`, `alamat_pasien`, `status_delete`) VALUES
(18, 'Tony', '081238283182', 'Jln. Merdeka No.3', 0),
(19, 'Rudi', '081567284182', 'Jln. Muso Salim Gang 8', 0),
(20, 'Muhammad Andy', '085723890999', 'Jln. P.M. Noor', 0),
(21, 'Ilham', '0888888888', 'Jln. Mongonsidi', 0),
(22, 'Muhammad Ilham', '0841231232', 'Jln. Rajawali Dalam 3', 0),
(23, 'Fajar', '08538888888', '-', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pemakaian_obat`
--

CREATE TABLE `pemakaian_obat` (
  `id_pemakaian` int NOT NULL,
  `tanggal_pemakaian` date NOT NULL,
  `id_dokter` int DEFAULT NULL,
  `id_supplier` int DEFAULT NULL,
  `id_obat` int NOT NULL,
  `stok_pakai` int NOT NULL,
  `ket_data` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `pemakaian_obat`
--

INSERT INTO `pemakaian_obat` (`id_pemakaian`, `tanggal_pemakaian`, `id_dokter`, `id_supplier`, `id_obat`, `stok_pakai`, `ket_data`) VALUES
(1, '2021-08-03', NULL, 5, 29, 10, 'supplier'),
(2, '2021-08-03', NULL, 6, 39, 10, 'supplier'),
(3, '2021-08-03', NULL, 6, 39, 10, 'supplier'),
(4, '2021-08-03', NULL, 6, 39, 10, 'supplier'),
(5, '2021-08-03', NULL, 6, 39, 10, 'supplier'),
(6, '2021-08-03', NULL, 6, 39, 10, 'supplier'),
(7, '2021-08-03', NULL, 6, 39, 10, 'supplier'),
(8, '2021-08-03', NULL, 6, 39, 10, 'supplier'),
(9, '2021-08-03', NULL, 5, 24, 12, 'supplier'),
(10, '2021-08-03', NULL, 5, 24, 12, 'supplier'),
(11, '2021-08-03', NULL, 5, 24, 12, 'supplier'),
(12, '2021-08-03', NULL, 5, 24, 12, 'supplier'),
(13, '2021-08-03', NULL, 5, 24, 12, 'supplier'),
(14, '2021-08-03', NULL, 5, 7, 10, 'supplier'),
(15, '2021-08-03', NULL, 5, 7, 10, 'supplier'),
(16, '2021-08-03', NULL, 2, 18, 10, 'supplier'),
(17, '2021-08-03', NULL, 2, 18, 10, 'supplier'),
(18, '2021-08-03', NULL, 2, 18, 20, 'supplier'),
(19, '2021-08-03', NULL, 5, 8, 10, 'supplier'),
(20, '2021-08-04', NULL, 6, 39, 10, 'supplier'),
(21, '2021-08-04', NULL, 2, 18, 10, 'supplier'),
(22, '2021-08-04', NULL, 2, 32, 5, 'supplier'),
(23, '2021-08-04', NULL, 6, 39, 10, 'supplier'),
(24, '2021-08-04', NULL, 6, 39, 10, 'supplier'),
(25, '2021-08-04', NULL, 6, 39, 20, 'supplier'),
(26, '2021-08-04', 1, 6, 39, 10, 'dokter'),
(27, '2021-08-04', 1, 4, 4, 10, 'dokter'),
(28, '2021-08-09', NULL, 6, 39, 5, 'supplier'),
(29, '2021-08-10', 1, 6, 39, 10, 'dokter'),
(30, '2021-08-10', 1, 6, 39, 10, 'dokter'),
(31, '2021-08-12', NULL, 6, 39, 10, 'supplier'),
(32, '2021-08-12', NULL, 6, 39, 10, 'supplier'),
(33, '2021-08-12', 2, 6, 39, 10, 'dokter'),
(34, '2021-08-12', NULL, 5, 40, 10, 'supplier'),
(35, '2021-08-13', 2, 5, 40, 10, 'dokter'),
(36, '2021-08-13', 2, 5, 40, 10, 'dokter'),
(37, '2021-08-13', 2, 5, 40, 10, 'dokter'),
(38, '2021-08-13', NULL, 5, 40, 10, 'supplier'),
(39, '2021-08-13', NULL, 5, 40, 10, 'supplier'),
(40, '2021-08-13', NULL, 6, 39, 10, 'supplier'),
(41, '2021-08-13', NULL, 5, 40, 20, 'supplier'),
(42, '2021-08-13', NULL, 5, 40, 10, 'supplier'),
(43, '2021-08-13', NULL, 5, 40, 10, 'supplier'),
(44, '2021-08-13', NULL, 5, 40, 10, 'supplier'),
(45, '2021-08-13', NULL, 4, 18, 10, 'supplier'),
(46, '2021-08-15', NULL, 5, 40, 10, 'supplier'),
(47, '2021-08-15', NULL, 4, 4, 10, 'supplier'),
(48, '2021-08-15', NULL, 5, 40, 10, 'supplier'),
(49, '2021-08-15', NULL, 5, 40, 10, 'supplier'),
(50, '2021-08-15', NULL, 5, 40, 20, 'supplier'),
(51, '2021-08-15', NULL, 5, 40, 10, 'supplier'),
(52, '2021-08-15', NULL, 4, 9, 10, 'supplier'),
(53, '2021-08-15', NULL, 5, 40, 10, 'supplier'),
(54, '2021-08-15', NULL, 5, 40, 10, 'supplier'),
(55, '2021-08-15', 2, 4, 5, 10, 'dokter'),
(56, '2021-08-15', 2, 4, 7, 10, 'dokter'),
(57, '2021-08-15', NULL, 4, 18, 10, 'supplier'),
(58, '2021-08-16', NULL, 4, 7, 10, 'supplier'),
(59, '2021-08-16', NULL, 4, 9, 10, 'supplier'),
(60, '2021-08-17', 2, 2, 9, 10, 'dokter'),
(61, '2021-08-17', 2, 4, 5, 10, 'dokter'),
(62, '2021-08-17', NULL, 2, 5, 10, 'supplier'),
(63, '2021-08-17', NULL, 2, 5, 10, 'supplier'),
(64, '2021-08-17', NULL, 2, 5, 10, 'supplier'),
(65, '2021-08-17', NULL, 2, 5, 10, 'supplier'),
(66, '2021-08-17', NULL, 2, 5, 10, 'supplier'),
(67, '2021-08-17', NULL, 2, 5, 10, 'supplier'),
(68, '2021-08-17', NULL, 2, 5, 10, 'supplier'),
(69, '2021-08-17', NULL, 4, 4, 10, 'supplier'),
(70, '2021-08-17', NULL, 4, 4, 10, 'supplier'),
(71, '2021-08-17', NULL, 4, 4, 10, 'supplier'),
(72, '2021-08-17', NULL, 4, 4, 10, 'supplier'),
(73, '2021-08-17', NULL, 4, 4, 10, 'supplier'),
(74, '2021-08-17', 2, 4, 5, 10, 'dokter'),
(75, '2021-08-17', 2, 4, 5, 10, 'dokter'),
(76, '2021-08-17', 2, 4, 5, 10, 'dokter'),
(77, '2021-08-17', 2, 4, 5, 10, 'dokter'),
(78, '2021-08-17', 2, 4, 5, 10, 'dokter'),
(79, '2021-08-17', 2, 4, 5, 10, 'dokter'),
(80, '2021-08-17', 2, 4, 5, 10, 'dokter'),
(81, '2021-08-17', 2, 4, 5, 10, 'dokter'),
(82, '2021-08-18', NULL, 2, 43, 100, 'supplier'),
(83, '2021-08-18', NULL, 2, 5, 100, 'supplier'),
(84, '2021-08-18', 2, 2, 4, 10, 'dokter'),
(85, '2021-08-18', 2, 2, 5, 100, 'dokter'),
(86, '2021-08-18', 2, 2, 5, 10, 'dokter'),
(87, '2021-08-18', 2, 2, 7, 10, 'dokter'),
(88, '2021-08-18', NULL, 2, 18, 10, 'supplier'),
(89, '2021-08-19', 2, 5, 41, 10, 'dokter'),
(90, '2021-08-19', 2, 5, 41, 10, 'dokter'),
(91, '2021-08-19', 2, 5, 41, 10, 'dokter'),
(92, '2021-08-19', NULL, 5, 41, 10, 'supplier'),
(93, '2021-08-19', NULL, 5, 41, 10, 'supplier'),
(94, '2021-08-21', NULL, 4, 5, 10, 'supplier'),
(95, '2021-08-21', NULL, 4, 5, 10, 'supplier'),
(96, '2021-08-21', NULL, 4, 5, 10, 'supplier'),
(97, '2021-08-21', NULL, 4, 5, 10, 'supplier'),
(98, '2021-08-21', NULL, 4, 5, 10, 'supplier'),
(99, '2021-08-21', NULL, 4, 5, 10, 'supplier'),
(100, '2021-08-21', NULL, 4, 5, 10, 'supplier'),
(101, '2021-08-21', NULL, 4, 5, 10, 'supplier'),
(102, '2021-08-21', NULL, 4, 5, 10, 'supplier'),
(103, '2021-08-21', NULL, 4, 5, 10, 'supplier'),
(104, '2021-08-21', NULL, 4, 5, 10, 'supplier'),
(105, '2021-08-21', NULL, 4, 5, 10, 'supplier'),
(106, '2021-08-21', NULL, 4, 5, 10, 'supplier'),
(107, '2021-08-21', NULL, 4, 5, 10, 'supplier'),
(108, '2021-08-21', NULL, 4, 5, 10, 'supplier'),
(109, '2021-08-21', NULL, 4, 5, 10, 'supplier'),
(110, '2021-08-21', NULL, 6, 39, 10, 'supplier'),
(111, '2021-08-21', NULL, 6, 39, 10, 'supplier'),
(112, '2021-08-21', 1, 2, 4, 1, 'dokter'),
(113, '2021-08-21', 1, 2, 7, 1, 'dokter'),
(114, '2021-08-21', 1, 4, 9, 1, 'dokter'),
(115, '2021-08-21', 1, 4, 18, 1, 'dokter'),
(116, '2021-08-21', 2, 2, 7, 1, 'dokter'),
(117, '2021-08-21', 2, 2, 4, 1, 'dokter'),
(118, '2021-08-21', 2, 4, 9, 1, 'dokter'),
(119, '2021-08-21', 2, 2, 7, 1, 'dokter'),
(120, '2021-08-21', 2, 2, 4, 1, 'dokter'),
(121, '2021-08-21', 2, 4, 9, 1, 'dokter'),
(122, '2021-08-21', 2, 2, 7, 1, 'dokter'),
(123, '2021-08-21', 2, 2, 4, 1, 'dokter'),
(124, '2021-08-21', 2, 4, 9, 1, 'dokter'),
(125, '2021-08-21', 2, 2, 4, 1, 'dokter'),
(126, '2021-08-21', NULL, 2, 4, 1, 'supplier'),
(127, '2021-08-21', NULL, 4, 4, 1, 'supplier'),
(128, '2021-08-24', 2, 2, 4, 1, 'dokter'),
(129, '2021-08-24', 2, 2, 4, 1, 'dokter'),
(130, '2021-08-24', 2, 2, 4, 1, 'dokter'),
(131, '2021-08-24', 2, 4, 7, 1, 'dokter'),
(132, '2021-08-24', 2, 4, 9, 1, 'dokter'),
(133, '2021-08-24', 2, 4, 4, 1, 'dokter'),
(134, '2021-08-24', NULL, 4, 4, 2, 'supplier'),
(135, '2021-08-24', NULL, 2, 4, 2, 'supplier'),
(136, '2021-08-25', 2, 2, 4, 1, 'dokter'),
(137, '2021-08-25', 2, 4, 9, 1, 'dokter'),
(138, '2021-08-25', 2, 2, 4, 1, 'dokter'),
(139, '2021-08-25', 2, 4, 9, 1, 'dokter'),
(140, '2021-08-25', 2, 2, 4, 1, 'dokter'),
(141, '2021-08-25', 2, 4, 9, 1, 'dokter'),
(142, '2021-08-25', 2, 2, 4, 1, 'dokter'),
(143, '2021-08-25', 2, 4, 9, 1, 'dokter'),
(144, '2021-08-25', 2, 2, 4, 1, 'dokter'),
(145, '2021-08-25', 2, 4, 9, 1, 'dokter'),
(146, '2021-08-25', 2, 4, 18, 1, 'dokter'),
(147, '2021-08-25', 2, 2, 24, 1, 'dokter'),
(148, '2021-08-25', 2, 4, 4, 1, 'dokter'),
(149, '2021-08-25', 2, 2, 9, 1, 'dokter'),
(150, '2021-08-25', 2, 4, 4, 1, 'dokter'),
(151, '2021-08-25', 2, 2, 9, 1, 'dokter'),
(152, '2021-08-25', 1, 2, 4, 1, 'dokter'),
(153, '2021-08-25', NULL, 4, 4, 1, 'supplier'),
(154, '2021-08-25', NULL, 4, 4, 1, 'supplier'),
(155, '2021-08-26', NULL, 2, 4, 1, 'supplier'),
(156, '2021-08-26', NULL, 4, 4, 2, 'supplier'),
(157, '2021-08-26', NULL, 2, 4, 2, 'supplier'),
(158, '2021-08-26', NULL, 2, 4, 2, 'supplier'),
(159, '2021-08-26', NULL, 2, 4, 1, 'supplier'),
(160, '2021-08-26', NULL, 2, 4, 1, 'supplier'),
(161, '2021-08-26', 2, 4, 4, 1, 'dokter'),
(162, '2021-08-26', 2, 4, 4, 1, 'dokter'),
(163, '2021-08-26', NULL, 2, 7, 1, 'supplier'),
(164, '2021-08-26', NULL, 2, 7, 1, 'supplier'),
(165, '2021-08-26', NULL, 2, 7, 1, 'supplier'),
(166, '2021-08-26', NULL, 2, 7, 1, 'supplier'),
(167, '2021-08-26', 2, 2, 4, 1, 'dokter'),
(168, '2021-08-26', 2, 4, 18, 1, 'dokter'),
(169, '2021-08-26', 2, 4, 4, 1, 'dokter'),
(170, '2021-08-26', 2, 2, 9, 1, 'dokter'),
(171, '2021-08-26', 2, 4, 4, 1, 'dokter'),
(172, '2021-08-26', 2, 2, 4, 1, 'dokter'),
(173, '2021-08-26', 2, 2, 4, 1, 'dokter'),
(174, '2021-08-26', 2, 4, 4, 1, 'dokter'),
(175, '2021-08-26', 2, 4, 18, 1, 'dokter'),
(176, '2021-08-26', 2, 4, 9, 1, 'dokter'),
(177, '2021-08-26', 1, 4, 4, 1, 'dokter'),
(178, '2021-09-07', 2, 4, 4, 1, 'dokter'),
(179, '2021-09-07', 2, 4, 7, 1, 'dokter'),
(180, '2021-09-07', 2, 2, 9, 1, 'dokter'),
(181, '2021-09-09', NULL, 4, 4, 10, 'supplier'),
(182, '2021-09-09', NULL, 4, 4, 10, 'supplier'),
(183, '2021-09-09', NULL, 4, 4, 10, 'supplier'),
(184, '2021-09-09', NULL, 4, 4, 10, 'supplier'),
(185, '2021-09-09', NULL, 4, 4, 10, 'supplier'),
(186, '2021-09-09', NULL, 4, 4, 10, 'supplier'),
(187, '2021-09-09', NULL, 4, 4, 10, 'supplier'),
(188, '2021-09-09', NULL, 4, 4, 10, 'supplier'),
(189, '2021-09-09', NULL, 4, 4, 10, 'supplier'),
(190, '2021-09-09', NULL, 4, 4, 10, 'supplier'),
(191, '2021-09-09', NULL, 4, 4, 10, 'supplier'),
(192, '2021-09-09', NULL, 4, 4, 10, 'supplier'),
(193, '2021-09-09', NULL, 4, 4, 10, 'supplier'),
(194, '2021-09-09', NULL, 4, 4, 10, 'supplier'),
(195, '2021-09-09', NULL, 4, 4, 10, 'supplier'),
(196, '2021-09-09', NULL, 4, 4, 10, 'supplier'),
(197, '2021-09-09', NULL, 4, 4, 10, 'supplier'),
(198, '2021-09-09', NULL, 4, 4, 10, 'supplier'),
(199, '2021-09-09', NULL, 4, 4, 10, 'supplier'),
(200, '2021-09-09', NULL, 4, 4, 10, 'supplier'),
(201, '2021-09-09', NULL, 4, 9, 10, 'supplier'),
(202, '2021-09-09', NULL, 2, 7, 1, 'supplier'),
(203, '2021-09-10', NULL, 4, 9, 1, 'supplier'),
(204, '2021-09-10', 2, 4, 4, 10, 'dokter'),
(205, '2021-09-10', 2, 2, 7, 10, 'dokter'),
(206, '2021-09-10', 2, 2, 9, 10, 'dokter'),
(207, '2021-09-10', 2, 4, 18, 10, 'dokter'),
(208, '2021-09-10', NULL, 4, 4, 2, 'supplier'),
(209, '2021-09-10', NULL, 4, 4, 2, 'supplier'),
(210, '2021-09-10', 2, 4, 4, 1, 'dokter'),
(211, '2021-09-10', 2, 2, 7, 1, 'dokter'),
(212, '2021-09-10', 2, 2, 9, 1, 'dokter'),
(213, '2021-09-10', 2, 2, 18, 1, 'dokter'),
(214, '2021-09-10', NULL, 2, 9, 1, 'supplier'),
(215, '2021-09-14', 1, 2, 4, 1, 'dokter'),
(216, '2021-09-20', NULL, 4, 4, 1, 'supplier'),
(217, '2021-09-23', NULL, 2, 4, 1, 'supplier'),
(218, '2021-09-23', NULL, 4, 4, 1, 'supplier'),
(219, '2021-09-23', 2, 4, 4, 1, 'dokter'),
(220, '2021-09-30', 2, 2, 4, 1, 'dokter'),
(221, '2021-09-30', 2, 4, 7, 1, 'dokter'),
(222, '2021-09-30', 2, 2, 7, 1, 'dokter'),
(223, '2021-10-01', NULL, 4, 4, 1, 'supplier'),
(224, '2021-10-01', NULL, 4, 4, 1, 'supplier'),
(225, '2021-10-01', NULL, 2, 4, 1, 'supplier'),
(226, '2021-10-01', NULL, 2, 4, 1, 'supplier'),
(227, '2021-10-01', NULL, 2, 7, 1, 'supplier'),
(228, '2021-10-01', NULL, 4, 4, 1, 'supplier'),
(229, '2021-10-01', NULL, 2, 5, 1, 'supplier'),
(230, '2021-10-04', NULL, 4, 7, 12, 'supplier'),
(231, '2021-10-19', 1, 4, 4, 2, 'dokter'),
(232, '2021-10-20', NULL, 5, 40, 0, 'supplier'),
(233, '2021-10-20', NULL, 2, 4, 0, 'supplier'),
(234, '2021-10-20', NULL, 4, 4, 0, 'supplier'),
(235, '2021-10-20', NULL, 2, 4, 0, 'supplier'),
(236, '2021-10-20', NULL, 2, 4, 1, 'supplier'),
(237, '2021-10-20', NULL, 2, 7, 1, 'supplier'),
(238, '2021-10-20', NULL, 2, 7, 1, 'supplier'),
(239, '2021-10-20', 2, 2, 4, 2, 'dokter'),
(240, '2021-10-20', 2, 2, 4, 2, 'dokter'),
(241, '2021-10-20', 2, 2, 4, 2, 'dokter'),
(242, '2021-10-20', 2, 2, 4, 2, 'dokter'),
(243, '2021-10-20', 2, 2, 4, 2, 'dokter'),
(244, '2021-10-20', 2, 4, 4, 2, 'dokter'),
(245, '2021-10-22', NULL, 4, 4, 10, 'supplier'),
(246, '2021-10-22', NULL, 4, 4, 10, 'supplier'),
(247, '2021-10-22', NULL, 4, 4, 10, 'supplier'),
(248, '2021-10-22', NULL, 4, 4, 10, 'supplier'),
(249, '2021-10-22', NULL, 4, 4, 10, 'supplier'),
(250, '2021-10-22', NULL, 2, 7, 10, 'supplier'),
(251, '2021-10-22', NULL, 2, 7, 10, 'supplier'),
(252, '2021-10-22', NULL, 4, 7, 10, 'supplier'),
(253, '2021-10-22', NULL, 4, 7, 10, 'supplier'),
(254, '2021-10-22', NULL, 2, 4, 10, 'supplier'),
(255, '2021-10-22', NULL, 4, 7, 10, 'supplier'),
(256, '2021-10-22', NULL, 2, 4, 4, 'supplier'),
(257, '2021-10-22', NULL, 2, 4, 4, 'supplier'),
(258, '2021-10-22', NULL, 2, 7, 10, 'supplier'),
(259, '2021-10-22', NULL, 4, 4, 10, 'supplier'),
(260, '2021-10-22', NULL, 4, 4, 10, 'supplier'),
(261, '2021-10-22', NULL, 4, 4, 4, 'supplier'),
(262, '2021-10-22', NULL, 4, 4, 4, 'supplier'),
(263, '2021-10-28', 1, 2, 4, 1, 'dokter'),
(264, '2021-10-28', 1, 4, 4, 1, 'dokter'),
(265, '2021-10-29', 1, 4, 4, 1, 'dokter'),
(266, '2021-10-29', 1, 4, 4, 1, 'dokter'),
(267, '2021-10-29', 1, 4, 4, 1, 'dokter'),
(268, '2021-10-29', 1, 4, 4, 1, 'dokter'),
(269, '2021-10-29', 1, 4, 4, 1, 'dokter'),
(270, '2021-10-29', 1, 4, 4, 1, 'dokter'),
(271, '2021-10-29', 1, 4, 4, 1, 'dokter'),
(272, '2021-10-29', 1, 4, 4, 1, 'dokter'),
(273, '2021-10-29', 1, 4, 7, 1, 'dokter'),
(274, '2021-10-29', 1, 2, 40, 1, 'dokter'),
(275, '2021-10-29', 1, 4, 7, 1, 'dokter'),
(276, '2021-10-29', 1, 2, 40, 1, 'dokter'),
(277, '2021-10-29', 1, 4, 7, 1, 'dokter'),
(278, '2021-10-29', 1, 2, 40, 1, 'dokter'),
(279, '2021-11-18', NULL, 2, 4, 1, 'supplier'),
(280, '2021-12-14', NULL, 2, 5, 10, 'supplier'),
(281, '2021-12-16', NULL, 4, 4, 10, 'supplier'),
(282, '2021-12-16', NULL, 4, 5, 10, 'supplier'),
(283, '2021-12-16', NULL, 2, 7, 10, 'supplier'),
(284, '2021-12-16', NULL, 4, 24, 10, 'supplier'),
(285, '2021-12-16', NULL, 2, 40, 10, 'supplier'),
(286, '2021-12-16', NULL, 2, 7, 10, 'supplier'),
(287, '2021-12-16', NULL, 4, 24, 10, 'supplier'),
(288, '2021-12-16', NULL, 2, 40, 10, 'supplier'),
(289, '2021-12-16', NULL, 4, 7, 10, 'supplier'),
(290, '2021-12-16', NULL, 4, 7, 10, 'supplier'),
(291, '2021-12-16', NULL, 4, 7, 10, 'supplier'),
(292, '2021-12-16', NULL, 2, 7, 10, 'supplier'),
(293, '2021-12-16', NULL, 2, 9, 5, 'supplier'),
(294, '2022-04-04', NULL, 2, 4, 10, 'supplier'),
(295, '2022-04-04', NULL, 4, 4, 10, 'supplier'),
(296, '2022-04-04', NULL, 2, 4, 10, 'supplier'),
(297, '2022-04-08', 1, 2, 4, 2, 'dokter'),
(298, '2022-04-08', 1, 2, 7, 2, 'dokter'),
(299, '2022-04-08', 1, 4, 9, 2, 'dokter'),
(300, '2022-04-09', 1, 2, 4, 2, 'dokter'),
(301, '2022-04-09', 1, 2, 7, 2, 'dokter'),
(302, '2022-04-09', 1, 4, 9, 2, 'dokter'),
(303, '2022-04-09', 1, 2, 4, 2, 'dokter'),
(304, '2022-04-09', 1, 2, 7, 2, 'dokter'),
(305, '2022-04-09', 1, 4, 9, 2, 'dokter'),
(306, '2022-04-09', 1, 2, 4, 2, 'dokter'),
(307, '2022-04-09', 1, 2, 7, 2, 'dokter'),
(308, '2022-04-09', 1, 4, 9, 2, 'dokter'),
(309, '2022-04-09', 1, 2, 4, 2, 'dokter'),
(310, '2022-04-09', 1, 2, 7, 2, 'dokter'),
(311, '2022-04-09', 1, 4, 9, 2, 'dokter'),
(312, '2022-04-09', 1, 2, 4, 2, 'dokter'),
(313, '2022-04-09', 1, 2, 7, 2, 'dokter'),
(314, '2022-04-09', 1, 4, 9, 2, 'dokter'),
(315, '2022-04-09', 1, 2, 4, 2, 'dokter'),
(316, '2022-04-09', 1, 2, 7, 2, 'dokter'),
(317, '2022-04-09', 1, 4, 9, 2, 'dokter'),
(318, '2022-04-12', 2, 4, 4, 2, 'dokter'),
(319, '2022-04-12', 2, 2, 7, 2, 'dokter'),
(320, '2022-04-12', 2, 4, 4, 2, 'dokter'),
(321, '2022-04-12', 2, 2, 7, 2, 'dokter'),
(322, '2022-04-12', 2, 4, 4, 2, 'dokter'),
(323, '2022-04-12', 2, 2, 7, 2, 'dokter'),
(324, '2022-04-12', 2, 4, 4, 2, 'dokter'),
(325, '2022-04-12', 2, 2, 7, 2, 'dokter'),
(326, '2022-04-12', 2, 2, 4, 2, 'dokter'),
(327, '2022-04-12', 2, 4, 9, 2, 'dokter'),
(328, '2022-04-12', 2, 4, 4, 1, 'dokter'),
(329, '2022-04-12', 2, 2, 7, 5, 'dokter'),
(330, '2022-04-12', 2, 2, 4, 2, 'dokter'),
(331, '2022-04-12', 2, 4, 9, 2, 'dokter'),
(332, '2022-04-12', 2, 4, 4, 1, 'dokter'),
(333, '2022-04-12', 2, 2, 7, 5, 'dokter'),
(334, '2022-04-12', 2, 2, 4, 1, 'dokter'),
(335, '2022-04-12', 2, 4, 7, 1, 'dokter'),
(336, '2022-04-12', 1, 2, 4, 1, 'dokter'),
(337, '2022-04-12', 1, 2, 7, 1, 'dokter'),
(338, '2022-04-12', 1, 4, 9, 1, 'dokter'),
(339, '2022-04-12', 1, 2, 18, 1, 'dokter'),
(340, '2022-04-12', 1, 2, 4, 1, 'dokter'),
(341, '2022-04-12', 1, 2, 7, 1, 'dokter'),
(342, '2022-04-12', 1, 4, 9, 1, 'dokter'),
(343, '2022-04-12', 1, 2, 18, 1, 'dokter'),
(344, '2022-04-12', 1, 2, 4, 1, 'dokter'),
(345, '2022-04-12', 1, 2, 7, 1, 'dokter'),
(346, '2022-04-12', 1, 4, 9, 1, 'dokter'),
(347, '2022-04-12', 1, 2, 18, 1, 'dokter'),
(348, '2022-04-12', 1, 2, 4, 1, 'dokter'),
(349, '2022-04-12', 1, 2, 7, 1, 'dokter'),
(350, '2022-04-12', 1, 4, 9, 1, 'dokter'),
(351, '2022-04-12', 1, 2, 18, 1, 'dokter'),
(352, '2022-04-12', 1, 2, 4, 1, 'dokter'),
(353, '2022-04-12', 1, 2, 7, 1, 'dokter'),
(354, '2022-04-12', 1, 4, 9, 1, 'dokter'),
(355, '2022-04-12', 1, 2, 18, 1, 'dokter'),
(356, '2022-04-12', 1, 2, 4, 1, 'dokter'),
(357, '2022-04-12', 1, 2, 7, 1, 'dokter'),
(358, '2022-04-12', 1, 4, 9, 1, 'dokter'),
(359, '2022-04-12', 1, 2, 18, 1, 'dokter'),
(360, '2022-04-12', 1, 2, 4, 1, 'dokter'),
(361, '2022-04-12', 1, 2, 7, 1, 'dokter'),
(362, '2022-04-12', 1, 4, 9, 1, 'dokter'),
(363, '2022-04-12', 1, 2, 18, 1, 'dokter'),
(364, '2022-04-18', NULL, 4, 4, 10, 'supplier'),
(365, '2022-04-19', NULL, 2, 43, 10, 'supplier'),
(366, '2022-05-28', 2, 2, 4, 10, 'dokter'),
(367, '2022-05-28', 2, 4, 7, 10, 'dokter'),
(368, '2022-05-28', 2, 2, 9, 2, 'dokter'),
(369, '2022-05-28', 2, 2, 18, 10, 'dokter'),
(370, '2022-06-01', NULL, 2, 4, 5, 'supplier'),
(371, '2022-06-01', NULL, 2, 4, 5, 'supplier'),
(372, '2022-06-01', NULL, 2, 4, 5, 'supplier'),
(373, '2022-06-02', 1, 4, 4, 10, 'dokter'),
(374, '2022-06-02', 1, 2, 4, 10, 'dokter'),
(375, '2022-06-02', 1, 2, 4, 10, 'dokter'),
(376, '2022-06-02', 1, 2, 4, 10, 'dokter'),
(377, '2022-06-02', 1, 4, 7, 10, 'dokter'),
(378, '2022-06-02', NULL, 2, 7, 10, 'supplier'),
(379, '2022-06-02', NULL, 4, 7, 10, 'supplier'),
(380, '2022-06-10', 1, 2, 7, 10, 'dokter'),
(381, '2022-06-10', NULL, 4, 7, 10, 'supplier'),
(382, '2022-06-21', NULL, 2, 18, 10, 'supplier'),
(383, '2022-06-21', NULL, 2, 24, 10, 'supplier'),
(384, '2022-06-21', NULL, 2, 40, 5, 'supplier'),
(385, '2022-06-24', 1, 4, 4, 10, 'dokter'),
(386, '2022-06-24', 1, 4, 4, 10, 'dokter'),
(387, '2022-06-24', 1, 4, 4, 10, 'dokter'),
(388, '2022-06-24', 1, 2, 4, 10, 'dokter'),
(389, '2022-06-24', 1, 4, 9, 10, 'dokter'),
(390, '2022-06-24', 1, 2, 18, 10, 'dokter'),
(391, '2022-06-25', NULL, 4, 5, 10, 'supplier'),
(392, '2022-06-25', 1, 2, 30, 10, 'dokter'),
(393, '2022-06-25', 2, 2, 30, 10, 'dokter'),
(394, '2022-06-25', 1, 4, 9, 5, 'dokter'),
(395, '2022-06-25', 1, 2, 30, 10, 'dokter'),
(396, '2022-06-27', NULL, 2, 9, 2, 'supplier'),
(397, '2022-06-27', 1, 4, 9, 2, 'dokter'),
(398, '2022-07-11', 2, 4, 5, 2, 'dokter'),
(399, '2022-07-11', NULL, 2, 7, 10, 'supplier'),
(400, '2022-07-11', NULL, 4, 24, 10, 'supplier'),
(401, '2022-07-11', 1, 2, 9, 1, 'dokter'),
(402, '2022-07-11', 1, 4, 18, 1, 'dokter'),
(403, '2022-07-22', NULL, 2, 4, 10, 'supplier'),
(404, '2022-07-22', 1, 2, 4, 1, 'dokter'),
(405, '2022-07-22', 1, 2, 4, 1, 'dokter'),
(406, '2022-07-22', 1, 2, 4, 1, 'dokter'),
(407, '2022-07-22', 1, 2, 4, 1, 'dokter'),
(408, '2022-07-22', 1, 4, 7, 10, 'dokter'),
(409, '2022-07-22', 1, 4, 7, 10, 'dokter'),
(410, '2022-07-22', 1, 4, 7, 10, 'dokter'),
(411, '2022-07-22', 1, 4, 7, 10, 'dokter'),
(412, '2022-07-22', 1, 4, 7, 10, 'dokter'),
(413, '2022-07-22', 1, 4, 7, 10, 'dokter'),
(414, '2022-07-22', 1, 4, 7, 10, 'dokter'),
(415, '2022-07-23', 1, 2, 18, 10, 'dokter'),
(416, '2022-07-23', 1, 4, 9, 10, 'dokter'),
(417, '2022-07-23', 1, 4, 18, 10, 'dokter'),
(418, '2022-07-23', 1, 6, 39, 10, 'dokter'),
(419, '2022-07-23', NULL, 2, 4, 5, 'supplier'),
(420, '2022-07-25', NULL, 4, 4, 5, 'supplier'),
(421, '2022-08-01', NULL, 4, 4, 1, 'supplier'),
(422, '2022-08-01', NULL, 4, 4, 1, 'supplier'),
(423, '2022-08-01', NULL, 2, 4, 1, 'supplier'),
(424, '2022-08-01', NULL, 2, 18, 1, 'supplier'),
(425, '2022-08-01', NULL, 4, 18, 1, 'supplier'),
(426, '2022-08-01', NULL, 4, 18, 1, 'supplier'),
(427, '2022-08-01', NULL, 4, 4, 1, 'supplier'),
(428, '2022-08-01', NULL, 2, 4, 1, 'supplier'),
(429, '2022-08-01', NULL, 2, 4, 1, 'supplier'),
(430, '2022-08-01', NULL, 2, 4, 1, 'supplier'),
(431, '2022-08-01', NULL, 4, 4, 1, 'supplier'),
(432, '2022-08-01', NULL, 4, 4, 1, 'supplier'),
(433, '2022-08-18', NULL, 2, 4, 10, 'supplier'),
(434, '2022-08-18', NULL, 2, 4, 1, 'supplier'),
(435, '2022-08-18', NULL, 4, 4, 1, 'supplier'),
(436, '2022-08-18', NULL, 4, 4, 1, 'supplier'),
(437, '2022-08-18', NULL, 2, 4, 3, 'supplier'),
(438, '2022-08-18', NULL, 2, 4, 3, 'supplier'),
(439, '2022-08-18', NULL, 2, 4, 3, 'supplier'),
(440, '2022-08-18', NULL, 4, 4, 3, 'supplier'),
(441, '2022-08-18', NULL, 4, 5, 3, 'supplier'),
(442, '2022-08-18', NULL, 4, 5, 3, 'supplier'),
(443, '2022-08-18', NULL, 4, 5, 3, 'supplier'),
(444, '2022-08-18', NULL, 2, 4, 3, 'supplier'),
(445, '2022-08-18', NULL, 2, 18, 2, 'supplier'),
(446, '2022-08-18', NULL, 4, 18, 2, 'supplier'),
(447, '2022-08-21', 1, 2, 30, 1, 'dokter'),
(448, '2022-08-21', 1, 6, 39, 1, 'dokter'),
(449, '2022-08-21', 1, 2, 4, 1, 'dokter'),
(450, '2022-08-21', 1, 4, 18, 1, 'dokter'),
(451, '2022-08-21', 1, 2, 30, 1, 'dokter'),
(452, '2022-08-21', 1, 5, 41, 1, 'dokter'),
(453, '2022-08-21', 1, 5, 41, 1, 'dokter'),
(454, '2022-08-21', 1, 2, 4, 1, 'dokter'),
(455, '2022-08-21', 1, 4, 24, 1, 'dokter'),
(456, '2022-08-21', 1, 2, 40, 1, 'dokter'),
(457, '2022-08-21', 1, 2, 18, 1, 'dokter'),
(458, '2022-08-21', 1, 2, 29, 1, 'dokter');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_detail`
--

CREATE TABLE `pembelian_detail` (
  `id_pembelian_detail` int NOT NULL,
  `id_pembelian_obat` int NOT NULL,
  `id_obat` int NOT NULL,
  `jumlah` int NOT NULL,
  `harga_obat` int NOT NULL,
  `disc_1` double NOT NULL,
  `disc_2` double NOT NULL,
  `disc_3` double NOT NULL,
  `sub_total` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `pembelian_detail`
--

INSERT INTO `pembelian_detail` (`id_pembelian_detail`, `id_pembelian_obat`, `id_obat`, `jumlah`, `harga_obat`, `disc_1`, `disc_2`, `disc_3`, `sub_total`) VALUES
(14, 12, 32, 10, 23000, 0, 0, 0, 230000),
(15, 12, 41, 10, 3000, 0, 0, 0, 30000),
(16, 13, 32, 10, 20000, 0, 0, 0, 200000),
(17, 13, 40, 100, 4000, 0, 0, 0, 400000),
(18, 14, 32, 10, 20000, 0, 0, 0, 200000),
(19, 14, 40, 100, 4000, 0, 0, 0, 400000),
(20, 15, 32, 10, 20000, 0, 0, 0, 200000),
(21, 15, 40, 100, 4000, 0, 0, 0, 400000),
(22, 16, 32, 10, 20000, 0, 0, 0, 200000),
(23, 16, 40, 100, 4000, 0, 0, 0, 400000),
(24, 17, 32, 10, 20000, 0, 0, 0, 200000),
(25, 17, 40, 100, 4000, 0, 0, 0, 400000),
(26, 18, 32, 10, 20000, 0, 0, 0, 200000),
(27, 18, 40, 100, 4000, 0, 0, 0, 400000),
(28, 19, 32, 10, 20000, 0, 0, 0, 200000),
(29, 19, 40, 100, 4000, 0, 0, 0, 400000),
(30, 20, 32, 10, 20000, 0, 0, 0, 200000),
(31, 20, 40, 100, 4000, 0, 0, 0, 400000),
(32, 21, 30, 100, 10000, 0, 0, 0, 1000000),
(33, 22, 8, 10, 2000, 0, 0, 0, 20000),
(34, 24, 8, 1, 2000, 0, 0, 0, 2000),
(35, 25, 8, 1, 2000, 0, 0, 0, 2000),
(36, 26, 4, 2, 2000, 0, 0, 0, 4400),
(37, 27, 4, 2, 2000, 0, 0, 0, 4400),
(38, 28, 4, 2, 2000, 0, 0, 0, 4400),
(39, 29, 4, 2, 2000, 0, 0, 0, 4400),
(40, 30, 4, 1, 2000, 0, 0, 0, 2200),
(41, 32, 4, 1, 2200, 0, 0, 0, 2420),
(42, 33, 4, 10, 2200, 0, 0, 0, 22000),
(43, 34, 4, 10, 2200, 0, 0, 0, 22000),
(44, 35, 4, 10, 2200, 0, 0, 0, 22000),
(45, 36, 4, 1, 2200, 1, 0, 0, 2189),
(46, 37, 4, 1, 2200, 0, 0, 0, 2196),
(47, 38, 4, 10, 2200, 0, 0, 0, 21890),
(48, 39, 4, 10, 2200, 1, 0, 0, 21886),
(49, 40, 4, 10, 2200, 10, 0, 0, 19800),
(50, 41, 4, 10, 2200, 10, 0, 0, 19609),
(51, 42, 4, 5, 12775, 1.5, 0, 0, 62916.875),
(52, 42, 4, 5, 12775, 1.5, 0, 0, 62916.875),
(53, 42, 41, 5, 15265, 0, 0, 0, 76325),
(54, 43, 4, 5, 12775, 1.5, 0, 0, 62916.875),
(55, 43, 4, 5, 12775, 1.5, 0, 0, 62916.875),
(56, 43, 41, 5, 15265, 0, 0, 0, 76325),
(57, 44, 4, 5, 12775, 1.5, 0, 0, 62916.875),
(58, 44, 4, 5, 12775, 1.5, 0, 0, 62916.875),
(59, 44, 41, 5, 15265, 0, 0, 0, 76325),
(60, 45, 4, 5, 12775, 1.5, 0, 0, 62916.875),
(61, 45, 4, 5, 12775, 1.5, 0, 0, 62916.875),
(62, 45, 41, 5, 15265, 0, 0, 0, 76325),
(63, 46, 4, 5, 12775, 1.5, 0, 0, 62916.875),
(64, 46, 4, 5, 12775, 1.5, 0, 0, 62916.875),
(65, 46, 41, 5, 15265, 0, 0, 0, 76325),
(66, 47, 4, 5, 12775, 1.5, 0, 0, 62916.875),
(67, 47, 4, 5, 12775, 1.5, 0, 0, 62916.875),
(68, 47, 41, 5, 15265, 0, 0, 0, 76325),
(69, 48, 4, 5, 12775, 1.5, 0, 0, 62916.875),
(70, 48, 4, 5, 12775, 1.5, 0, 0, 62916.875),
(71, 48, 41, 5, 15265, 0, 0, 0, 76325),
(72, 49, 4, 10, 12776, 0, 0, 0, 127758.9),
(82, 53, 4, 5, 14180, 0, 0, 0, 70900),
(83, 53, 9, 5, 2220, 0, 0, 0, 11100),
(84, 53, 18, 5, 22200, 0, 0, 0, 111000),
(85, 54, 4, 5, 14180, 0, 0, 0, 70900),
(86, 54, 9, 5, 2220, 0, 0, 0, 11100),
(87, 54, 29, 5, 11100, 0, 0, 0, 55500),
(88, 55, 4, 5, 14180, 0, 0, 0, 70900),
(89, 56, 5, 10, 2220, 0, 0, 0, 22200),
(90, 57, 4, 10, 14180, 1, 2, 3, 133447.1292),
(91, 57, 5, 10, 2220, 4, 5, 6, 19031.616),
(92, 58, 4, 10, 14180, 1, 2, 3, 133447.1292),
(93, 58, 5, 10, 2220, 4, 5, 6, 19031.616),
(94, 59, 4, 10, 14180, 1, 2, 3, 133447.1292),
(95, 60, 4, 10, 14180, 0, 0, 0, 141800),
(96, 61, 4, 10, 14180, 0, 0, 0, 141800),
(97, 62, 4, 10, 14180, 0, 0, 0, 141800);

--
-- Triggers `pembelian_detail`
--
DELIMITER $$
CREATE TRIGGER `stok_tambah_obat` AFTER INSERT ON `pembelian_detail` FOR EACH ROW BEGIN
    	UPDATE obat SET stok_obat = NEW.jumlah + stok_obat WHERE id_obat = NEW.id_obat;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_obat`
--

CREATE TABLE `pembelian_obat` (
  `id_pembelian_obat` int NOT NULL,
  `id_supplier` int NOT NULL,
  `nomor_faktur` varchar(30) NOT NULL,
  `kode_pembelian` varchar(100) NOT NULL,
  `tanggal_terima` date NOT NULL,
  `waktu_hutang` int NOT NULL,
  `tanggal_jatuh_tempo` date DEFAULT NULL,
  `jenis_beli` varchar(50) NOT NULL,
  `total_dpp` double NOT NULL,
  `total_ppn` double NOT NULL,
  `total_semua` double NOT NULL,
  `tanggal_input` date NOT NULL,
  `id_users` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `pembelian_obat`
--

INSERT INTO `pembelian_obat` (`id_pembelian_obat`, `id_supplier`, `nomor_faktur`, `kode_pembelian`, `tanggal_terima`, `waktu_hutang`, `tanggal_jatuh_tempo`, `jenis_beli`, `total_dpp`, `total_ppn`, `total_semua`, `tanggal_input`, `id_users`) VALUES
(12, 5, '', 'OBA-00000000001', '2021-08-15', 20, '2021-09-04', 'cash', 0, 0, 260000, '2021-08-15', 1),
(13, 5, '', 'OBA-00000000002', '2021-08-18', 20, '2021-09-07', 'cash', 0, 0, 600000, '2021-08-18', 1),
(14, 5, '', 'OBA-00000000003', '2021-08-18', 20, '2021-09-07', 'cash', 0, 0, 600000, '2021-08-18', 1),
(15, 5, '', 'OBA-00000000004', '2021-08-18', 20, '2021-09-07', 'cash', 0, 0, 600000, '2021-08-18', 1),
(16, 5, '', 'OBA-00000000005', '2021-08-18', 20, '2021-09-07', 'cash', 0, 0, 600000, '2021-08-18', 1),
(17, 5, '', 'OBA-00000000006', '2021-08-18', 20, '2021-09-07', 'cash', 0, 0, 600000, '2021-08-18', 1),
(18, 5, '', 'OBA-00000000007', '2021-08-18', 20, '2021-09-07', 'cash', 0, 0, 600000, '2021-08-18', 1),
(19, 5, '', 'OBA-00000000008', '2021-08-18', 20, '2021-09-07', 'cash', 0, 0, 600000, '2021-08-18', 1),
(20, 5, '', 'OBA-00000000009', '2021-08-18', 20, '2021-09-07', 'cash', 0, 0, 600000, '2021-08-18', 1),
(21, 2, '', 'OBA-00000000010', '2021-08-31', 20, '2021-09-20', 'cash', 0, 0, 1000000, '2021-08-31', 1),
(22, 2, '', 'OBA-00000000011', '2021-09-12', 2, '2021-09-14', 'cash', 0, 0, 20000, '2021-09-12', 1),
(23, 2, '', 'OBA-00000000012', '2021-09-12', 2, '2021-09-14', 'cash', 0, 0, 0, '2021-09-12', 1),
(24, 2, '', 'OBA-00000000013', '2021-09-12', 2, '2021-09-14', 'cash', 0, 0, 2000, '2021-09-12', 1),
(25, 2, '', 'OBA-00000000014', '2021-09-12', 1, '2021-09-13', 'cash', 0, 0, 2000, '2021-09-12', 1),
(26, 2, '', 'OBA-00000000015', '2021-10-20', 10, '2021-10-30', 'kredit', 0, 0, 4400, '2021-10-20', 1),
(27, 2, '', 'OBA-00000000016', '2021-10-20', 0, NULL, 'cash', 0, 0, 4400, '2021-10-20', 1),
(28, 2, '', 'OBA-00000000017', '2021-10-20', 0, NULL, 'cash', 0, 0, 4400, '2021-10-20', 1),
(29, 2, '', 'OBA-00000000018', '2021-10-20', 0, NULL, 'cash', 0, 0, 4400, '2021-10-20', 1),
(30, 2, '', 'OBA-00000000019', '2021-11-01', 1, '2021-11-02', 'kredit', 0, 0, 2200, '2021-11-01', 1),
(31, 2, '', 'OBA-00000000020', '2021-10-11', 1, '2021-10-12', 'kredit', 0, 0, 2420, '2021-10-11', 1),
(32, 2, '', 'OBA-00000000021', '2021-10-11', 1, '2021-10-12', 'kredit', 0, 0, 2420, '2021-10-11', 1),
(33, 2, '', 'OBA-00000000022', '2021-12-16', 0, NULL, 'cash', 0, 0, 22000, '2021-12-16', 1),
(34, 2, '', 'OBA-00000000023', '2021-12-17', 0, '2021-12-17', 'cash', 0, 0, 22000, '2021-12-17', 1),
(35, 2, '', 'OBA-00000000024', '2021-12-17', 0, '2021-12-17', 'cash', 0, 0, 22000, '2021-12-17', 1),
(36, 2, '', 'OBA-00000000025', '2022-03-30', 0, '2022-03-30', 'cash', 0, 0, 2409, '2022-03-30', 1),
(37, 2, '', 'OBA-00000000026', '2022-03-31', 0, '2022-03-31', 'cash', 0, 0, 2416, '2022-03-31', 1),
(38, 2, '', 'OBA-00000000027', '2022-04-04', 0, '2022-04-04', 'cash', 0, 0, 24090, '2022-04-04', 1),
(39, 2, '', 'OBA-00000000028', '2022-04-06', 0, '2022-04-06', 'cash', 0, 0, 24086, '2022-04-06', 1),
(40, 2, '', 'OBA-00000000029', '2022-04-06', 0, '2022-04-06', 'cash', 0, 0, 22000, '2022-04-06', 1),
(41, 5, '', 'OBA-00000000030', '2022-04-06', 0, NULL, 'cash', 0, 0, 21809, '2022-04-06', 1),
(42, 2, '', 'OBA-00000000031', '2022-04-13', 0, '2022-04-13', 'cash', 202158.75, 22237.462890625, 224396.21875, '2022-04-13', 1),
(43, 2, '', 'OBA-00000000032', '2022-04-13', 0, '2022-04-13', 'cash', 202158.75, 22237.462890625, 224396.21875, '2022-04-13', 1),
(44, 2, '', 'OBA-00000000033', '2022-04-13', 0, '2022-04-13', 'cash', 202158.765625, 22237.4609375, 224396.21875, '2022-04-13', 1),
(45, 2, '', 'OBA-00000000034', '2022-04-13', 0, '2022-04-13', 'cash', 202158.765625, 22237.4609375, 224396.21875, '2022-04-13', 1),
(46, 2, '', 'OBA-00000000035', '2022-04-13', 0, '2022-04-13', 'cash', 202158.75, 22237.462890625, 224396.21875, '2022-04-13', 1),
(47, 2, '', 'OBA-00000000036', '2022-04-13', 0, '2022-04-13', 'cash', 202158.75, 22237.462890625, 224396.21875, '2022-04-13', 1),
(48, 2, '', 'OBA-00000000037', '2022-04-13', 0, '2022-04-13', 'cash', 202158.75, 22237.4625, 224396.2125, '2022-04-13', 1),
(49, 2, '', 'OBA-00000000038', '2022-04-19', 0, '2022-04-19', 'cash', 127758.9, 14053.479, 141812.379, '2022-04-19', 1),
(53, 2, 'FMM0001', '220640004', '2022-06-21', 2, '2022-06-23', 'kredit', 193000, 0, 193000, '2022-06-21', 1),
(54, 4, 'SI00901', '220640005', '2022-06-21', 10, '2022-07-01', 'kredit', 137500, 0, 137500, '2022-06-21', 1),
(55, 4, 'SI09002', '220640006', '2022-06-21', 10, '2022-07-01', 'kredit', 70900, 0, 70900, '2022-06-21', 1),
(56, 2, 'FMM001', '220660001', '2022-06-24', 0, '2022-06-24', 'konsinyasi', 22200, 0, 22200, '2022-06-24', 1),
(57, 2, 'FMM', '220750001', '2022-07-21', 0, '2022-07-21', 'cash', 152478.7452, 0, 152478.7452, '2022-07-21', 1),
(58, 2, 'FMM', '220750002', '2022-07-21', 0, '2022-07-21', 'cash', 152478.7452, 0, 152478.7452, '2022-07-21', 1),
(59, 2, 'FMM', '220760001', '2022-07-23', 10, '2022-08-02', 'konsinyasi', 133447.1292, 14679.184212, 148126.313412, '2022-07-23', 1),
(60, 2, 'FMM', '220750003', '2022-07-24', 0, '2022-07-24', 'cash', 141800, 0, 141800, '2022-07-24', 1),
(61, 2, 'FMM', '220750004', '2022-07-24', 0, '2022-07-24', 'cash', 141800, 0, 141800, '2022-07-24', 1),
(62, 2, 'FMM', '220750005', '2022-07-24', 0, '2022-07-24', 'cash', 141800, 0, 141800, '2022-07-24', 1);

-- --------------------------------------------------------

--
-- Table structure for table `persen_ppn`
--

CREATE TABLE `persen_ppn` (
  `id_persen_ppn` int NOT NULL,
  `ppn` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `persen_ppn`
--

INSERT INTO `persen_ppn` (`id_persen_ppn`, `ppn`) VALUES
(1, 11);

-- --------------------------------------------------------

--
-- Table structure for table `profile_instansi`
--

CREATE TABLE `profile_instansi` (
  `id_profile_instansi` int NOT NULL,
  `nama_instansi` varchar(100) NOT NULL,
  `alamat_instansi` text NOT NULL,
  `nomor_telepon_instansi` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profile_instansi`
--

INSERT INTO `profile_instansi` (`id_profile_instansi`, `nama_instansi`, `alamat_instansi`, `nomor_telepon_instansi`) VALUES
(1, 'Apotek Bunda Farma', 'Jl. K.H. Wahid Hasyim', '-');

-- --------------------------------------------------------

--
-- Table structure for table `racik_obat`
--

CREATE TABLE `racik_obat` (
  `id_racik_obat` int NOT NULL,
  `id_racik_obat_data` int NOT NULL,
  `nama_racik` varchar(70) NOT NULL,
  `jenis_racik` varchar(10) NOT NULL,
  `jumlah_racik` int NOT NULL,
  `ongkos_racik` int NOT NULL,
  `harga_total_racik` int NOT NULL,
  `keterangan_racik` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `racik_obat`
--

INSERT INTO `racik_obat` (`id_racik_obat`, `id_racik_obat_data`, `nama_racik`, `jenis_racik`, `jumlah_racik`, `ongkos_racik`, `harga_total_racik`, `keterangan_racik`) VALUES
(48, 1, 'Obat Racik 1', 'non-dtd', 12, 0, 400000, ''),
(49, 2, 'Obat Racik 1', 'non-dtd', 12, 0, 400000, ''),
(50, 3, 'Obat Racik 1', 'non-dtd', 12, 0, 400000, ''),
(51, 3, 'Obat Racik 2', 'non-dtd', 10, 0, 200000, ''),
(53, 5, 'Obat Racik 1', 'non-dtd', 10, 0, 200000, ''),
(54, 6, 'Obat Racik 1', 'non-dtd', 10, 0, 1430000, ''),
(55, 7, 'Obat Racik 1', 'non-dtd', 10, 10000, 1353000, ''),
(56, 8, 'Obat Racik 1', 'non-dtd', 10, 10000, 1353000, ''),
(57, 9, 'Obat Racikan 1', 'non-dtd', 10, 10000, 1353000, ''),
(58, 10, 'Obat Racik 1', 'non-dtd', 10, 1000, 51000, ''),
(59, 11, 'Obat Racik 1', 'non-dtd', 10, 1000, 51000, ''),
(60, 12, 'Obat Racik 1', 'non-dtd', 10, 1000, 51000, ''),
(61, 13, 'Obat Racik 1', 'non-dtd', 10, 1000, 61000, ''),
(62, 15, 'Obat Tanpa Racik 1', '-', 0, 0, 40000, ''),
(63, 16, 'Obat Racik 1', 'non-dtd', 10, 1000, 41000, ''),
(64, 17, 'Obat Racik 1', 'non-dtd', 10, 1000, 41000, ''),
(65, 18, 'Obat Racik 1', 'non-dtd', 10, 1000, 41000, ''),
(66, 19, 'Obat Racik 1', 'non-dtd', 10, 1000, 41000, ''),
(67, 20, 'Obat Racik 1', 'non-dtd', 100, 1000, 221000, ''),
(68, 21, 'Obat Tanpa Racik 1', '-', 0, 0, 40000, ''),
(69, 22, 'Obat Racik 1', 'non-dtd', 10, 1000, 31000, ''),
(70, 23, 'Obat Racik 1', 'non-dtd', 10, 1000, 31000, ''),
(71, 24, 'Obat Tanpa Racik 1', '-', 0, 0, 30000, ''),
(72, 25, 'Obat Tanpa Racik', '-', 0, 0, 28000, ''),
(73, 26, 'Obat Tanpa Racik', '-', 0, 0, 3000, ''),
(74, 27, 'Obat Tanpa Racik', '-', 0, 0, 3000, ''),
(75, 28, 'Obat Tanpa Racik', '-', 0, 0, 3000, ''),
(76, 29, 'Obat Tanpa Racik', '-', 0, 0, 25000, ''),
(77, 30, 'Obat Racik 1', 'non-dtd', 1, 1000, 4000, ''),
(78, 30, 'Obat Tanpa Racik', '-', 0, 0, 3000, ''),
(79, 31, 'Obat Racik 1', 'non-dtd', 1, 1000, 4000, ''),
(80, 31, 'Obat Tanpa Racik', '-', 0, 0, 6000, ''),
(81, 32, 'Obat Tanpa Racik', '-', 0, 0, 3000, ''),
(82, 33, 'Obat Tanpa Racik', '-', 0, 0, 3000, ''),
(83, 34, 'Obat Tanpa Racik', '-', 0, 0, 9000, ''),
(84, 35, 'Obat Tanpa Racik', '-', 0, 0, 9000, ''),
(85, 36, 'Obat Tanpa Racik', '-', 0, 0, 9000, ''),
(86, 37, 'Obat Tanpa Racik', '-', 0, 0, 9000, ''),
(87, 37, 'Obat Racik 1', 'non-dtd', 1, 1000, 51000, ''),
(88, 38, 'Obat Racik 1', 'non-dtd', 1, 1000, 4000, ''),
(89, 38, 'Obat Racik 3', 'non-dtd', 1, 1000, 4000, ''),
(90, 39, 'Obat Racik 1', 'non-dtd', 1, 1000, 4000, ''),
(91, 39, 'Obat Racik 3', 'non-dtd', 1, 1000, 4000, ''),
(92, 40, 'Obat Tanpa Racik', '-', 0, 0, 3000, ''),
(93, 41, 'Obat Racik 1', 'non-dtd', 1, 1000, 4000, ''),
(94, 41, 'Obat Tanpa Racik', '-', 0, 0, 3000, ''),
(95, 42, 'Obat Tanpa Racik', '-', 0, 0, 3000, ''),
(96, 43, 'Obat Tanpa Racik', '-', 0, 0, 25000, ''),
(97, 43, 'Obat Racik 1', 'non-dtd', 1, 1000, 4000, ''),
(98, 43, 'Obat Racik 1', 'non-dtd', 1, 1000, 4000, ''),
(99, 44, 'Obat Racik 1', 'non-dtd', 1, 1000, 4000, ''),
(100, 44, 'Obat Racik 2', 'non-dtd', 2, 1000, 4000, ''),
(101, 44, 'Obat Tanpa Racik', '-', 0, 0, 28000, ''),
(102, 45, 'Obat Tanpa Racik', '-', 0, 0, 3000, ''),
(103, 46, 'Obat Racik 1', 'non-dtd', 10, 1000, 4000, ''),
(104, 46, 'Obat Tanpa Racik', '-', 0, 0, 6000, ''),
(105, 47, 'Obat Racik 1', 'non-dtd', 1, 1000, 51000, ''),
(106, 47, 'Obat Tanpa Racik', '-', 0, 0, 267000, ''),
(107, 48, 'Obat Racik 1', 'non-dtd', 1, 1000, 7000, ''),
(108, 48, 'Obat Tanpa Racik', '-', 0, 0, 28000, ''),
(109, 49, 'Obat Tanpa Racik', '-', 0, 0, 3000, ''),
(110, 50, 'Obat Racik 1', 'non-dtd', 1, 1000, 4000, ''),
(111, 51, 'Obat Racik 1', 'non-dtd', 1, 1000, 7000, ''),
(112, 51, 'Obat Tanpa Racik', '-', 0, 0, 3000, ''),
(113, 52, 'Obat Tanpa Racik', '-', 0, 0, 7000, ''),
(114, 53, 'Obat Tanpa Racik', '-', 0, 0, 6000, ''),
(115, 54, 'Obat Tanpa Racik', '-', 0, 0, 6000, ''),
(116, 55, 'Obat Tanpa Racik', '-', 0, 0, 6000, ''),
(117, 56, 'Obat Tanpa Racik', '-', 0, 0, 6000, ''),
(118, 57, 'Obat Tanpa Racik', '-', 0, 0, 6000, ''),
(119, 58, 'Obat Tanpa Racik', '-', 0, 0, 6000, ''),
(120, 59, 'Obat Tanpa Racik', '-', 0, 0, 4000, ''),
(121, 59, 'Obat Racik 1', 'non-dtd', 1, 1000, 5000, ''),
(122, 60, 'Obat Tanpa Racik', '-', 0, 0, 4000, ''),
(123, 61, 'Obat Tanpa Racik', '-', 0, 0, 4000, ''),
(124, 62, 'Obat Tanpa Racik', '-', 0, 0, 4000, ''),
(125, 63, 'Obat Tanpa Racik', '-', 0, 0, 4000, ''),
(126, 64, 'Obat Tanpa Racik', '-', 0, 0, 4000, ''),
(127, 65, 'Obat Tanpa Racik', '-', 0, 0, 4000, ''),
(128, 66, 'Obat Tanpa Racik', '-', 0, 0, 4000, ''),
(129, 67, 'Obat Tanpa Racik', '-', 0, 0, 4000, ''),
(130, 68, 'Obat Tanpa Racik', '-', 0, 0, 10000, ''),
(131, 69, 'Obat Tanpa Racik', '-', 0, 0, 10000, ''),
(132, 70, 'Obat Tanpa Racik', '-', 0, 0, 10000, ''),
(133, 71, 'Obat Racik 1', 'non-dtd', 1, 1000, 8000, ''),
(134, 71, 'Obat Tanpa Racik', '-', 0, 0, 6000, ''),
(135, 72, 'Obat Racik 1', 'non-dtd', 1, 1000, 8000, ''),
(136, 72, 'Obat Tanpa Racik', '-', 0, 0, 6000, ''),
(137, 73, 'Obat Racik 1', 'non-dtd', 1, 1000, 8000, ''),
(138, 73, 'Obat Tanpa Racik', '-', 0, 0, 6000, ''),
(139, 74, 'Obat Racik 1', 'non-dtd', 1, 1000, 8000, ''),
(140, 74, 'Obat Tanpa Racik', '-', 0, 0, 6000, ''),
(141, 75, 'Obat Racik 1', 'non-dtd', 1, 1000, 8000, ''),
(142, 75, 'Obat Tanpa Racik', '-', 0, 0, 6000, ''),
(143, 76, 'Obat Racik 1', 'non-dtd', 1, 1000, 8000, ''),
(144, 76, 'Obat Tanpa Racik', '-', 0, 0, 6000, ''),
(145, 77, 'Obat Racik 1', 'non-dtd', 1, 1000, 8000, ''),
(146, 77, 'Obat Tanpa Racik', '-', 0, 0, 6000, ''),
(147, 78, 'Obat Tanpa Racik', '-', 0, 0, 13000, ''),
(148, 79, 'Obat Tanpa Racik', '-', 0, 0, 13000, ''),
(149, 80, 'Obat Tanpa Racik', '-', 0, 0, 13000, ''),
(150, 81, 'Obat Tanpa Racik', '-', 0, 0, 13000, ''),
(151, 82, 'Obat Racik Mantap', 'non-dtd', 10, 1000, 13000, ''),
(152, 82, 'Obat Tanpa Racik', '-', 0, 0, 20000, ''),
(153, 83, 'Obat Racik Mantap', 'non-dtd', 10, 1000, 13000, ''),
(154, 83, 'Obat Tanpa Racik', '-', 0, 0, 20000, ''),
(155, 84, 'Obat Tanpa Racik', '-', 0, 0, 8000, ''),
(156, 85, 'Test Racik', 'non-dtd', 1, 1000, 8000, ''),
(157, 85, 'Obat Tanpa Racik', '-', 0, 0, 30000, ''),
(158, 86, 'Test Racik', 'non-dtd', 1, 1000, 8000, ''),
(159, 86, 'Obat Tanpa Racik', '-', 0, 0, 30000, ''),
(160, 87, 'Test Racik', 'non-dtd', 1, 1000, 8000, ''),
(161, 87, 'Obat Tanpa Racik', '-', 0, 0, 30000, ''),
(162, 88, 'Test Racik', 'non-dtd', 1, 1000, 8000, ''),
(163, 88, 'Obat Tanpa Racik', '-', 0, 0, 30000, ''),
(164, 89, 'Test Racik', 'non-dtd', 1, 1000, 8000, ''),
(165, 89, 'Obat Tanpa Racik', '-', 0, 0, 30000, ''),
(166, 90, 'Test Racik', 'non-dtd', 1, 1000, 8000, ''),
(167, 90, 'Obat Tanpa Racik', '-', 0, 0, 30000, ''),
(168, 91, 'Test Racik', 'non-dtd', 1, 1000, 8000, ''),
(169, 91, 'Obat Tanpa Racik', '-', 0, 0, 30000, ''),
(170, 92, 'Obat Tanpa Racik', '-', 0, 0, 423000, ''),
(171, 93, 'Obat Tanpa Racik', '-', 0, 0, 142000, ''),
(172, 94, 'Obat Tanpa Racik', '-', 0, 0, 142000, ''),
(173, 95, 'Obat Tanpa Racik', '-', 0, 0, 142000, ''),
(174, 96, 'Obat Tanpa Racik', '-', 0, 0, 142000, ''),
(175, 97, 'Obat Tanpa Racik', '-', 0, 0, 26000, ''),
(176, 98, 'Obat Tanpa Racik', '-', 0, 0, 35000, ''),
(177, 99, 'Obat Tanpa Racik', '-', 0, 0, 141000, ''),
(178, 100, 'Obat Tanpa Racik', '-', 0, 0, 141000, ''),
(179, 101, 'Obat Tanpa Racik', '-', 0, 0, 141000, ''),
(180, 102, 'Obat Tanpa Racik', '-', 0, 0, 141000, ''),
(181, 103, 'Obat Tanpa Racik', '-', 0, 0, 22000, ''),
(182, 104, 'Obat Tanpa Racik', '-', 0, 0, 220000, ''),
(183, 105, 'Obat Tanpa Racik', '-', 0, 0, 123000, ''),
(184, 106, 'Obat Tanpa Racik', '-', 0, 0, 123000, ''),
(185, 107, 'Obat Tanpa Racik', '-', 0, 0, 11000, ''),
(186, 108, 'Obat Tanpa Racik', '-', 0, 0, 123000, ''),
(187, 109, 'Obat Tanpa Racik', '-', 0, 0, 5000, ''),
(188, 110, 'Obat Tanpa Racik', '-', 0, 0, 5000, ''),
(189, 111, 'Obat Tanpa Racik', '-', 0, 0, 26000, ''),
(190, 112, 'Obat Tanpa Racik', '-', 0, 0, 15000, ''),
(191, 113, 'Obat Tanpa Racik', '-', 0, 0, 15000, ''),
(192, 114, 'Obat Tanpa Racik', '-', 0, 0, 15000, ''),
(193, 115, 'Obat Tanpa Racik', '-', 0, 0, 15000, ''),
(194, 117, 'Racik Puyer', 'non-dtd', 10, 1000, 26000, 'BUNGKUS'),
(195, 118, 'Racik Puyer', 'non-dtd', 10, 1000, 26000, 'BUNGKUS'),
(196, 119, 'Racik Puyer', 'non-dtd', 10, 1000, 26000, 'BUNGKUS'),
(197, 120, 'Racik Puyer', 'non-dtd', 10, 1000, 26000, 'BUNGKUS'),
(198, 121, 'Racik Puyer', 'non-dtd', 10, 1000, 26000, 'BUNGKUS'),
(199, 122, 'Racik Puyer', 'non-dtd', 10, 1000, 26000, 'BUNGKUS'),
(200, 123, 'Racik Puyer', 'non-dtd', 10, 1000, 26000, 'BUNGKUS'),
(201, 124, 'Obat Racik 1', 'non-dtd', 10, 1000, 221000, 'PCS'),
(202, 125, 'Obat Racik 1', 'non-dtd', 10, 1000, 23000, 'POT'),
(203, 125, 'Obat Racik 2', 'non-dtd', 10, 1000, 221000, 'BTL'),
(204, 125, 'Obat Tanpa Racik', '-', 0, 0, 1663000, '-'),
(205, 133, 'Obat Tanpa Racik', '-', 0, 0, 0, '-'),
(206, 134, 'Obat Tanpa Racik', '-', 0, 0, 0, '-'),
(207, 135, 'Obat Racik 1', 'non-dtd', 1, 1000, 43000, 'PCS'),
(208, 135, 'Obat Tanpa Racik', '-', 0, 0, 20000, '-'),
(209, 135, 'Obat Racik 2', 'non-dtd', 1, 1000, 20000, 'POT'),
(210, 136, 'Obat Racik 1', 'non-dtd', 1, 1000, 43000, 'PCS'),
(211, 136, 'Obat Tanpa Racik', '-', 0, 0, 46000, '-');

-- --------------------------------------------------------

--
-- Table structure for table `racik_obat_data`
--

CREATE TABLE `racik_obat_data` (
  `id_racik_obat_data` int NOT NULL,
  `tanggal_racik` date NOT NULL,
  `id_pasien` int NOT NULL,
  `id_dokter` int NOT NULL,
  `total_semua` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `racik_obat_data`
--

INSERT INTO `racik_obat_data` (`id_racik_obat_data`, `tanggal_racik`, `id_pasien`, `id_dokter`, `total_semua`) VALUES
(1, '2021-07-27', 18, 1, 0),
(2, '2021-07-27', 18, 1, 600000),
(3, '2021-07-27', 18, 1, 600000),
(5, '2021-08-03', 18, 2, 200000),
(6, '2021-08-04', 22, 1, 1430000),
(7, '2021-08-10', 21, 1, 1363000),
(8, '2021-08-10', 21, 1, 1363000),
(9, '2021-08-12', 19, 2, 1363000),
(10, '2021-08-13', 19, 2, 52000),
(11, '2021-08-13', 19, 2, 52000),
(12, '2021-08-13', 19, 2, 52000),
(13, '2021-08-15', 19, 2, 61000),
(14, '2021-08-17', 20, 2, 40000),
(15, '2021-08-17', 20, 2, 40000),
(16, '2021-08-17', 20, 2, 41000),
(17, '2021-08-17', 20, 2, 41000),
(18, '2021-08-17', 20, 2, 41000),
(19, '2021-08-17', 20, 2, 41000),
(20, '2021-08-18', 20, 2, 221000),
(21, '2021-08-18', 19, 2, 40000),
(22, '2021-08-19', 20, 2, 31000),
(23, '2021-08-19', 20, 2, 31000),
(24, '2021-08-19', 20, 2, 30000),
(25, '2021-08-21', 18, 1, 34000),
(26, '2021-08-21', 19, 2, 9000),
(27, '2021-08-21', 19, 2, 9000),
(28, '2021-08-21', 19, 2, 9000),
(29, '2021-08-21', 18, 2, 3000),
(30, '2021-08-24', 22, 2, 7000),
(31, '2021-08-24', 20, 2, 10000),
(32, '2021-08-24', 18, 2, 3000),
(33, '2021-08-25', 18, 2, 6000),
(34, '2021-08-25', 18, 2, 6000),
(35, '2021-08-25', 18, 2, 6000),
(36, '2021-08-25', 18, 2, 6000),
(37, '2021-08-25', 18, 2, 57000),
(38, '2021-08-25', 18, 2, 8000),
(39, '2021-08-25', 18, 2, 8000),
(40, '2021-08-25', 18, 1, 2700),
(41, '2021-08-26', 22, 2, 7000),
(42, '2021-08-26', 18, 2, 28000),
(43, '2021-08-26', 18, 2, 14000),
(44, '2021-08-26', 18, 2, 36000),
(45, '2021-08-26', 21, 1, 3000),
(46, '2021-09-07', 22, 2, 10000),
(47, '2021-09-10', 18, 2, 159000),
(48, '2021-09-10', 20, 2, 35000),
(49, '2021-09-14', 19, 1, 3000),
(50, '2021-09-23', 19, 2, 4000),
(51, '2021-09-30', 18, 2, 10000),
(52, '2021-10-19', 18, 1, 7000),
(53, '2021-10-20', 18, 2, 6000),
(54, '2021-10-20', 18, 2, 6000),
(55, '2021-10-20', 18, 2, 6000),
(56, '2021-10-20', 18, 2, 6000),
(57, '2021-10-20', 18, 2, 6000),
(58, '2021-10-20', 18, 2, 6000),
(59, '2021-10-28', 18, 1, 9000),
(60, '2021-10-29', 18, 1, 4000),
(61, '2021-10-29', 18, 1, 4000),
(62, '2021-10-29', 18, 1, 4000),
(63, '2021-10-29', 18, 1, 4000),
(64, '2021-10-29', 18, 1, 4000),
(65, '2021-10-29', 18, 1, 4000),
(66, '2021-10-29', 18, 1, 4000),
(67, '2021-10-29', 18, 1, 4000),
(68, '2021-10-29', 18, 1, 9000),
(69, '2021-10-29', 18, 1, 9000),
(70, '2021-10-29', 18, 1, 9000),
(71, '2022-04-08', 18, 1, 18000),
(72, '2022-04-09', 18, 1, 18000),
(73, '2022-04-09', 18, 1, 18000),
(74, '2022-04-09', 18, 1, 18000),
(75, '2022-04-09', 18, 1, 18000),
(76, '2022-04-09', 18, 1, 18000),
(77, '2022-04-09', 18, 1, 18000),
(78, '2022-04-12', 18, 2, 12000),
(79, '2022-04-12', 18, 2, 12000),
(80, '2022-04-12', 18, 2, 12000),
(81, '2022-04-12', 18, 2, 12000),
(82, '2022-04-12', 22, 2, 34000),
(83, '2022-04-12', 22, 2, 34000),
(84, '2022-04-12', 18, 2, 8000),
(85, '2022-04-12', 18, 1, 39000),
(86, '2022-04-12', 18, 1, 39000),
(87, '2022-04-12', 18, 1, 39000),
(88, '2022-04-12', 18, 1, 39000),
(89, '2022-04-12', 18, 1, 39000),
(90, '2022-04-12', 18, 1, 39000),
(91, '2022-04-12', 18, 1, 39000),
(92, '2022-05-28', 18, 2, 423000),
(93, '2022-06-02', 18, 1, 142000),
(94, '2022-06-02', 18, 1, 142000),
(95, '2022-06-02', 18, 1, 142000),
(96, '2022-06-02', 18, 1, 142000),
(97, '2022-06-02', 18, 1, 26000),
(98, '2022-06-10', 18, 1, 35000),
(99, '2022-06-24', 18, 1, 141000),
(100, '2022-06-24', 18, 1, 141000),
(101, '2022-06-24', 18, 1, 141000),
(102, '2022-06-24', 18, 1, 141000),
(103, '2022-06-24', 18, 1, 22000),
(104, '2022-06-24', 18, 1, 220000),
(105, '2022-06-25', 18, 1, 123000),
(106, '2022-06-25', 19, 2, 123000),
(107, '2022-06-25', 18, 1, 11000),
(108, '2022-06-25', 18, 1, 123000),
(109, '2022-06-27', 18, 1, 5000),
(110, '2022-07-11', 18, 2, 5000),
(111, '2022-07-11', 20, 1, 26000),
(112, '2022-07-22', 18, 1, 15000),
(113, '2022-07-22', 18, 1, 15000),
(114, '2022-07-22', 18, 1, 15000),
(115, '2022-07-22', 18, 1, 15000),
(116, '2022-07-22', 18, 1, 27000),
(117, '2022-07-22', 18, 1, 27000),
(118, '2022-07-22', 18, 1, 27000),
(119, '2022-07-22', 18, 1, 27000),
(120, '2022-07-22', 18, 1, 27000),
(121, '2022-07-22', 18, 1, 27000),
(122, '2022-07-22', 18, 1, 27000),
(123, '2022-07-22', 18, 1, 27000),
(124, '2022-07-23', 18, 1, 222000),
(125, '2022-07-23', 18, 1, 1909000),
(126, '2022-08-21', 18, 1, 58000),
(127, '2022-08-21', 18, 1, 58000),
(128, '2022-08-21', 18, 1, 58000),
(129, '2022-08-21', 18, 1, 211000),
(130, '2022-08-21', 18, 1, 70000),
(131, '2022-08-21', 18, 1, 70000),
(132, '2022-08-21', 18, 1, 70000),
(133, '2022-08-21', 18, 1, 225000),
(134, '2022-08-21', 18, 1, 225000),
(135, '2022-08-21', 18, 1, 99000),
(136, '2022-08-21', 18, 1, 90000);

-- --------------------------------------------------------

--
-- Table structure for table `racik_obat_detail`
--

CREATE TABLE `racik_obat_detail` (
  `id_racik_obat_detail` int NOT NULL,
  `id_racik_obat` int NOT NULL,
  `id_obat` int NOT NULL,
  `id_supplier` int NOT NULL,
  `jumlah` varchar(10) NOT NULL,
  `embalase` int NOT NULL,
  `sub_total` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `racik_obat_detail`
--

INSERT INTO `racik_obat_detail` (`id_racik_obat_detail`, `id_racik_obat`, `id_obat`, `id_supplier`, `jumlah`, `embalase`, `sub_total`) VALUES
(22, 48, 4, 2, '10', 0, 200000),
(23, 48, 5, 2, '10', 0, 200000),
(24, 49, 4, 2, '10', 0, 200000),
(25, 49, 5, 2, '10', 0, 200000),
(26, 50, 4, 2, '10', 0, 200000),
(27, 50, 5, 2, '10', 0, 200000),
(28, 51, 4, 2, '10', 0, 200000),
(31, 53, 4, 2, '10', 0, 200000),
(32, 54, 39, 6, '10', 0, 1230000),
(33, 54, 4, 2, '10', 0, 200000),
(34, 55, 39, 6, '10', 0, 1353000),
(35, 56, 39, 6, '10', 0, 1353000),
(36, 57, 39, 6, '10', 0, 1353000),
(37, 58, 40, 5, '10', 0, 51000),
(38, 59, 40, 5, '10', 0, 51000),
(39, 60, 40, 5, '10', 0, 51000),
(40, 61, 5, 4, '10', 0, 30000),
(41, 61, 7, 4, '10', 0, 30000),
(42, 62, 9, 2, '10', 0, 20000),
(43, 62, 5, 4, '10', 0, 20000),
(44, 63, 5, 4, '10', 0, 20000),
(45, 63, 5, 4, '10', 0, 20000),
(46, 64, 5, 4, '10', 0, 20000),
(47, 64, 5, 4, '10', 0, 20000),
(48, 65, 5, 4, '10', 0, 20000),
(49, 65, 5, 4, '10', 0, 20000),
(50, 66, 5, 4, '10', 0, 20000),
(51, 66, 5, 4, '10', 0, 20000),
(52, 67, 4, 2, '10', 0, 20000),
(53, 67, 5, 2, '100', 0, 200000),
(54, 68, 5, 2, '10', 0, 20000),
(55, 68, 7, 2, '10', 0, 20000),
(56, 69, 41, 5, '10', 0, 30000),
(57, 70, 41, 5, '10', 0, 30000),
(58, 71, 41, 5, '10', 0, 30000),
(59, 72, 4, 2, '1', 0, 3000),
(60, 72, 7, 2, '1', 0, 3000),
(61, 72, 9, 4, '1', 0, 3000),
(62, 72, 18, 4, '1', 0, 25000),
(63, 73, 7, 2, '1', 0, 3000),
(64, 73, 4, 2, '1', 0, 3000),
(65, 73, 9, 4, '1', 0, 3000),
(66, 74, 7, 2, '1', 0, 3000),
(67, 74, 4, 2, '1', 0, 3000),
(68, 74, 9, 4, '1', 0, 3000),
(69, 75, 7, 2, '1', 0, 3000),
(70, 75, 4, 2, '1', 0, 3000),
(71, 75, 9, 4, '1', 0, 3000),
(72, 76, 4, 2, '1', 0, 3000),
(73, 77, 4, 2, '1', 0, 3000),
(74, 78, 4, 2, '1', 0, 3000),
(75, 79, 4, 2, '1', 0, 3000),
(76, 80, 7, 4, '1', 0, 3000),
(77, 80, 9, 4, '1', 0, 3000),
(78, 81, 4, 4, '1', 0, 3000),
(79, 82, 4, 2, '1', 0, 3000),
(80, 82, 9, 4, '1', 0, 3000),
(81, 83, 4, 2, '1', 0, 3000),
(82, 83, 9, 4, '1', 0, 3000),
(83, 84, 4, 2, '1', 0, 3000),
(84, 84, 9, 4, '1', 0, 3000),
(85, 85, 4, 2, '1', 0, 3000),
(86, 85, 9, 4, '1', 0, 3000),
(87, 86, 4, 2, '1', 0, 3000),
(88, 86, 9, 4, '1', 0, 3000),
(89, 87, 18, 4, '1', 0, 25000),
(90, 87, 24, 2, '1', 0, 25000),
(91, 88, 4, 4, '1', 0, 3000),
(92, 89, 9, 2, '1', 0, 3000),
(93, 90, 4, 4, '1', 0, 3000),
(94, 91, 9, 2, '1', 0, 3000),
(95, 92, 4, 2, '1', 0, 3000),
(96, 93, 4, 4, '1', 0, 3000),
(97, 94, 4, 4, '1', 0, 3000),
(98, 95, 4, 2, '1', 0, 3000),
(99, 95, 18, 4, '1', 0, 25000),
(100, 96, 4, 4, '1', 0, 3000),
(101, 96, 9, 2, '1', 0, 3000),
(102, 97, 4, 4, '1', 0, 3000),
(103, 98, 4, 2, '1', 0, 3000),
(104, 99, 4, 2, '1', 0, 3000),
(105, 100, 4, 4, '1', 0, 3000),
(106, 101, 18, 4, '1', 0, 25000),
(107, 101, 9, 4, '1', 0, 3000),
(108, 102, 4, 4, '1', 0, 3000),
(109, 103, 4, 4, '1', 0, 3000),
(110, 104, 7, 4, '1', 0, 3000),
(111, 104, 9, 2, '1', 0, 3000),
(112, 105, 4, 4, '10', 0, 25000),
(113, 105, 7, 2, '10', 0, 25000),
(114, 106, 9, 2, '10', 0, 25000),
(115, 106, 18, 4, '10', 0, 242000),
(116, 107, 4, 4, '1', 0, 3000),
(117, 107, 7, 2, '1', 0, 3000),
(118, 108, 9, 2, '1', 0, 3000),
(119, 108, 18, 2, '1', 0, 25000),
(120, 109, 4, 2, '1', 0, 3000),
(121, 110, 4, 4, '1', 0, 3000),
(122, 111, 4, 2, '1', 0, 3000),
(123, 111, 7, 4, '1', 0, 3000),
(124, 112, 7, 2, '1', 0, 3000),
(125, 113, 4, 4, '2', 2000, 5000),
(126, 114, 4, 2, '2', 1000, 5000),
(127, 115, 4, 2, '2', 1000, 5000),
(128, 116, 4, 2, '2', 1000, 5000),
(129, 117, 4, 2, '2', 1000, 5000),
(130, 118, 4, 2, '2', 1000, 5000),
(131, 119, 4, 4, '2', 1000, 5000),
(132, 120, 4, 2, '1', 1000, 3000),
(133, 121, 4, 4, '1', 1000, 3000),
(134, 122, 4, 4, '1', 1000, 3000),
(135, 123, 4, 4, '1', 1000, 3000),
(136, 124, 4, 4, '1', 1000, 3000),
(137, 125, 4, 4, '1', 1000, 3000),
(138, 126, 4, 4, '1', 1000, 3000),
(139, 127, 4, 4, '1', 1000, 3000),
(140, 128, 4, 4, '1', 1000, 3000),
(141, 129, 4, 4, '1', 1000, 3000),
(142, 130, 7, 4, '1', 1000, 3000),
(143, 130, 40, 2, '1', 1000, 5000),
(144, 131, 7, 4, '1', 1000, 3000),
(145, 131, 40, 2, '1', 1000, 5000),
(146, 132, 7, 4, '1', 1000, 3000),
(147, 132, 40, 2, '1', 1000, 5000),
(148, 133, 4, 2, '2', 1000, 6000),
(149, 134, 7, 2, '2', 1000, 5000),
(150, 134, 9, 4, '2', 1000, 5000),
(151, 135, 4, 2, '2', 1000, 6000),
(152, 136, 7, 2, '2', 1000, 5000),
(153, 136, 9, 4, '2', 1000, 5000),
(154, 137, 4, 2, '2', 1000, 6000),
(155, 138, 7, 2, '2', 1000, 5000),
(156, 138, 9, 4, '2', 1000, 5000),
(157, 139, 4, 2, '2', 1000, 6000),
(158, 140, 7, 2, '2', 1000, 5000),
(159, 140, 9, 4, '2', 1000, 5000),
(160, 141, 4, 2, '2', 1000, 6000),
(161, 142, 7, 2, '2', 1000, 5000),
(162, 142, 9, 4, '2', 1000, 5000),
(163, 143, 4, 2, '2', 1000, 6000),
(164, 144, 7, 2, '2', 1000, 5000),
(165, 144, 9, 4, '2', 1000, 5000),
(166, 145, 4, 2, '2', 1000, 6000),
(167, 146, 7, 2, '2', 1000, 5000),
(168, 146, 9, 4, '2', 1000, 5000),
(169, 147, 4, 4, '2', 1000, 6000),
(170, 147, 7, 2, '2', 1000, 5000),
(171, 148, 4, 4, '2', 1000, 6000),
(172, 148, 7, 2, '2', 1000, 5000),
(173, 149, 4, 4, '2', 1000, 6000),
(174, 149, 7, 2, '2', 1000, 5000),
(175, 150, 4, 4, '2', 1000, 6000),
(176, 150, 7, 2, '2', 1000, 5000),
(177, 151, 4, 2, '2', 1000, 6000),
(178, 151, 9, 4, '2', 1000, 5000),
(179, 152, 4, 4, '1', 2000, 3000),
(180, 152, 7, 2, '5', 2000, 13000),
(181, 153, 4, 2, '2', 1000, 6000),
(182, 153, 9, 4, '2', 1000, 5000),
(183, 154, 4, 4, '1', 2000, 3000),
(184, 154, 7, 2, '5', 2000, 13000),
(185, 155, 4, 2, '1', 1000, 3000),
(186, 155, 7, 4, '1', 1000, 3000),
(187, 156, 4, 2, '1', 1000, 3000),
(188, 156, 7, 2, '1', 1000, 3000),
(189, 157, 9, 4, '1', 1000, 3000),
(190, 157, 18, 2, '1', 1000, 25000),
(191, 158, 4, 2, '1', 1000, 3000),
(192, 158, 7, 2, '1', 1000, 3000),
(193, 159, 9, 4, '1', 1000, 3000),
(194, 159, 18, 2, '1', 1000, 25000),
(195, 160, 4, 2, '1', 1000, 3000),
(196, 160, 7, 2, '1', 1000, 3000),
(197, 161, 9, 4, '1', 1000, 3000),
(198, 161, 18, 2, '1', 1000, 25000),
(199, 162, 4, 2, '1', 1000, 3000),
(200, 162, 7, 2, '1', 1000, 3000),
(201, 163, 9, 4, '1', 1000, 3000),
(202, 163, 18, 2, '1', 1000, 25000),
(203, 164, 4, 2, '1', 1000, 3000),
(204, 164, 7, 2, '1', 1000, 3000),
(205, 165, 9, 4, '1', 1000, 3000),
(206, 165, 18, 2, '1', 1000, 25000),
(207, 166, 4, 2, '1', 1000, 3000),
(208, 166, 7, 2, '1', 1000, 3000),
(209, 167, 9, 4, '1', 1000, 3000),
(210, 167, 18, 2, '1', 1000, 25000),
(211, 168, 4, 2, '1', 1000, 3000),
(212, 168, 7, 2, '1', 1000, 3000),
(213, 169, 9, 4, '1', 1000, 3000),
(214, 169, 18, 2, '1', 1000, 25000),
(215, 170, 4, 2, '10', 0, 141000),
(216, 170, 7, 4, '10', 1000, 25000),
(217, 170, 9, 2, '2', 1000, 5000),
(218, 170, 18, 2, '10', 5000, 245000),
(219, 171, 4, 4, '10', 1000, 141000),
(220, 172, 4, 2, '10', 1000, 141000),
(221, 173, 4, 2, '10', 1000, 141000),
(222, 174, 4, 2, '10', 1000, 141000),
(223, 175, 7, 4, '10', 1000, 25000),
(224, 176, 7, 2, '10', 10000, 25000),
(225, 177, 4, 4, '10', 0, 141000),
(226, 178, 4, 4, '10', 0, 141000),
(227, 179, 4, 4, '10', 0, 141000),
(228, 180, 4, 2, '10', 0, 141000),
(229, 181, 9, 4, '10', 0, 22000),
(230, 182, 18, 2, '10', 0, 220000),
(231, 183, 30, 2, '10', 0, 123000),
(232, 184, 30, 2, '10', 0, 123000),
(233, 185, 9, 4, '5', 0, 11000),
(234, 186, 30, 2, '10', 0, 123000),
(235, 187, 9, 4, '2', 0, 5000),
(236, 188, 5, 4, '2', 0, 5000),
(237, 189, 9, 2, '1', 0, 3000),
(238, 189, 18, 4, '1', 1000, 22000),
(239, 190, 4, 2, '1', 0, 15000),
(240, 191, 4, 2, '1', 0, 15000),
(241, 192, 4, 2, '1', 0, 15000),
(242, 193, 4, 2, '1', 0, 15000),
(243, 194, 7, 4, '10', 1000, 25000),
(244, 195, 7, 4, '10', 1000, 25000),
(245, 196, 7, 4, '10', 1000, 25000),
(246, 197, 7, 4, '10', 1000, 25000),
(247, 198, 7, 4, '10', 1000, 25000),
(248, 199, 7, 4, '10', 1000, 25000),
(249, 200, 7, 4, '10', 1000, 25000),
(250, 201, 18, 2, '10', 1000, 220000),
(251, 202, 9, 4, '10', 1000, 22000),
(252, 203, 18, 4, '10', 1000, 220000),
(253, 204, 39, 6, '10', 10000, 1653000),
(254, 206, 30, 2, '1', 1000, 13000),
(255, 206, 39, 6, '1', 1000, 166000),
(256, 207, 4, 2, '1', 1000, 16000),
(257, 207, 18, 4, '1', 1000, 25000),
(258, 208, 30, 2, '1', 1000, 13000),
(259, 208, 41, 5, '1', 1000, 19000),
(260, 209, 41, 5, '1', 1000, 19000),
(261, 210, 4, 2, '1', 1000, 16000),
(262, 210, 24, 4, '1', 1000, 25000),
(263, 211, 40, 2, '1', 1000, 5000),
(264, 211, 18, 2, '1', 1000, 25000),
(265, 211, 29, 2, '1', 1000, 13000);

--
-- Triggers `racik_obat_detail`
--
DELIMITER $$
CREATE TRIGGER `kurang_stok_by_racik` AFTER INSERT ON `racik_obat_detail` FOR EACH ROW BEGIN
	UPDATE obat SET stok_obat = stok_obat - NEW.jumlah WHERE id_obat = NEW.id_obat;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `racik_obat_sementara`
--

CREATE TABLE `racik_obat_sementara` (
  `id_racik_obat_sementara` int NOT NULL,
  `kode_racik` varchar(100) NOT NULL,
  `nama_racik` varchar(70) NOT NULL,
  `jenis_racik` varchar(10) NOT NULL,
  `jumlah_racik` int NOT NULL,
  `ongkos_racik` int NOT NULL,
  `total_racik` int NOT NULL,
  `keterangan_racik` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `racik_obat_sementara`
--

INSERT INTO `racik_obat_sementara` (`id_racik_obat_sementara`, `kode_racik`, `nama_racik`, `jenis_racik`, `jumlah_racik`, `ongkos_racik`, `total_racik`, `keterangan_racik`) VALUES
(8, '1f3ad70f-7176-4325-95e5-2f233a4355e0', 'Obat Tanpa Racik', '-', 0, 0, 0, '-'),
(10, 'b09d4beb-6385-48f7-ab89-7936fe9a9a5e', 'Obat Tanpa Racik', '-', 0, 0, 0, '-'),
(13, '5f9dccf4-9e1f-438b-8d11-59d07e77e8db', 'Obat Racik 1', 'non-dtd', 1, 1000, 43000, 'PCS'),
(14, 'fb6d7cc6-0108-4016-ae05-dae39e6fd195', 'Obat Tanpa Racik', '-', 0, 0, 0, '-'),
(15, '9f01696a-89bf-48a2-bf72-53a5f4a2ae81', 'Obat Racik 1', 'non-dtd', 1, 1000, 43000, 'PCS'),
(16, 'd6fc1927-f7ec-48c8-84e9-7e48227f9885', 'Obat Tanpa Racik', '-', 0, 0, 0, '-'),
(17, 'a44dd7ef-7294-448a-99cc-72a7b528014a', 'Obat Racik 1', 'non-dtd', 1, 1000, 43000, 'PCS'),
(18, '71983555-b20d-4338-8eb2-8a3c80b7d775', 'Obat Tanpa Racik', '-', 0, 0, 0, '-'),
(19, 'e5ec238a-1066-4164-8348-7f415a651001', 'Obat Racik 1', 'non-dtd', 1, 1000, 43000, 'PCS'),
(20, '2c07543d-9a2d-499e-b8c2-3739b5d5a2bf', 'Obat Tanpa Racik', '-', 0, 0, 0, '-'),
(21, '2ff435a6-46d4-447e-9602-570f80f181cb', 'Obat Racik 1', 'non-dtd', 1, 1000, 43000, 'PCS'),
(23, '85e18cd1-92c8-4210-86b6-0f4e8bf55e20', 'Obat Racik 1', 'non-dtd', 1, 1000, 17000, 'PCS'),
(24, '5d5a188f-a080-48ce-825a-f396816ce18d', 'Obat Tanpa Racik', '-', 0, 0, 0, '-'),
(25, '773169d1-162c-4fc3-8e21-d58e485b605d', 'Obat Racik 1', 'non-dtd', 1, 1000, 43000, 'PCS'),
(26, '773169d1-162c-4fc3-8e21-d58e485b605d', 'Obat Tanpa Racik', '-', 0, 0, 0, '-'),
(30, '1db49ef8-b3e3-46dc-a5a7-1c780f79bdc3', 'Obat Racik 1', 'non-dtd', 1, 1000, 31000, 'PCS'),
(31, '1db49ef8-b3e3-46dc-a5a7-1c780f79bdc3', 'Obat Tanpa Racik', '-', 0, 0, 52000, '-');

-- --------------------------------------------------------

--
-- Table structure for table `racik_obat_sementara_detail`
--

CREATE TABLE `racik_obat_sementara_detail` (
  `id_racik_obat_sementara_detail` int NOT NULL,
  `id_racik_obat_sementara` int NOT NULL,
  `id_obat` int NOT NULL,
  `id_supplier` int NOT NULL,
  `jumlah` int NOT NULL,
  `embalase` int NOT NULL,
  `sub_total` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `racik_obat_sementara_detail`
--

INSERT INTO `racik_obat_sementara_detail` (`id_racik_obat_sementara_detail`, `id_racik_obat_sementara`, `id_obat`, `id_supplier`, `jumlah`, `embalase`, `sub_total`) VALUES
(5, 8, 18, 4, 1, 1000, 25000),
(9, 13, 4, 2, 1, 1000, 16000),
(10, 13, 24, 2, 1, 1000, 25000),
(11, 14, 30, 2, 1, 1000, 13000),
(12, 15, 4, 4, 1, 1000, 16000),
(13, 15, 18, 4, 1, 1000, 25000),
(14, 16, 39, 6, 1, 1000, 166000),
(15, 17, 4, 4, 1, 1000, 16000),
(16, 17, 18, 4, 1, 1000, 25000),
(17, 18, 24, 4, 1, 1000, 25000),
(18, 19, 4, 2, 1, 1000, 16000),
(19, 19, 18, 2, 1, 1000, 25000),
(20, 20, 24, 2, 1, 1000, 25000),
(21, 21, 4, 4, 1, 1000, 16000),
(22, 21, 18, 2, 1, 1000, 25000),
(25, 23, 4, 2, 1, 1000, 16000),
(26, 24, 30, 2, 1, 1000, 13000),
(27, 25, 4, 2, 1, 1000, 16000),
(28, 25, 18, 2, 1, 1000, 25000),
(29, 26, 4, 4, 1, 1000, 16000),
(30, 26, 18, 4, 1, 1000, 25000),
(36, 30, 24, 2, 1, 1000, 25000),
(37, 30, 43, 2, 1, 1000, 4000),
(38, 31, 18, 4, 1, 1000, 25000),
(39, 31, 32, 5, 1, 1000, 25000);

-- --------------------------------------------------------

--
-- Table structure for table `retur_barang`
--

CREATE TABLE `retur_barang` (
  `id_retur_barang` int NOT NULL,
  `nomor_retur` varchar(50) NOT NULL,
  `tanggal_retur` date NOT NULL,
  `keterangan_retur` text NOT NULL,
  `tanggal_transaksi` date NOT NULL,
  `nomor_transaksi` int NOT NULL,
  `total_nominal_retur` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `nomor_transaksi_` int NOT NULL,
  `total_nominal_retur_` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `retur_barang`
--

INSERT INTO `retur_barang` (`id_retur_barang`, `nomor_retur`, `tanggal_retur`, `keterangan_retur`, `tanggal_transaksi`, `nomor_transaksi`, `total_nominal_retur`, `created_at`, `updated_at`, `nomor_transaksi_`, `total_nominal_retur_`) VALUES
(1, 'OBA-00000000001', '2022-06-10', 'Ngetes', '2022-06-10', 220620003, 0, NULL, NULL, 0, 0),
(2, 'OBA-00000000002', '2022-06-21', 'Ngetes Retur Barang', '2022-06-21', 220620004, 393000, NULL, NULL, 0, 0),
(3, '220670001', '2022-06-22', 'Ngetes aja dulu', '2022-06-21', 220620004, 110000, NULL, NULL, 0, 0),
(4, '220670002', '2022-06-25', 'Sip', '2022-06-21', 220620004, 273000, NULL, NULL, 0, 0),
(5, '220670003', '2022-06-25', 'oke', '2022-06-21', 220620004, 258000, NULL, NULL, 0, 0),
(6, '220670004', '2022-06-30', '-', '2022-06-02', 220610001, 13320, NULL, NULL, 0, 0),
(7, '220670005', '2022-06-30', '-', '2022-06-02', 220610001, 13320, NULL, NULL, 0, 0),
(8, '220770001', '2022-07-26', '-', '2022-07-25', 220710001, 85080, NULL, NULL, 0, 0),
(9, '220870001', '2022-08-18', '-', '2022-06-01', 220620001, 80000, NULL, NULL, 0, 0),
(10, '220870002', '2022-08-18', '-', '2022-07-23', 220730015, 25000, NULL, NULL, 0, 0),
(11, '220870002', '2022-08-18', '-', '2022-07-23', 220730015, 25000, NULL, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `retur_barang_detail`
--

CREATE TABLE `retur_barang_detail` (
  `id_retur_barang_detail` int NOT NULL,
  `id_retur_barang` int NOT NULL,
  `id_obat` int NOT NULL,
  `stok_transaksi` int NOT NULL,
  `stok_retur` int NOT NULL,
  `nominal_retur` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `stok_transaksi_` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `retur_barang_detail`
--

INSERT INTO `retur_barang_detail` (`id_retur_barang_detail`, `id_retur_barang`, `id_obat`, `stok_transaksi`, `stok_retur`, `nominal_retur`, `created_at`, `updated_at`, `stok_transaksi_`) VALUES
(1, 1, 7, 10, 10, 0, '2022-06-10 15:51:42', '2022-06-10 15:51:42', 0),
(2, 2, 18, 10, 5, 0, '2022-06-21 22:42:47', '2022-06-21 22:42:47', 0),
(3, 2, 24, 10, 10, 0, '2022-06-21 22:42:47', '2022-06-21 22:42:47', 0),
(4, 2, 40, 5, 5, 0, '2022-06-21 22:42:47', '2022-06-21 22:42:47', 0),
(5, 3, 18, 10, 5, 0, '2022-06-22 22:00:19', '2022-06-22 22:00:19', 0),
(6, 4, 18, 10, 5, 100000, '2022-06-25 01:41:20', '2022-06-25 01:41:20', 0),
(7, 4, 24, 10, 5, 123000, '2022-06-25 01:41:20', '2022-06-25 01:41:20', 0),
(8, 4, 40, 5, 5, 50000, '2022-06-25 01:41:20', '2022-06-25 01:41:20', 0),
(9, 5, 18, 10, 5, 110000, '2022-06-25 01:44:43', '2022-06-25 01:44:43', 0),
(10, 5, 24, 10, 5, 123000, '2022-06-25 01:44:43', '2022-06-25 01:44:43', 0),
(11, 5, 40, 5, 5, 25000, '2022-06-25 01:44:43', '2022-06-25 01:44:43', 0),
(12, 6, 7, 10, 5, 13320, '2022-06-30 11:49:57', '2022-06-30 11:49:57', 0),
(13, 7, 7, 10, 5, 13320, '2022-06-30 11:50:39', '2022-06-30 11:50:39', 0),
(14, 8, 4, 5, 5, 85080, '2022-07-26 00:19:09', '2022-07-26 00:19:09', 0),
(15, 9, 4, 5, 5, 80000, '2022-08-18 21:50:43', '2022-08-18 21:50:43', 0),
(16, 11, 18, 10, 10, 25000, '2022-08-18 21:59:30', '2022-08-18 21:59:30', 0);

-- --------------------------------------------------------

--
-- Table structure for table `stok_opnem`
--

CREATE TABLE `stok_opnem` (
  `id_stok_opnem` int NOT NULL,
  `tanggal_stok_opnem` date NOT NULL,
  `keterangan` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status_input` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stok_opnem`
--

INSERT INTO `stok_opnem` (`id_stok_opnem`, `tanggal_stok_opnem`, `keterangan`, `status_input`, `created_at`, `updated_at`) VALUES
(1, '2021-08-17', '-', 1, '2021-08-17 21:13:33', '2022-04-19 14:15:06'),
(2, '2021-08-17', '-', 1, '2021-08-17 21:13:51', '2022-04-19 14:15:06'),
(3, '2021-08-17', '-', 1, '2021-08-17 21:14:35', '2022-04-19 14:15:06'),
(4, '2021-08-17', '-', 1, '2021-08-17 21:14:57', '2022-04-19 14:15:06'),
(5, '2021-08-17', '-', 1, '2021-08-17 21:15:13', '2022-04-19 14:15:06'),
(6, '2021-08-17', '-', 1, '2021-08-17 21:15:47', '2022-04-19 14:15:06'),
(7, '2021-08-17', '-', 1, '2021-08-17 21:15:59', '2022-04-19 14:15:06'),
(8, '2021-08-17', '-', 1, '2021-08-17 21:18:10', '2022-04-19 14:15:06'),
(9, '2021-08-17', '-', 1, '2021-08-17 21:18:28', '2022-04-19 14:15:06'),
(10, '2021-08-17', '-', 1, '2021-08-17 21:18:48', '2022-04-19 14:15:06'),
(11, '2021-08-17', '-', 1, '2021-08-17 21:19:36', '2022-04-19 14:15:06'),
(12, '2021-08-17', '-', 1, '2021-08-17 21:19:47', '2022-04-19 14:15:06'),
(13, '2021-08-17', '-', 1, '2021-08-17 21:20:05', '2022-04-19 14:15:06'),
(14, '2021-08-17', '-', 1, '2021-08-17 21:20:13', '2022-04-19 14:15:06'),
(15, '2021-08-17', '-', 1, '2021-08-17 21:20:30', '2022-04-19 14:15:06'),
(16, '2021-08-17', '-', 1, '2021-08-17 21:21:28', '2022-04-19 14:15:06'),
(17, '2021-08-17', '-', 1, '2021-08-17 21:21:34', '2022-04-19 14:15:06'),
(18, '2021-09-08', '10', 1, '2021-09-08 12:43:17', '2022-04-19 14:15:06'),
(19, '2021-09-08', '10', 1, '2021-09-08 12:44:32', '2022-04-19 14:15:06'),
(20, '2021-09-08', '-', 1, '2021-09-08 13:00:18', '2022-04-19 14:15:06'),
(21, '2021-09-10', '-', 1, '2021-09-10 11:01:50', '2022-04-19 14:15:06'),
(22, '2021-09-10', '-', 1, '2021-09-10 11:04:11', '2022-04-19 14:15:06'),
(23, '2021-09-10', '-', 1, '2021-09-10 11:04:42', '2022-04-19 14:15:06'),
(24, '2021-09-20', '-', 1, '2021-09-20 14:13:23', '2022-04-19 14:15:06'),
(25, '2021-09-20', '-', 1, '2021-09-20 19:44:15', '2022-04-19 14:15:06'),
(26, '2021-09-23', '-', 1, '2021-09-23 20:57:37', '2022-04-19 14:15:06'),
(27, '2021-10-01', '-', 1, '2021-10-01 19:59:25', '2022-04-19 14:15:06');

-- --------------------------------------------------------

--
-- Table structure for table `stok_opnem_detail`
--

CREATE TABLE `stok_opnem_detail` (
  `id_stok_opnem_detail` int NOT NULL,
  `id_stok_opnem` int NOT NULL,
  `id_obat` int NOT NULL,
  `stok_komputer` int NOT NULL,
  `stok_fisik` varchar(100) NOT NULL,
  `stok_selisih` varchar(100) NOT NULL,
  `sub_nilai` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stok_opnem_detail`
--

INSERT INTO `stok_opnem_detail` (`id_stok_opnem_detail`, `id_stok_opnem`, `id_obat`, `stok_komputer`, `stok_fisik`, `stok_selisih`, `sub_nilai`) VALUES
(1, 4, 4, 0, '10', '479', 20000),
(2, 4, 5, 0, '10', '288', 20000),
(3, 4, 7, 0, '10', '268', 20000),
(4, 4, 9, 0, '10', '39', 20000),
(5, 4, 18, 0, '10', '40', 200000),
(6, 4, 24, 0, '10', '163', 200000),
(7, 4, 29, 0, '10', '0', 100000),
(8, 4, 30, 0, '10', '90', 100000),
(9, 4, 32, 0, '10', '205', 200000),
(10, 4, 39, 0, '10', '107', 1353000),
(11, 4, 40, 0, '10', '-10', 40000),
(12, 4, 41, 0, '10', '100', 20000),
(13, 4, 43, 0, '10', '190', 20000),
(14, 5, 4, 0, '10', '479', 20000),
(15, 5, 5, 0, '10', '288', 20000),
(16, 5, 7, 0, '10', '268', 20000),
(17, 5, 9, 0, '10', '39', 20000),
(18, 5, 18, 0, '10', '40', 200000),
(19, 5, 24, 0, '10', '163', 200000),
(20, 5, 29, 0, '10', '0', 100000),
(21, 5, 30, 0, '10', '90', 100000),
(22, 5, 32, 0, '10', '205', 200000),
(23, 5, 39, 0, '10', '107', 1353000),
(24, 5, 40, 0, '10', '-10', 40000),
(25, 5, 41, 0, '10', '100', 20000),
(26, 5, 43, 0, '10', '190', 20000),
(27, 6, 4, 0, '10', '479', 20000),
(28, 6, 5, 0, '10', '288', 20000),
(29, 6, 7, 0, '10', '268', 20000),
(30, 6, 9, 0, '10', '39', 20000),
(31, 6, 18, 0, '10', '40', 200000),
(32, 6, 24, 0, '10', '163', 200000),
(33, 6, 29, 0, '10', '0', 100000),
(34, 6, 30, 0, '10', '90', 100000),
(35, 6, 32, 0, '10', '205', 200000),
(36, 6, 39, 0, '10', '107', 1353000),
(37, 6, 40, 0, '10', '-10', 40000),
(38, 6, 41, 0, '10', '100', 20000),
(39, 6, 43, 0, '10', '190', 20000),
(40, 7, 4, 0, '10', '479', 20000),
(41, 7, 5, 0, '10', '288', 20000),
(42, 7, 7, 0, '10', '268', 20000),
(43, 7, 9, 0, '10', '39', 20000),
(44, 7, 18, 0, '10', '40', 200000),
(45, 7, 24, 0, '10', '163', 200000),
(46, 7, 29, 0, '10', '0', 100000),
(47, 7, 30, 0, '10', '90', 100000),
(48, 7, 32, 0, '10', '205', 200000),
(49, 7, 39, 0, '10', '107', 1353000),
(50, 7, 40, 0, '10', '-10', 40000),
(51, 7, 41, 0, '10', '100', 20000),
(52, 7, 43, 0, '10', '190', 20000),
(53, 8, 4, 0, '10', '479', 20000),
(54, 8, 5, 0, '10', '288', 20000),
(55, 8, 7, 0, '10', '268', 20000),
(56, 8, 9, 0, '10', '39', 20000),
(57, 8, 18, 0, '10', '40', 200000),
(58, 8, 24, 0, '10', '163', 200000),
(59, 8, 29, 0, '10', '0', 100000),
(60, 8, 30, 0, '10', '90', 100000),
(61, 8, 32, 0, '10', '205', 200000),
(62, 8, 39, 0, '10', '107', 1353000),
(63, 8, 40, 0, '10', '-10', 40000),
(64, 8, 41, 0, '10', '100', 20000),
(65, 8, 43, 0, '10', '190', 20000),
(66, 9, 4, 0, '10', '479', 20000),
(67, 9, 5, 0, '10', '288', 20000),
(68, 9, 7, 0, '10', '268', 20000),
(69, 9, 9, 0, '10', '39', 20000),
(70, 9, 18, 0, '10', '40', 200000),
(71, 9, 24, 0, '10', '163', 200000),
(72, 9, 29, 0, '10', '0', 100000),
(73, 9, 30, 0, '10', '90', 100000),
(74, 9, 32, 0, '10', '205', 200000),
(75, 9, 39, 0, '10', '107', 1353000),
(76, 9, 40, 0, '10', '-10', 40000),
(77, 9, 41, 0, '10', '100', 20000),
(78, 9, 43, 0, '10', '190', 20000),
(79, 10, 4, 0, '10', '479', 20000),
(80, 10, 5, 0, '10', '288', 20000),
(81, 10, 7, 0, '10', '268', 20000),
(82, 10, 9, 0, '10', '39', 20000),
(83, 10, 18, 0, '10', '40', 200000),
(84, 10, 24, 0, '10', '163', 200000),
(85, 10, 29, 0, '10', '0', 100000),
(86, 10, 30, 0, '10', '90', 100000),
(87, 10, 32, 0, '10', '205', 200000),
(88, 10, 39, 0, '10', '107', 1353000),
(89, 10, 40, 0, '10', '-10', 40000),
(90, 10, 41, 0, '10', '100', 20000),
(91, 10, 43, 0, '10', '190', 20000),
(92, 11, 4, 0, '10', '479', 20000),
(93, 11, 5, 0, '10', '288', 20000),
(94, 11, 7, 0, '10', '268', 20000),
(95, 11, 9, 0, '10', '39', 20000),
(96, 11, 18, 0, '10', '40', 200000),
(97, 11, 24, 0, '10', '163', 200000),
(98, 11, 29, 0, '10', '0', 100000),
(99, 11, 30, 0, '10', '90', 100000),
(100, 11, 32, 0, '10', '205', 200000),
(101, 11, 39, 0, '10', '107', 1353000),
(102, 11, 40, 0, '10', '-10', 40000),
(103, 11, 41, 0, '10', '100', 20000),
(104, 11, 43, 0, '10', '190', 20000),
(105, 12, 4, 0, '10', '479', 20000),
(106, 12, 5, 0, '10', '288', 20000),
(107, 12, 7, 0, '10', '268', 20000),
(108, 12, 9, 0, '10', '39', 20000),
(109, 12, 18, 0, '10', '40', 200000),
(110, 12, 24, 0, '10', '163', 200000),
(111, 12, 29, 0, '10', '0', 100000),
(112, 12, 30, 0, '10', '90', 100000),
(113, 12, 32, 0, '10', '205', 200000),
(114, 12, 39, 0, '10', '107', 1353000),
(115, 12, 40, 0, '10', '-10', 40000),
(116, 12, 41, 0, '10', '100', 20000),
(117, 12, 43, 0, '10', '190', 20000),
(118, 13, 4, 0, '10', '479', 20000),
(119, 13, 5, 0, '10', '288', 20000),
(120, 13, 7, 0, '10', '268', 20000),
(121, 13, 9, 0, '10', '39', 20000),
(122, 13, 18, 0, '10', '40', 200000),
(123, 13, 24, 0, '10', '163', 200000),
(124, 13, 29, 0, '10', '0', 100000),
(125, 13, 30, 0, '10', '90', 100000),
(126, 13, 32, 0, '10', '205', 200000),
(127, 13, 39, 0, '10', '107', 1353000),
(128, 13, 40, 0, '10', '-10', 40000),
(129, 13, 41, 0, '10', '100', 20000),
(130, 13, 43, 0, '10', '190', 20000),
(131, 14, 4, 0, '10', '479', 20000),
(132, 14, 5, 0, '10', '288', 20000),
(133, 14, 7, 0, '10', '268', 20000),
(134, 14, 9, 0, '10', '39', 20000),
(135, 14, 18, 0, '10', '40', 200000),
(136, 14, 24, 0, '10', '163', 200000),
(137, 14, 29, 0, '10', '0', 100000),
(138, 14, 30, 0, '10', '90', 100000),
(139, 14, 32, 0, '10', '205', 200000),
(140, 14, 39, 0, '10', '107', 1353000),
(141, 14, 40, 0, '10', '-10', 40000),
(142, 14, 41, 0, '10', '100', 20000),
(143, 14, 43, 0, '10', '190', 20000),
(144, 15, 4, 0, '10', '479', 20000),
(145, 15, 5, 0, '10', '288', 20000),
(146, 15, 7, 0, '10', '268', 20000),
(147, 15, 9, 0, '10', '39', 20000),
(148, 15, 18, 0, '10', '40', 200000),
(149, 15, 24, 0, '10', '163', 200000),
(150, 15, 29, 0, '10', '0', 100000),
(151, 15, 30, 0, '10', '90', 100000),
(152, 15, 32, 0, '10', '205', 200000),
(153, 15, 39, 0, '10', '107', 1353000),
(154, 15, 40, 0, '10', '-10', 40000),
(155, 15, 41, 0, '10', '100', 20000),
(156, 15, 43, 0, '10', '190', 20000),
(157, 16, 4, 0, '10', '479', 20000),
(158, 16, 5, 0, '10', '288', 20000),
(159, 16, 7, 0, '10', '268', 20000),
(160, 16, 9, 0, '10', '39', 20000),
(161, 16, 18, 0, '10', '40', 200000),
(162, 16, 24, 0, '10', '163', 200000),
(163, 16, 29, 0, '10', '0', 100000),
(164, 16, 30, 0, '10', '90', 100000),
(165, 16, 32, 0, '10', '205', 200000),
(166, 16, 39, 0, '10', '107', 1353000),
(167, 16, 40, 0, '10', '-10', 40000),
(168, 16, 41, 0, '10', '100', 20000),
(169, 16, 43, 0, '10', '190', 20000),
(170, 17, 4, 0, '10', '479', 20000),
(171, 17, 5, 0, '10', '288', 20000),
(172, 17, 7, 0, '10', '268', 20000),
(173, 17, 9, 0, '10', '39', 20000),
(174, 17, 18, 0, '10', '40', 200000),
(175, 17, 24, 0, '10', '163', 200000),
(176, 17, 29, 0, '10', '0', 100000),
(177, 17, 30, 0, '10', '90', 100000),
(178, 17, 32, 0, '10', '205', 200000),
(179, 17, 39, 0, '10', '107', 1353000),
(180, 17, 40, 0, '10', '-10', 40000),
(181, 17, 41, 0, '10', '100', 20000),
(182, 17, 43, 0, '10', '190', 20000),
(196, 19, 4, 0, '10', '418', 20000),
(197, 19, 5, 0, '10', '-42', 20000),
(198, 19, 7, 0, '10', '264', 20000),
(199, 19, 9, 0, '10', '39', 20000),
(200, 19, 18, 0, '10', '30', 200000),
(201, 19, 24, 0, '10', '163', 200000),
(202, 19, 29, 0, '10', '0', 100000),
(203, 19, 30, 0, '10', '190', 100000),
(204, 19, 32, 0, '10', '285', 200000),
(205, 19, 39, 0, '10', '87', 1353000),
(206, 19, 40, 0, '10', '790', 40000),
(207, 19, 41, 0, '10', '80', 20000),
(208, 19, 43, 0, '10', '90', 20000),
(209, 20, 4, 0, '10', '418', 20000),
(210, 21, 4, 0, '10', '218', 20000),
(211, 21, 5, 0, '10', '-42', 20000),
(212, 21, 7, 0, '10', '263', 20000),
(213, 22, 4, 0, '10', '218', 20000),
(214, 22, 5, 0, '10', '-42', 20000),
(215, 22, 7, 0, '10', '263', 20000),
(216, 23, 4, 0, '10', '218', 20000),
(217, 24, 4, 0, '10', '214', 20000),
(218, 25, 4, 234, '10', '224', 20000),
(219, 26, 4, 242, '10', '232', 20000),
(220, 27, 5, -32, '23', '-55', 46000);

--
-- Triggers `stok_opnem_detail`
--
DELIMITER $$
CREATE TRIGGER `stok_obat_update` AFTER INSERT ON `stok_opnem_detail` FOR EACH ROW BEGIN
	UPDATE obat SET stok_obat = NEW.stok_fisik WHERE id_obat = NEW.id_obat;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `supplier_obat`
--

CREATE TABLE `supplier_obat` (
  `id_supplier` int NOT NULL,
  `nama_supplier` varchar(100) NOT NULL,
  `nomor_telepon` varchar(20) NOT NULL,
  `alamat_supplier` text NOT NULL,
  `singkatan_supplier` varchar(20) NOT NULL,
  `status_delete` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier_obat`
--

INSERT INTO `supplier_obat` (`id_supplier`, `nama_supplier`, `nomor_telepon`, `alamat_supplier`, `singkatan_supplier`, `status_delete`) VALUES
(2, 'PT. Farmindo Mitra Mandiri', '05412323232', 'Jln. Kartini No. 18', 'FMM', 0),
(4, 'PT. Servier Indonesia', '02157903940', 'Menara Kadin Indonesia, \r\nKav . 2- 3,\r\n Jl. H. R. Rasuna Said, \r\nRT.1/RW.2,\r\n Kuningan, Kuningan Tim.,\r\n Kota Jakarta Selatan, \r\nDaerah Khusus Ibukota Jakarta 12950', 'SI', 0),
(5, 'Kalbe Farma', '0', '-', 'KF', 0),
(6, 'PT. Indofarma (INAF) Tbk', '0', '-', 'IDF', 0);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_kasir`
--

CREATE TABLE `transaksi_kasir` (
  `id_transaksi` int NOT NULL,
  `id_users` int NOT NULL,
  `tanggal_transaksi` date NOT NULL,
  `jam_transaksi` time NOT NULL,
  `kode_transaksi` int NOT NULL,
  `total` int NOT NULL,
  `bayar` int NOT NULL,
  `kembali` int NOT NULL,
  `id_jam_shift` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi_kasir`
--

INSERT INTO `transaksi_kasir` (`id_transaksi`, `id_users`, `tanggal_transaksi`, `jam_transaksi`, `kode_transaksi`, `total`, `bayar`, `kembali`, `id_jam_shift`, `created_at`, `updated_at`) VALUES
(77, 4, '2021-08-04', '00:00:00', 1628065699, 123600, 123600, 0, 1, NULL, NULL),
(78, 4, '2021-08-04', '00:00:00', 1628065763, 2500, 2500, 0, 1, NULL, NULL),
(81, 1, '2021-08-12', '00:00:00', 1628749589, 1353000, 1353000, 0, 1, NULL, NULL),
(82, 1, '2021-08-12', '00:00:00', 1628783946, 51000, 51000, 0, 1, NULL, NULL),
(83, 1, '2021-08-13', '00:00:00', 1628785348, 1353000, 1353000, 0, 1, NULL, NULL),
(84, 1, '2021-08-13', '00:00:00', 1628868975, 407000, 407000, 0, 2, NULL, NULL),
(85, 1, '2021-08-15', '00:00:00', 1628965186, 50000, 50000, 0, 2, NULL, NULL),
(86, 1, '2021-08-15', '00:00:00', 1629003089, 102000, 102000, 0, 2, NULL, NULL),
(87, 1, '2021-08-15', '00:00:00', 1629005097, 50000, 50000, 0, 2, NULL, NULL),
(88, 1, '2021-08-15', '00:00:00', 1629005187, 50000, 50000, 0, 2, NULL, NULL),
(89, 1, '2021-08-15', '00:00:00', 1629034393, 320000, 320000, 0, 2, NULL, NULL),
(90, 1, '2021-08-16', '00:00:00', 1629115354, 30000, 30000, 0, 2, NULL, NULL),
(91, 1, '2021-08-16', '00:00:00', 1629123189, 40000, 40000, 0, 2, NULL, NULL),
(92, 1, '2021-08-17', '00:00:00', 1629212874, 30000, 30000, 0, 2, NULL, NULL),
(93, 1, '2021-08-17', '00:00:00', 1629212884, 30000, 30000, 0, 2, NULL, NULL),
(94, 1, '2021-08-17', '00:00:00', 1629213062, 30000, 30000, 0, 2, NULL, NULL),
(95, 1, '2021-08-17', '00:00:00', 1629213072, 30000, 30000, 0, 2, NULL, NULL),
(96, 1, '2021-08-17', '00:00:00', 1629213092, 30000, 30000, 0, 2, NULL, NULL),
(97, 1, '2021-08-17', '00:00:00', 1629213129, 30000, 30000, 0, 2, NULL, NULL),
(98, 1, '2021-08-17', '00:00:00', 1629213132, 30000, 30000, 0, 2, NULL, NULL),
(99, 1, '2021-08-17', '00:00:00', 1629213225, 27000, 27000, 0, 2, NULL, NULL),
(100, 1, '2021-08-17', '00:00:00', 1629213252, 27000, 27000, 0, 2, NULL, NULL),
(101, 1, '2021-08-17', '00:00:00', 1629213300, 27000, 27000, 0, 2, NULL, NULL),
(102, 1, '2021-08-17', '00:00:00', 1629213343, 27000, 27000, 0, 2, NULL, NULL),
(103, 1, '2021-08-17', '00:00:00', 1629213370, 27000, 27000, 0, 2, NULL, NULL),
(104, 1, '2021-08-18', '00:00:00', 1629251408, 300000, 300000, 0, 2, NULL, NULL),
(105, 1, '2021-08-18', '00:00:00', 1629264035, 300000, 300000, 0, 2, NULL, NULL),
(106, 1, '2021-08-18', '00:00:00', 1629287745, 320000, 320000, 0, 2, NULL, NULL),
(107, 1, '2021-08-19', '00:00:00', 1629356966, 30000, 30000, 0, 1, NULL, NULL),
(108, 1, '2021-08-19', '00:00:00', 1629357008, 30000, 30000, 0, 1, NULL, NULL),
(109, 1, '2021-08-21', '00:00:00', 1629523109, 25000, 25000, 0, 1, NULL, NULL),
(110, 1, '2021-08-21', '00:00:00', 1629523148, 25000, 25000, 0, 1, NULL, NULL),
(111, 1, '2021-08-21', '00:00:00', 1629523153, 25000, 25000, 0, 1, NULL, NULL),
(112, 1, '2021-08-21', '00:00:00', 1629523167, 25000, 25000, 0, 1, NULL, NULL),
(113, 1, '2021-08-21', '00:00:00', 1629523172, 25000, 25000, 0, 1, NULL, NULL),
(114, 1, '2021-08-21', '00:00:00', 1629523205, 25000, 25000, 0, 1, NULL, NULL),
(115, 1, '2021-08-21', '00:00:00', 1629523215, 25000, 25000, 0, 1, NULL, NULL),
(116, 1, '2021-08-21', '00:00:00', 1629523227, 25000, 25000, 0, 1, NULL, NULL),
(117, 1, '2021-08-21', '00:00:00', 1629523265, 25000, 25000, 0, 1, NULL, NULL),
(118, 1, '2021-08-21', '00:00:00', 1629523292, 25000, 25000, 0, 1, NULL, NULL),
(119, 1, '2021-08-21', '00:00:00', 1629523298, 25000, 25000, 0, 1, NULL, NULL),
(120, 1, '2021-08-21', '00:00:00', 1629523305, 25000, 25000, 0, 1, NULL, NULL),
(121, 1, '2021-08-21', '00:00:00', 1629523312, 25000, 25000, 0, 1, NULL, NULL),
(122, 1, '2021-08-21', '00:00:00', 1629523322, 25000, 25000, 0, 1, NULL, NULL),
(123, 1, '2021-08-21', '00:00:00', 1629523332, 25000, 25000, 0, 1, NULL, NULL),
(124, 1, '2021-08-21', '00:00:00', 1629523445, 25000, 25000, 0, 1, NULL, NULL),
(125, 1, '2021-08-21', '00:00:00', 1629523477, 1638000, 1638000, 0, 1, NULL, NULL),
(126, 1, '2021-08-21', '00:00:00', 1629523546, 1638000, 1638000, 0, 1, NULL, NULL),
(127, 4, '2021-08-25', '00:00:00', 1629871256, 3000, 3000, 0, 1, NULL, NULL),
(128, 4, '2021-08-25', '00:00:00', 1629871271, 3000, 5000, 2000, 1, NULL, NULL),
(129, 1, '2021-08-26', '00:00:00', 1629951762, 3000, 3000, 0, 1, NULL, NULL),
(130, 1, '2021-08-26', '00:00:00', 1629951815, 10000, 10000, 0, 1, NULL, NULL),
(131, 1, '2021-08-26', '00:00:00', 1629951841, 5000, 5000, 0, 1, NULL, NULL),
(132, 1, '2021-08-26', '00:00:00', 1629953357, 3000, 3000, 0, 1, NULL, NULL),
(133, 1, '2021-09-10', '00:00:00', 1631256433, 5000, 5000, 0, 1, NULL, NULL),
(134, 1, '2021-09-10', '00:00:00', 1631256701, 5000, 5000, 0, 1, NULL, NULL),
(135, 1, '2021-10-01', '00:00:00', 1633077177, 3000, 3000, 0, 1, NULL, NULL),
(136, 1, '2021-10-01', '00:00:00', 1633077205, 3000, 3000, 0, 1, NULL, NULL),
(137, 1, '2021-10-01', '00:00:00', 1633077600, 3000, 3000, 0, 1, NULL, NULL),
(138, 1, '2021-10-01', '00:00:00', 1633077629, 3000, 3000, 0, 1, NULL, NULL),
(139, 1, '2021-10-01', '00:00:00', 1633079496, 1000, 3000, 2000, 2, NULL, NULL),
(140, 1, '2021-10-01', '00:00:00', 1633099654, 3000, 3000, 0, 2, NULL, NULL),
(141, 1, '2021-10-01', '00:00:00', 1633100296, 3000, 3000, 0, 2, NULL, NULL),
(142, 1, '2021-10-04', '00:00:00', 1633330885, 30000, 30000, 0, 1, NULL, NULL),
(143, 1, '2021-10-20', '00:00:00', 1634711958, 25000, 25000, 0, 1, NULL, NULL),
(144, 1, '2021-10-20', '00:00:00', 1634712011, 25000, 25000, 0, 1, NULL, NULL),
(145, 1, '2021-10-20', '00:00:00', 1634712045, 25000, 25000, 0, 1, NULL, NULL),
(146, 1, '2021-10-20', '00:00:00', 1634712168, 87420, 87420, 0, 1, NULL, NULL),
(147, 1, '2021-10-20', '00:00:00', 1634712207, 6000, 6000, 0, 1, NULL, NULL),
(148, 1, '2021-10-20', '00:00:00', 1634712236, 3000, 3000, 0, 1, NULL, NULL),
(149, 1, '2021-10-22', '00:00:00', 1634888227, 10000, 10000, 0, 1, NULL, NULL),
(150, 1, '2021-10-22', '00:00:00', 1634888299, 10000, 10000, 0, 1, NULL, NULL),
(151, 1, '2021-10-22', '00:00:00', 1634888312, 10000, 10000, 0, 1, NULL, NULL),
(152, 1, '2021-10-22', '00:00:00', 1634888321, 10000, 10000, 0, 1, NULL, NULL),
(153, 1, '2021-10-22', '00:00:00', 1634888330, 10000, 10000, 0, 1, NULL, NULL),
(154, 1, '2021-10-22', '00:00:00', 1634888347, 10000, 10000, 0, 1, NULL, NULL),
(155, 1, '2021-10-22', '00:00:00', 1634888399, 10000, 10000, 0, 1, NULL, NULL),
(156, 1, '2021-10-22', '00:00:00', 1634888472, 50000, 50000, 0, 1, NULL, NULL),
(157, 1, '2021-10-22', '00:00:00', 1634888560, 20000, 20000, 0, 1, NULL, NULL),
(158, 1, '2021-12-16', '00:00:00', 1639657846, 23000, 23000, 0, 2, NULL, NULL),
(159, 1, '2021-12-16', '00:00:00', 1639657920, 23000, 23000, 0, 2, NULL, NULL),
(160, 1, '2021-12-16', '00:00:00', 1639658033, 23000, 23000, 0, 2, NULL, NULL),
(161, 1, '2021-12-16', '00:00:00', 1639658068, 33000, 33000, 0, 2, NULL, NULL),
(162, 1, '2021-12-16', '00:00:00', 1639658589, 22000, 22000, 0, 2, NULL, NULL),
(163, 1, '2021-12-16', '00:00:00', 1639660552, 49000, 49000, 0, 2, NULL, NULL),
(164, 1, '2022-04-04', '00:00:00', 1649081711, 0, 25000, 25000, 2, NULL, NULL),
(165, 1, '2022-04-04', '00:00:00', 1649081947, 0, 25000, 25000, 2, NULL, NULL),
(166, 1, '2022-04-04', '00:00:00', 1649082389, 25000, 25000, 0, 2, NULL, NULL),
(167, 1, '2022-06-01', '00:00:00', 1654026006, 71000, 71000, 0, 5, NULL, NULL),
(168, 1, '2022-06-01', '00:00:00', 220620001, 71000, 71000, 0, 5, NULL, NULL),
(169, 1, '2022-06-01', '00:00:00', 220620002, 71000, 71000, 0, 5, NULL, NULL),
(170, 1, '2022-06-10', '00:00:00', 220620003, 25000, 25000, 0, 5, NULL, NULL),
(171, 1, '2022-06-21', '00:00:00', 220620004, 515000, 515000, 0, 5, NULL, NULL),
(172, 1, '2022-06-25', '00:00:00', 220620005, 20000, 20000, 0, 5, NULL, NULL),
(173, 1, '2022-06-27', '00:00:00', 220620006, 5000, 5000, 0, 1, NULL, NULL),
(174, 1, '2022-07-11', '00:00:00', 220720001, 270000, 270000, 0, 1, NULL, NULL),
(175, 1, '2022-07-22', '15:24:29', 220720002, 141000, 141000, 0, 2, NULL, NULL),
(176, 15, '2022-07-23', '23:34:11', 220720003, 78000, 78000, 0, 5, NULL, NULL),
(177, 1, '2022-08-01', '11:32:30', 220920001, 16000, 16000, 0, 1, NULL, NULL),
(178, 1, '2022-08-01', '11:33:22', 220920002, 16000, 16000, 0, 1, NULL, NULL),
(179, 1, '2022-08-01', '11:33:32', 220920003, 16000, 16000, 0, 1, NULL, NULL),
(180, 1, '2022-08-01', '11:33:58', 220922083, 25000, 25000, 0, 1, NULL, NULL),
(187, 1, '2022-08-01', '13:08:39', 220820001, 16000, 16000, 0, 1, NULL, NULL),
(188, 1, '2022-08-01', '13:08:42', 220820002, 16000, 16000, 0, 1, NULL, NULL),
(189, 1, '2022-08-18', '22:07:19', 220820003, 156000, 156000, 0, 5, NULL, NULL),
(190, 1, '2022-08-18', '22:11:27', 220820004, 16000, 16000, 0, 5, NULL, NULL),
(191, 1, '2022-08-18', '22:14:48', 220820005, 16000, 16000, 0, 5, NULL, NULL),
(192, 1, '2022-08-18', '22:14:48', 220820006, 16000, 16000, 0, 5, NULL, NULL),
(193, 1, '2022-08-18', '22:34:17', 220820007, 47000, 47000, 0, 5, NULL, NULL),
(194, 1, '2022-08-18', '22:34:17', 220820008, 47000, 47000, 0, 5, NULL, NULL),
(195, 1, '2022-08-18', '22:34:59', 220820009, 47000, 47000, 0, 5, NULL, NULL),
(196, 1, '2022-08-18', '22:35:16', 220820010, 47000, 47000, 0, 5, NULL, NULL),
(197, 1, '2022-08-18', '23:01:15', 220820011, 8000, 8000, 0, 5, NULL, NULL),
(198, 1, '2022-08-18', '23:01:15', 220820012, 8000, 8000, 0, 5, NULL, NULL),
(199, 1, '2022-08-18', '23:01:15', 220820013, 8000, 8000, 0, 5, NULL, NULL),
(200, 1, '2022-08-18', '23:06:02', 220820014, 47000, 47000, 0, 5, NULL, NULL),
(201, 1, '2022-08-18', '23:06:13', 220820015, 49000, 49000, 0, 5, NULL, NULL),
(202, 1, '2022-08-18', '23:06:28', 220820016, 49000, 49000, 0, 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_kasir_det`
--

CREATE TABLE `transaksi_kasir_det` (
  `id_transaksi_det` int NOT NULL,
  `id_transaksi` int NOT NULL,
  `id_obat` int NOT NULL,
  `id_supplier` int NOT NULL,
  `sub_total_obat` int NOT NULL,
  `jumlah` int NOT NULL,
  `diskon` int NOT NULL,
  `jenis_diskon` varchar(20) NOT NULL,
  `sub_total` int NOT NULL,
  `tipe_transaksi` varchar(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Item Kasir Detail';

--
-- Dumping data for table `transaksi_kasir_det`
--

INSERT INTO `transaksi_kasir_det` (`id_transaksi_det`, `id_transaksi`, `id_obat`, `id_supplier`, `sub_total_obat`, `jumlah`, `diskon`, `jenis_diskon`, `sub_total`, `tipe_transaksi`, `created_at`, `updated_at`) VALUES
(74, 77, 39, 6, 0, 10, 10, 'persen', 123000, 'bayar-tunai', '2021-08-04 16:28:20', '2021-08-04 16:28:20'),
(75, 77, 18, 2, 0, 10, 5, 'persen', 600, 'bayar-tunai', '2021-08-04 16:28:20', '2021-08-04 16:28:20'),
(76, 78, 32, 2, 0, 5, 5, 'persen', 2500, 'bayar-tunai', '2021-08-04 16:29:23', '2021-08-04 16:29:23'),
(79, 81, 39, 6, 0, 10, 0, 'persen', 1353000, 'bayar-tunai', '2021-08-12 14:26:29', '2021-08-12 14:26:29'),
(80, 82, 40, 5, 0, 10, 0, 'persen', 51000, 'bayar-tunai', '2021-08-12 23:59:06', '2021-08-12 23:59:06'),
(81, 83, 39, 6, 0, 10, 0, 'persen', 1353000, 'bayar-kredit', '2021-08-13 00:22:28', '2021-08-13 00:22:28'),
(82, 83, 40, 5, 0, 20, 0, 'persen', 0, 'bayar-kredit', '2021-08-13 00:22:28', '2021-08-13 00:22:28'),
(83, 84, 40, 5, 0, 10, 10, 'persen', 55000, 'bayar-tunai', '2021-08-13 23:36:15', '2021-08-13 23:36:15'),
(84, 84, 18, 4, 0, 10, 10, 'persen', 352000, 'bayar-tunai', '2021-08-13 23:36:15', '2021-08-13 23:36:15'),
(85, 85, 40, 5, 0, 10, 0, 'persen', 50000, 'bayar-tunai', '2021-08-15 02:19:46', '2021-08-15 02:19:46'),
(86, 86, 40, 5, 0, 20, 0, 'persen', 102000, 'bayar-kredit', '2021-08-15 12:51:29', '2021-08-15 12:51:29'),
(87, 87, 40, 5, 0, 10, 0, 'persen', 50000, 'bayar-tunai', '2021-08-15 13:24:57', '2021-08-15 13:24:57'),
(88, 88, 40, 5, 0, 10, 0, 'persen', 50000, 'bayar-kredit', '2021-08-15 13:26:27', '2021-08-15 13:26:27'),
(89, 89, 18, 4, 0, 10, 0, 'persen', 320000, 'bayar-tunai', '2021-08-15 21:33:13', '2021-08-15 21:33:13'),
(90, 90, 7, 4, 0, 10, 0, 'persen', 30000, 'bayar-tunai', '2021-08-16 20:02:34', '2021-08-16 20:02:34'),
(91, 91, 9, 4, 0, 10, 0, 'persen', 40000, 'bayar-tunai', '2021-08-16 22:13:09', '2021-08-16 22:13:09'),
(92, 92, 5, 2, 0, 10, 0, 'persen', 30000, 'bayar-tunai', '2021-08-17 23:07:55', '2021-08-17 23:07:55'),
(93, 93, 5, 2, 0, 10, 0, 'persen', 30000, 'bayar-tunai', '2021-08-17 23:08:04', '2021-08-17 23:08:04'),
(94, 94, 5, 2, 0, 10, 0, 'persen', 30000, 'bayar-tunai', '2021-08-17 23:11:02', '2021-08-17 23:11:02'),
(95, 95, 5, 2, 0, 10, 0, 'persen', 30000, 'bayar-tunai', '2021-08-17 23:11:12', '2021-08-17 23:11:12'),
(96, 96, 5, 2, 0, 10, 0, 'persen', 30000, 'bayar-tunai', '2021-08-17 23:11:32', '2021-08-17 23:11:32'),
(97, 97, 5, 2, 0, 10, 0, 'persen', 30000, 'bayar-tunai', '2021-08-17 23:12:09', '2021-08-17 23:12:09'),
(98, 98, 5, 2, 0, 10, 0, 'persen', 30000, 'bayar-tunai', '2021-08-17 23:12:12', '2021-08-17 23:12:12'),
(99, 99, 4, 4, 0, 10, 10, 'persen', 27000, 'bayar-tunai', '2021-08-17 23:13:45', '2021-08-17 23:13:45'),
(100, 100, 4, 4, 0, 10, 10, 'persen', 27000, 'bayar-tunai', '2021-08-17 23:14:12', '2021-08-17 23:14:12'),
(101, 101, 4, 4, 0, 10, 10, 'persen', 27000, 'bayar-tunai', '2021-08-17 23:15:00', '2021-08-17 23:15:00'),
(102, 102, 4, 4, 0, 10, 10, 'persen', 27000, 'bayar-tunai', '2021-08-17 23:15:43', '2021-08-17 23:15:43'),
(103, 103, 4, 4, 0, 10, 10, 'persen', 27000, 'bayar-tunai', '2021-08-17 23:16:10', '2021-08-17 23:16:10'),
(104, 104, 43, 2, 0, 100, 0, 'persen', 300000, 'bayar-tunai', '2021-08-18 09:50:08', '2021-08-18 09:50:08'),
(105, 105, 5, 2, 0, 100, 0, 'persen', 300000, 'bayar-tunai', '2021-08-18 13:20:36', '2021-08-18 13:20:36'),
(106, 106, 18, 2, 0, 10, 0, 'persen', 320000, 'bayar-tunai', '2021-08-18 19:55:45', '2021-08-18 19:55:45'),
(107, 107, 41, 5, 0, 10, 0, 'persen', 30000, 'bayar-tunai', '2021-08-19 15:09:26', '2021-08-19 15:09:26'),
(108, 108, 41, 5, 0, 10, 0, 'persen', 30000, 'bayar-tunai', '2021-08-19 15:10:08', '2021-08-19 15:10:08'),
(109, 109, 5, 4, 0, 10, 0, 'persen', 25000, 'bayar-tunai', '2021-08-21 13:18:29', '2021-08-21 13:18:29'),
(110, 110, 5, 4, 0, 10, 0, 'persen', 25000, 'bayar-tunai', '2021-08-21 13:19:09', '2021-08-21 13:19:09'),
(111, 111, 5, 4, 0, 10, 0, 'persen', 25000, 'bayar-tunai', '2021-08-21 13:19:13', '2021-08-21 13:19:13'),
(112, 112, 5, 4, 0, 10, 0, 'persen', 25000, 'bayar-tunai', '2021-08-21 13:19:27', '2021-08-21 13:19:27'),
(113, 113, 5, 4, 0, 10, 0, 'persen', 25000, 'bayar-tunai', '2021-08-21 13:19:32', '2021-08-21 13:19:32'),
(114, 114, 5, 4, 0, 10, 0, 'persen', 25000, 'bayar-tunai', '2021-08-21 13:20:05', '2021-08-21 13:20:05'),
(115, 115, 5, 4, 0, 10, 0, 'persen', 25000, 'bayar-tunai', '2021-08-21 13:20:15', '2021-08-21 13:20:15'),
(116, 116, 5, 4, 0, 10, 0, 'persen', 25000, 'bayar-tunai', '2021-08-21 13:20:27', '2021-08-21 13:20:27'),
(117, 117, 5, 4, 0, 10, 0, 'persen', 25000, 'bayar-tunai', '2021-08-21 13:21:05', '2021-08-21 13:21:05'),
(118, 118, 5, 4, 0, 10, 0, 'persen', 25000, 'bayar-tunai', '2021-08-21 13:21:32', '2021-08-21 13:21:32'),
(119, 119, 5, 4, 0, 10, 0, 'persen', 25000, 'bayar-tunai', '2021-08-21 13:21:38', '2021-08-21 13:21:38'),
(120, 120, 5, 4, 0, 10, 0, 'persen', 25000, 'bayar-tunai', '2021-08-21 13:21:45', '2021-08-21 13:21:45'),
(121, 121, 5, 4, 0, 10, 0, 'persen', 25000, 'bayar-tunai', '2021-08-21 13:21:52', '2021-08-21 13:21:52'),
(122, 122, 5, 4, 0, 10, 0, 'persen', 25000, 'bayar-tunai', '2021-08-21 13:22:02', '2021-08-21 13:22:02'),
(123, 123, 5, 4, 0, 10, 0, 'persen', 25000, 'bayar-tunai', '2021-08-21 13:22:12', '2021-08-21 13:22:12'),
(124, 124, 5, 4, 0, 10, 0, 'persen', 25000, 'bayar-tunai', '2021-08-21 13:24:05', '2021-08-21 13:24:05'),
(125, 125, 39, 6, 0, 10, 0, 'persen', 1638000, 'bayar-tunai', '2021-08-21 13:24:37', '2021-08-21 13:24:37'),
(126, 126, 39, 6, 0, 10, 0, 'persen', 1638000, 'bayar-tunai', '2021-08-21 13:25:46', '2021-08-21 13:25:46'),
(127, 127, 4, 4, 0, 1, 0, 'persen', 3000, 'bayar-tunai', '2021-08-25 14:00:56', '2021-08-25 14:00:56'),
(128, 128, 4, 4, 0, 1, 0, 'persen', 3000, 'bayar-tunai', '2021-08-25 14:01:11', '2021-08-25 14:01:11'),
(129, 129, 4, 2, 0, 1, 0, 'persen', 3000, 'bayar-tunai', '2021-08-26 12:22:42', '2021-08-26 12:22:42'),
(130, 130, 4, 4, 0, 2, 0, 'persen', 5000, 'bayar-kredit', '2021-08-26 12:23:35', '2021-08-26 12:23:35'),
(131, 130, 4, 2, 0, 2, 0, 'persen', 5000, 'bayar-kredit', '2021-08-26 12:23:35', '2021-08-26 12:23:35'),
(132, 131, 4, 2, 0, 2, 0, 'persen', 5000, 'bayar-kredit', '2021-08-26 12:24:01', '2021-08-26 12:24:01'),
(133, 132, 4, 2, 0, 1, 0, 'persen', 3000, 'bayar-tunai', '2021-08-26 12:49:17', '2021-08-26 12:49:17'),
(134, 133, 4, 4, 0, 2, 0, 'persen', 5000, 'bayar-tunai', '2021-09-10 14:47:13', '2021-09-10 14:47:13'),
(135, 134, 4, 4, 0, 2, 0, 'persen', 5000, 'bayar-tunai', '2021-09-10 14:51:41', '2021-09-10 14:51:41'),
(136, 135, 4, 4, 0, 1, 0, '[object Object]', 3000, 'bayar-tunai', '2021-10-01 16:32:57', '2021-10-01 16:32:57'),
(137, 136, 4, 4, 0, 1, 0, '[object Object]', 3000, 'bayar-tunai', '2021-10-01 16:33:25', '2021-10-01 16:33:25'),
(138, 137, 4, 2, 0, 1, 0, 'rupiah', 3000, 'bayar-tunai', '2021-10-01 16:40:00', '2021-10-01 16:40:00'),
(139, 138, 4, 2, 0, 1, 0, 'rupiah', 3000, 'bayar-tunai', '2021-10-01 16:40:29', '2021-10-01 16:40:29'),
(140, 139, 7, 2, 0, 1, 2000, 'rupiah', 1000, 'bayar-tunai', '2021-10-01 17:11:36', '2021-10-01 17:11:36'),
(141, 140, 4, 4, 0, 1, 10, 'persen', 3000, 'bayar-tunai', '2021-10-01 22:47:34', '2021-10-01 22:47:34'),
(142, 141, 5, 2, 0, 1, 0, 'persen', 3000, 'bayar-tunai', '2021-10-01 22:58:16', '2021-10-01 22:58:16'),
(143, 142, 7, 4, 0, 12, 0, 'persen', 30000, 'bayar-tunai', '2021-10-04 15:01:25', '2021-10-04 15:01:25'),
(144, 147, 40, 5, 0, 0, 0, 'persen', 0, 'bayar-kredit', '2021-10-20 14:43:27', '2021-10-20 14:43:27'),
(145, 147, 4, 2, 0, 0, 0, 'persen', 0, 'bayar-kredit', '2021-10-20 14:43:27', '2021-10-20 14:43:27'),
(146, 147, 4, 4, 0, 0, 0, 'persen', 0, 'bayar-kredit', '2021-10-20 14:43:27', '2021-10-20 14:43:27'),
(147, 147, 4, 2, 0, 0, 0, 'persen', 0, 'bayar-kredit', '2021-10-20 14:43:27', '2021-10-20 14:43:27'),
(148, 147, 4, 2, 0, 1, 0, 'persen', 3000, 'bayar-kredit', '2021-10-20 14:43:27', '2021-10-20 14:43:27'),
(149, 147, 7, 2, 0, 1, 0, 'persen', 3000, 'bayar-kredit', '2021-10-20 14:43:27', '2021-10-20 14:43:27'),
(150, 148, 7, 2, 0, 1, 0, 'persen', 3000, 'bayar-kredit', '2021-10-20 14:43:56', '2021-10-20 14:43:56'),
(151, 150, 4, 2, 0, 4, 0, 'persen', 10000, 'bayar-tunai', '2021-10-22 15:38:19', '2021-10-22 15:38:19'),
(152, 153, 4, 2, 0, 4, 0, 'persen', 10000, 'bayar-tunai', '2021-10-22 15:38:50', '2021-10-22 15:38:50'),
(153, 154, 4, 2, 0, 4, 0, 'persen', 10000, 'bayar-tunai', '2021-10-22 15:39:07', '2021-10-22 15:39:07'),
(154, 155, 4, 2, 0, 4, 0, 'persen', 10000, 'bayar-tunai', '2021-10-22 15:40:00', '2021-10-22 15:40:00'),
(155, 156, 4, 4, 0, 10, 0, 'persen', 25000, 'bayar-kredit', '2021-10-22 15:41:12', '2021-10-22 15:41:12'),
(156, 156, 4, 4, 0, 10, 0, 'persen', 25000, 'bayar-kredit', '2021-10-22 15:41:12', '2021-10-22 15:41:12'),
(157, 157, 4, 4, 0, 4, 0, 'persen', 10000, 'bayar-tunai', '2021-10-22 15:42:40', '2021-10-22 15:42:40'),
(158, 157, 4, 4, 0, 4, 0, 'persen', 10000, 'bayar-tunai', '2021-10-22 15:42:40', '2021-10-22 15:42:40'),
(159, 158, 7, 4, 0, 10, 5, 'persen', 23000, 'bayar-tunai', '2021-12-16 20:30:46', '2021-12-16 20:30:46'),
(160, 159, 7, 4, 0, 10, 5, 'persen', 23000, 'bayar-tunai', '2021-12-16 20:32:00', '2021-12-16 20:32:00'),
(161, 160, 7, 4, 0, 10, 5, 'persen', 23000, 'bayar-tunai', '2021-12-16 20:33:53', '2021-12-16 20:33:53'),
(162, 161, 7, 2, 0, 10, 5, 'persen', 23000, 'bayar-tunai', '2021-12-16 20:34:28', '2021-12-16 20:34:28'),
(163, 161, 9, 2, 0, 5, 20, 'persen', 10000, 'bayar-tunai', '2021-12-16 20:34:28', '2021-12-16 20:34:28'),
(164, 162, 4, 4, 0, 10, 0, 'persen', 22000, 'bayar-kredit', '2021-12-16 20:43:09', '2021-12-16 20:43:09'),
(165, 163, 40, 2, 0, 10, 0, 'persen', 49000, 'bayar-kredit', '2021-12-16 21:15:52', '2021-12-16 21:15:52'),
(166, 164, 4, 2, 27000, 10, 2686, 'rupiah', 25000, 'bayar-tunai', '2022-04-04 22:15:11', '2022-04-04 22:15:11'),
(167, 165, 4, 4, 27000, 10, 2686, 'rupiah', 25000, 'bayar-tunai', '2022-04-04 22:19:07', '2022-04-04 22:19:07'),
(168, 166, 4, 2, 27000, 10, 2686, 'rupiah', 25000, 'bayar-tunai', '2022-04-04 22:26:29', '2022-04-04 22:26:29'),
(169, 167, 4, 2, 71000, 5, 0, 'rupiah', 71000, 'bayar-tunai', '2022-06-01 03:40:06', '2022-06-01 03:40:06'),
(170, 168, 4, 2, 71000, 5, 0, 'rupiah', 71000, 'bayar-tunai', '2022-06-01 03:44:36', '2022-06-01 03:44:36'),
(171, 169, 4, 2, 71000, 5, 0, 'rupiah', 71000, 'bayar-tunai', '2022-06-01 03:44:39', '2022-06-01 03:44:39'),
(172, 170, 7, 4, 25000, 10, 0, 'rupiah', 25000, 'bayar-tunai', '2022-06-10 00:25:55', '2022-06-10 00:25:55'),
(173, 171, 18, 2, 245000, 10, 0, 'rupiah', 245000, 'bayar-tunai', '2022-06-21 22:31:02', '2022-06-21 22:31:02'),
(174, 171, 24, 2, 245000, 10, 0, 'rupiah', 245000, 'bayar-tunai', '2022-06-21 22:31:02', '2022-06-21 22:31:02'),
(175, 171, 40, 2, 25000, 5, 0, 'rupiah', 25000, 'bayar-tunai', '2022-06-21 22:31:02', '2022-06-21 22:31:02'),
(176, 172, 5, 4, 22000, 10, 2000, 'rupiah', 20000, 'bayar-tunai', '2022-06-25 02:14:33', '2022-06-25 02:14:33'),
(177, 173, 9, 2, 5000, 2, 0, 'rupiah', 5000, 'bayar-tunai', '2022-06-27 11:59:08', '2022-06-27 11:59:08'),
(178, 174, 7, 2, 25000, 10, 0, 'rupiah', 25000, 'bayar-tunai', '2022-07-11 13:49:44', '2022-07-11 13:49:44'),
(179, 174, 24, 4, 245000, 10, 0, 'rupiah', 245000, 'bayar-tunai', '2022-07-11 13:49:44', '2022-07-11 13:49:44'),
(180, 175, 4, 2, 141000, 10, 0, 'rupiah', 141000, 'bayar-tunai', '2022-07-22 15:24:29', '2022-07-22 15:24:29'),
(181, 176, 4, 2, 78000, 5, 0, 'rupiah', 78000, 'bayar-tunai', '2022-07-23 23:34:11', '2022-07-23 23:34:11'),
(182, 177, 4, 4, 16000, 1, 0, 'rupiah', 16000, 'bayar-tunai', '2022-08-01 11:32:30', '2022-08-01 11:32:30'),
(183, 178, 4, 4, 16000, 1, 0, 'rupiah', 16000, 'bayar-tunai', '2022-08-01 11:33:22', '2022-08-01 11:33:22'),
(184, 179, 4, 2, 16000, 1, 0, 'rupiah', 16000, 'bayar-tunai', '2022-08-01 11:33:32', '2022-08-01 11:33:32'),
(185, 180, 18, 2, 25000, 1, 0, 'rupiah', 25000, 'bayar-tunai', '2022-08-01 11:33:58', '2022-08-01 11:33:58'),
(192, 187, 4, 4, 16000, 1, 0, 'rupiah', 16000, 'bayar-tunai', '2022-08-01 13:08:39', '2022-08-01 13:08:39'),
(193, 188, 4, 4, 16000, 1, 0, 'rupiah', 16000, 'bayar-tunai', '2022-08-01 13:08:42', '2022-08-01 13:08:42'),
(194, 189, 4, 2, 156000, 10, 0, 'rupiah', 156000, 'bayar-tunai', '2022-08-18 22:07:19', '2022-08-18 22:07:19'),
(195, 190, 4, 2, 16000, 1, 0, 'rupiah', 16000, 'bayar-tunai', '2022-08-18 22:11:27', '2022-08-18 22:11:27'),
(196, 191, 4, 4, 16000, 1, 0, 'rupiah', 16000, 'bayar-tunai', '2022-08-18 22:14:48', '2022-08-18 22:14:48'),
(197, 192, 4, 4, 16000, 1, 0, 'rupiah', 16000, 'bayar-tunai', '2022-08-18 22:14:48', '2022-08-18 22:14:48'),
(198, 193, 4, 2, 47000, 3, 0, 'rupiah', 47000, 'bayar-tunai', '2022-08-18 22:34:17', '2022-08-18 22:34:17'),
(199, 194, 4, 2, 47000, 3, 0, 'rupiah', 47000, 'bayar-tunai', '2022-08-18 22:34:17', '2022-08-18 22:34:17'),
(200, 195, 4, 2, 47000, 3, 0, 'rupiah', 47000, 'bayar-tunai', '2022-08-18 22:34:59', '2022-08-18 22:34:59'),
(201, 196, 4, 4, 47000, 3, 0, 'rupiah', 47000, 'bayar-tunai', '2022-08-18 22:35:16', '2022-08-18 22:35:16'),
(202, 197, 5, 4, 8000, 3, 0, 'rupiah', 8000, 'bayar-tunai', '2022-08-18 23:01:15', '2022-08-18 23:01:15'),
(203, 198, 5, 4, 8000, 3, 0, 'rupiah', 8000, 'bayar-tunai', '2022-08-18 23:01:15', '2022-08-18 23:01:15'),
(204, 199, 5, 4, 8000, 3, 0, 'rupiah', 8000, 'bayar-tunai', '2022-08-18 23:01:15', '2022-08-18 23:01:15'),
(205, 200, 4, 2, 47000, 3, 0, 'rupiah', 47000, 'bayar-tunai', '2022-08-18 23:06:02', '2022-08-18 23:06:02'),
(206, 201, 18, 2, 49000, 2, 0, 'rupiah', 49000, 'bayar-tunai', '2022-08-18 23:06:13', '2022-08-18 23:06:13'),
(207, 202, 18, 4, 49000, 2, 0, 'rupiah', 49000, 'bayar-tunai', '2022-08-18 23:06:28', '2022-08-18 23:06:28');

--
-- Triggers `transaksi_kasir_det`
--
DELIMITER $$
CREATE TRIGGER `kurang_stok_obat_kasir` AFTER INSERT ON `transaksi_kasir_det` FOR EACH ROW BEGIN
	IF NEW.tipe_transaksi = 'bayar-tunai' THEN
            UPDATE obat SET stok_obat = stok_obat - NEW.jumlah WHERE id_obat = NEW.id_obat;
    END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_racik_obat`
--

CREATE TABLE `transaksi_racik_obat` (
  `id_transaksi_racik_obat` int NOT NULL,
  `kode_transaksi` int NOT NULL,
  `id_racik_obat_data` int NOT NULL,
  `tanggal_transaksi` date NOT NULL,
  `jam_transaksi` time NOT NULL,
  `diskon` int NOT NULL,
  `harga_total` int NOT NULL,
  `bayar` int NOT NULL,
  `kembalian` int NOT NULL,
  `id_jam_shift` int NOT NULL,
  `id_users` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transaksi_racik_obat`
--

INSERT INTO `transaksi_racik_obat` (`id_transaksi_racik_obat`, `kode_transaksi`, `id_racik_obat_data`, `tanggal_transaksi`, `jam_transaksi`, `diskon`, `harga_total`, `bayar`, `kembalian`, `id_jam_shift`, `id_users`, `created_at`, `updated_at`) VALUES
(18, 0, 5, '2021-08-03', '00:00:00', 0, 200000, 200000, 0, 1, 1, '2021-08-03 02:36:50', '2021-08-03 02:36:50'),
(19, 0, 6, '2021-08-04', '00:00:00', 0, 1430000, 1430000, 0, 1, 1, '2021-08-04 18:39:32', '2021-08-04 18:39:32'),
(20, 1628533463, 7, '2021-08-10', '00:00:00', 0, 1363000, 1363000, 0, 1, 1, '2021-08-10 02:24:23', '2021-08-10 02:24:23'),
(21, 1628533512, 8, '2021-08-10', '00:00:00', 0, 1363000, 1363000, 0, 1, 1, '2021-08-10 02:25:12', '2021-08-10 02:25:12'),
(22, 1628749669, 9, '2021-08-12', '00:00:00', 0, 1363000, 1363000, 0, 1, 1, '2021-08-12 14:27:49', '2021-08-12 14:27:49'),
(23, 1628784152, 10, '2021-08-13', '00:00:00', 0, 52000, 52000, 0, 1, 1, '2021-08-13 00:02:32', '2021-08-13 00:02:32'),
(24, 1628784200, 11, '2021-08-13', '00:00:00', 0, 52000, 52000, 0, 1, 1, '2021-08-13 00:03:20', '2021-08-13 00:03:20'),
(25, 1628784211, 12, '2021-08-13', '00:00:00', 0, 52000, 52000, 0, 1, 1, '2021-08-13 00:03:31', '2021-08-13 00:03:31'),
(26, 1629006150, 13, '2021-08-15', '00:00:00', 0, 61000, 61000, 0, 1, 1, '2021-08-15 13:42:30', '2021-08-15 13:42:30'),
(27, 1629188606, 15, '2021-08-17', '00:00:00', 0, 40000, 40000, 0, 1, 1, '2021-08-17 16:23:26', '2021-08-17 16:23:26'),
(28, 1629214377, 16, '2021-08-17', '00:00:00', 0, 41000, 41000, 0, 1, 1, '2021-08-17 23:32:57', '2021-08-17 23:32:57'),
(29, 1629214482, 17, '2021-08-17', '00:00:00', 0, 41000, 41000, 0, 2, 1, '2021-08-17 23:34:42', '2021-08-17 23:34:42'),
(30, 1629214500, 18, '2021-08-17', '00:00:00', 0, 41000, 41000, 0, 2, 1, '2021-08-17 23:35:00', '2021-08-17 23:35:00'),
(31, 1629214508, 19, '2021-08-17', '00:00:00', 0, 41000, 41000, 0, 2, 1, '2021-08-17 23:35:08', '2021-08-17 23:35:08'),
(32, 1629264075, 20, '2021-08-18', '00:00:00', 0, 221000, 221000, 0, 2, 1, '2021-08-18 13:21:15', '2021-08-18 13:21:15'),
(33, 1629264249, 21, '2021-08-18', '00:00:00', 0, 40000, 40000, 0, 2, 1, '2021-08-18 13:24:09', '2021-08-18 13:24:09'),
(34, 1629356582, 23, '2021-08-19', '00:00:00', 0, 31000, 31000, 0, 1, 1, '2021-08-19 15:03:02', '2021-08-19 15:03:02'),
(35, 1629356918, 24, '2021-08-19', '00:00:00', 0, 30000, 30000, 0, 1, 1, '2021-08-19 15:08:38', '2021-08-19 15:08:38'),
(36, 1629539903, 28, '2021-08-21', '00:00:00', 0, 9000, 9000, 0, 2, 1, '2021-08-21 17:58:23', '2021-08-21 17:58:23'),
(37, 1629540607, 29, '2021-08-21', '00:00:00', 0, 3000, 3000, 0, 2, 1, '2021-08-21 18:10:07', '2021-08-21 18:10:07'),
(38, 1629787101, 30, '2021-08-24', '00:00:00', 0, 7000, 7000, 0, 1, 1, '2021-08-24 14:38:21', '2021-08-24 14:38:21'),
(39, 1629788664, 31, '2021-08-24', '00:00:00', 0, 10000, 10000, 0, 1, 1, '2021-08-24 15:04:24', '2021-08-24 15:04:24'),
(40, 1629792369, 32, '2021-08-24', '00:00:00', 0, 3000, 20000, 17000, 1, 1, '2021-08-24 16:06:09', '2021-08-24 16:06:09'),
(41, 1629868046, 33, '2021-08-25', '00:00:00', 0, 6000, 6000, 0, 1, 1, '2021-08-25 13:07:26', '2021-08-25 13:07:26'),
(42, 1629868285, 34, '2021-08-25', '00:00:00', 0, 6000, 6000, 0, 1, 1, '2021-08-25 13:11:25', '2021-08-25 13:11:25'),
(43, 1629868315, 35, '2021-08-25', '00:00:00', 0, 6000, 6000, 0, 1, 1, '2021-08-25 13:11:55', '2021-08-25 13:11:55'),
(44, 1629868395, 36, '2021-08-25', '00:00:00', 0, 6000, 6000, 0, 1, 1, '2021-08-25 13:13:15', '2021-08-25 13:13:15'),
(45, 1629868474, 37, '2021-08-25', '00:00:00', 0, 57000, 57000, 0, 1, 1, '2021-08-25 13:14:34', '2021-08-25 13:14:34'),
(46, 1629868635, 38, '2021-08-25', '00:00:00', 0, 8000, 8000, 0, 1, 1, '2021-08-25 13:17:15', '2021-08-25 13:17:15'),
(47, 1629868703, 39, '2021-08-25', '00:00:00', 0, 8000, 8000, 0, 1, 1, '2021-08-25 13:18:23', '2021-08-25 13:18:23'),
(48, 1629870794, 40, '2021-08-25', '00:00:00', 10, 2700, 3000, 300, 1, 1, '2021-08-25 13:53:14', '2021-08-25 13:53:14'),
(49, 1629953600, 41, '2021-08-26', '00:00:00', 0, 7000, 7000, 0, 1, 1, '2021-08-26 12:53:20', '2021-08-26 12:53:20'),
(50, 1629972463, 42, '2021-08-26', '00:00:00', 0, 28000, 28000, 0, 2, 1, '2021-08-26 18:07:43', '2021-08-26 18:07:43'),
(51, 1629973313, 44, '2021-08-26', '00:00:00', 0, 36000, 36000, 0, 2, 1, '2021-08-26 18:21:53', '2021-08-26 18:21:53'),
(52, 1629975028, 45, '2021-08-26', '00:00:00', 0, 3000, 3000, 0, 2, 1, '2021-08-26 18:50:28', '2021-08-26 18:50:28'),
(53, 1631256297, 47, '2021-09-10', '00:00:00', 50, 159000, 159000, 0, 1, 1, '2021-09-10 14:44:57', '2021-09-10 14:44:57'),
(54, 1631256804, 48, '2021-09-10', '00:00:00', 0, 35000, 35000, 0, 1, 1, '2021-09-10 14:53:24', '2021-09-10 14:53:24'),
(55, 1631606547, 49, '2021-09-14', '00:00:00', 0, 3000, 3000, 0, 1, 1, '2021-09-14 16:02:27', '2021-09-14 16:02:27'),
(56, 1632402403, 50, '2021-09-23', '00:00:00', 0, 4000, 4000, 0, 2, 1, '2021-09-23 21:06:43', '2021-09-23 21:06:43'),
(57, 1632980241, 51, '2021-09-30', '00:00:00', 0, 10000, 10000, 0, 1, 1, '2021-09-30 13:37:21', '2021-09-30 13:37:21'),
(58, 1634654813, 52, '2021-10-19', '00:00:00', 0, 7000, 7000, 0, 2, 1, '2021-10-19 22:46:53', '2021-10-19 22:46:53'),
(59, 1634722001, 57, '2021-10-20', '00:00:00', 0, 6000, 6000, 0, 2, 1, '2021-10-20 17:26:41', '2021-10-20 17:26:41'),
(60, 1634722357, 58, '2021-10-20', '00:00:00', 0, 6000, 6000, 0, 2, 1, '2021-10-20 17:32:37', '2021-10-20 17:32:37'),
(61, 1635420751, 59, '2021-10-28', '00:00:00', 0, 9000, 9000, 0, 2, 1, '2021-10-28 19:32:31', '2021-10-28 19:32:31'),
(62, 1635497483, 60, '2021-10-29', '00:00:00', 10, 4000, 4000, 0, 1, 1, '2021-10-29 16:51:23', '2021-10-29 16:51:23'),
(63, 1635497556, 61, '2021-10-29', '00:00:00', 10, 4000, 4000, 0, 1, 1, '2021-10-29 16:52:36', '2021-10-29 16:52:36'),
(64, 1635497577, 62, '2021-10-29', '00:00:00', 10, 4000, 4000, 0, 1, 1, '2021-10-29 16:52:57', '2021-10-29 16:52:57'),
(65, 1635498877, 63, '2021-10-29', '00:00:00', 10, 4000, 4000, 0, 2, 1, '2021-10-29 17:14:37', '2021-10-29 17:14:37'),
(66, 1635499159, 64, '2021-10-29', '00:00:00', 10, 4000, 4000, 0, 2, 1, '2021-10-29 17:19:19', '2021-10-29 17:19:19'),
(67, 1635499208, 65, '2021-10-29', '00:00:00', 10, 4000, 4000, 0, 2, 1, '2021-10-29 17:20:08', '2021-10-29 17:20:08'),
(68, 1635499214, 66, '2021-10-29', '00:00:00', 10, 4000, 4000, 0, 2, 1, '2021-10-29 17:20:14', '2021-10-29 17:20:14'),
(69, 1635499224, 67, '2021-10-29', '00:00:00', 10, 4000, 4000, 0, 2, 1, '2021-10-29 17:20:24', '2021-10-29 17:20:24'),
(70, 1635513813, 68, '2021-10-29', '00:00:00', 10, 9000, 9000, 0, 2, 1, '2021-10-29 21:23:34', '2021-10-29 21:23:34'),
(71, 1635513886, 69, '2021-10-29', '00:00:00', 10, 9000, 9000, 0, 2, 1, '2021-10-29 21:24:46', '2021-10-29 21:24:46'),
(72, 1635513972, 70, '2021-10-29', '00:00:00', 10, 9000, 9000, 0, 2, 1, '2021-10-29 21:26:12', '2021-10-29 21:26:12'),
(73, 1649433477, 71, '2022-04-08', '00:00:00', 10, 18000, 18000, 0, 5, 1, '2022-04-08 23:57:57', '2022-04-08 23:57:57'),
(74, 1649433669, 72, '2022-04-09', '00:00:00', 10, 18000, 18000, 0, 5, 1, '2022-04-09 00:01:09', '2022-04-09 00:01:09'),
(75, 1649433725, 73, '2022-04-09', '00:00:00', 10, 18000, 18000, 0, 5, 1, '2022-04-09 00:02:05', '2022-04-09 00:02:05'),
(76, 1649433735, 74, '2022-04-09', '00:00:00', 10, 18000, 18000, 0, 5, 1, '2022-04-09 00:02:15', '2022-04-09 00:02:15'),
(77, 1649433995, 75, '2022-04-09', '00:00:00', 10, 18000, 18000, 0, 5, 1, '2022-04-09 00:06:35', '2022-04-09 00:06:35'),
(78, 1649434095, 76, '2022-04-09', '00:00:00', 10, 18000, 18000, 0, 5, 1, '2022-04-09 00:08:15', '2022-04-09 00:08:15'),
(79, 1649434253, 77, '2022-04-09', '00:00:00', 10, 18000, 18000, 0, 5, 1, '2022-04-09 00:10:53', '2022-04-09 00:10:53'),
(80, 1649754163, 78, '2022-04-12', '00:00:00', 10, 12000, 15000, 3000, 2, 1, '2022-04-12 17:02:43', '2022-04-12 17:02:43'),
(81, 1649754364, 79, '2022-04-12', '00:00:00', 10, 12000, 15000, 3000, 2, 1, '2022-04-12 17:06:04', '2022-04-12 17:06:04'),
(82, 1649754394, 80, '2022-04-12', '00:00:00', 10, 12000, 15000, 3000, 2, 1, '2022-04-12 17:06:34', '2022-04-12 17:06:34'),
(83, 1649754409, 81, '2022-04-12', '00:00:00', 10, 12000, 15000, 3000, 2, 1, '2022-04-12 17:06:49', '2022-04-12 17:06:49'),
(84, 1649754626, 82, '2022-04-12', '00:00:00', 0, 34000, 35000, 1000, 2, 1, '2022-04-12 17:10:26', '2022-04-12 17:10:26'),
(85, 1649754909, 83, '2022-04-12', '00:00:00', 0, 34000, 35000, 1000, 2, 1, '2022-04-12 17:15:09', '2022-04-12 17:15:09'),
(86, 1649758419, 84, '2022-04-12', '00:00:00', 0, 8000, 10000, 2000, 2, 1, '2022-04-12 18:13:39', '2022-04-12 18:13:39'),
(87, 1649761430, 85, '2022-04-12', '00:00:00', 10, 36000, 36000, 0, 2, 1, '2022-04-12 19:03:50', '2022-04-12 19:03:50'),
(88, 1649761508, 86, '2022-04-12', '00:00:00', 10, 36000, 36000, 0, 2, 1, '2022-04-12 19:05:08', '2022-04-12 19:05:08'),
(89, 1649761644, 87, '2022-04-12', '00:00:00', 10, 36000, 36000, 0, 2, 1, '2022-04-12 19:07:24', '2022-04-12 19:07:24'),
(90, 1649761671, 88, '2022-04-12', '00:00:00', 10, 36000, 36000, 0, 2, 1, '2022-04-12 19:07:51', '2022-04-12 19:07:51'),
(91, 1649761693, 89, '2022-04-12', '00:00:00', 10, 36000, 36000, 0, 2, 1, '2022-04-12 19:08:13', '2022-04-12 19:08:13'),
(92, 1649761875, 90, '2022-04-12', '00:00:00', 10, 36000, 36000, 0, 2, 1, '2022-04-12 19:11:15', '2022-04-12 19:11:15'),
(93, 1649761942, 91, '2022-04-12', '00:00:00', 10, 36000, 36000, 0, 2, 1, '2022-04-12 19:12:22', '2022-04-12 19:12:22'),
(94, 220530001, 92, '2022-05-28', '00:00:00', 0, 423000, 425000, 2000, 5, 1, '2022-05-28 22:36:41', '2022-05-28 22:36:41'),
(95, 220630001, 93, '2022-06-02', '00:00:00', 10, 128000, 130000, 2000, 5, 1, '2022-06-02 00:10:34', '2022-06-02 00:10:34'),
(96, 220630002, 94, '2022-06-02', '00:00:00', 10, 128000, 130000, 2000, 5, 1, '2022-06-02 00:15:14', '2022-06-02 00:15:14'),
(97, 220630003, 95, '2022-06-02', '00:00:00', 10, 128000, 130000, 2000, 5, 1, '2022-06-02 00:15:15', '2022-06-02 00:15:15'),
(98, 220630004, 96, '2022-06-02', '00:00:00', 10, 128000, 130000, 2000, 5, 1, '2022-06-02 00:15:17', '2022-06-02 00:15:17'),
(99, 220630005, 97, '2022-06-02', '00:00:00', 10, 24000, 25000, 1000, 5, 1, '2022-06-02 00:16:07', '2022-06-02 00:16:07'),
(100, 220630006, 98, '2022-06-10', '00:00:00', 0, 35000, 35000, 0, 5, 1, '2022-06-10 00:25:34', '2022-06-10 00:25:34'),
(101, 220630007, 99, '2022-06-24', '00:00:00', 20, 113000, 141000, 28000, 2, 1, '2022-06-24 21:07:55', '2022-06-24 21:07:55'),
(102, 220630008, 100, '2022-06-24', '00:00:00', 20, 113000, 141000, 28000, 2, 1, '2022-06-24 21:09:11', '2022-06-24 21:09:11'),
(103, 220630009, 101, '2022-06-24', '00:00:00', 20, 113000, 141000, 28000, 2, 1, '2022-06-24 21:09:13', '2022-06-24 21:09:13'),
(104, 220630010, 102, '2022-06-24', '00:00:00', 10, 126000, 141000, 15000, 2, 1, '2022-06-24 21:11:49', '2022-06-24 21:11:49'),
(105, 220630011, 103, '2022-06-24', '00:00:00', 20, 17000, 17000, 0, 2, 1, '2022-06-24 21:12:45', '2022-06-24 21:12:45'),
(106, 220630012, 104, '2022-06-24', '00:00:00', 10, 198000, 200000, 2000, 2, 1, '2022-06-24 21:19:21', '2022-06-24 21:19:21'),
(107, 220630013, 105, '2022-06-25', '00:00:00', 10, 110000, 110000, 0, 1, 1, '2022-06-25 13:46:11', '2022-06-25 13:46:11'),
(108, 220630014, 106, '2022-06-25', '00:00:00', 10, 110000, 110000, 0, 1, 1, '2022-06-25 13:48:27', '2022-06-25 13:48:27'),
(109, 220630015, 107, '2022-06-25', '00:00:00', 10, 10000, 10000, 0, 1, 1, '2022-06-25 13:49:28', '2022-06-25 13:49:28'),
(110, 220630016, 108, '2022-06-25', '00:00:00', 10, 111000, 111000, 0, 1, 1, '2022-06-25 13:49:45', '2022-06-25 13:49:45'),
(111, 220630017, 109, '2022-06-27', '00:00:00', 0, 5000, 5000, 0, 1, 1, '2022-06-27 12:00:06', '2022-06-27 12:00:06'),
(112, 220730001, 110, '2022-07-11', '00:00:00', 2, 5000, 5000, 0, 1, 1, '2022-07-11 10:42:16', '2022-07-11 10:42:16'),
(113, 220730002, 111, '2022-07-11', '00:00:00', 0, 26000, 26000, 0, 1, 1, '2022-07-11 13:50:21', '2022-07-11 13:50:21'),
(114, 220730003, 112, '2022-07-22', '00:00:00', 20, 12000, 12000, 0, 2, 1, '2022-07-22 15:24:59', '2022-07-22 15:24:59'),
(115, 220730004, 113, '2022-07-22', '00:00:00', 20, 12000, 12000, 0, 2, 1, '2022-07-22 15:25:00', '2022-07-22 15:25:00'),
(116, 220730005, 114, '2022-07-22', '00:00:00', 20, 12000, 12000, 0, 2, 1, '2022-07-22 15:25:13', '2022-07-22 15:25:13'),
(117, 220730006, 115, '2022-07-22', '00:00:00', 20, 12000, 12000, 0, 2, 1, '2022-07-22 15:26:53', '2022-07-22 15:26:53'),
(118, 220730007, 117, '2022-07-22', '00:00:00', 10, 25000, 25000, 0, 2, 1, '2022-07-22 15:57:16', '2022-07-22 15:57:16'),
(119, 220730008, 118, '2022-07-22', '00:00:00', 10, 25000, 25000, 0, 2, 1, '2022-07-22 15:57:55', '2022-07-22 15:57:55'),
(120, 220730009, 119, '2022-07-22', '00:00:00', 10, 25000, 25000, 0, 2, 1, '2022-07-22 15:58:11', '2022-07-22 15:58:11'),
(121, 220730010, 120, '2022-07-22', '00:00:00', 10, 25000, 25000, 0, 2, 1, '2022-07-22 15:58:22', '2022-07-22 15:58:22'),
(122, 220730011, 121, '2022-07-22', '00:00:00', 10, 25000, 25000, 0, 2, 1, '2022-07-22 15:58:27', '2022-07-22 15:58:27'),
(123, 220730012, 122, '2022-07-22', '00:00:00', 10, 25000, 25000, 0, 2, 1, '2022-07-22 15:58:33', '2022-07-22 15:58:33'),
(124, 220730013, 123, '2022-07-22', '00:00:00', 10, 25000, 25000, 0, 2, 1, '2022-07-22 15:58:52', '2022-07-22 15:58:52'),
(125, 220730014, 124, '2022-07-23', '00:00:00', 0, 222000, 222000, 0, 5, 1, '2022-07-23 01:09:46', '2022-07-23 01:09:46'),
(126, 220730015, 125, '2022-07-23', '00:00:00', 0, 1909000, 2000000, 91000, 5, 1, '2022-07-23 01:25:50', '2022-07-23 01:25:50'),
(127, 220830001, 127, '2022-08-21', '00:00:00', 0, 58000, 60000, 2000, 2, 1, '2022-08-21 16:16:39', '2022-08-21 16:16:39'),
(128, 220830002, 128, '2022-08-21', '00:00:00', 0, 58000, 60000, 2000, 2, 1, '2022-08-21 16:17:24', '2022-08-21 16:17:24'),
(129, 220830003, 129, '2022-08-21', '00:00:00', 0, 211000, 211000, 0, 2, 1, '2022-08-21 16:18:26', '2022-08-21 16:18:26'),
(130, 220830004, 130, '2022-08-21', '00:00:00', 0, 70000, 80000, 10000, 2, 1, '2022-08-21 16:20:45', '2022-08-21 16:20:45'),
(131, 220830005, 131, '2022-08-21', '00:00:00', 0, 70000, 80000, 10000, 2, 1, '2022-08-21 16:21:56', '2022-08-21 16:21:56'),
(132, 220830006, 134, '2022-08-21', '00:00:00', 0, 225000, 225000, 0, 2, 1, '2022-08-21 16:24:41', '2022-08-21 16:24:41'),
(133, 220830007, 135, '2022-08-21', '00:00:00', 0, 99000, 100000, 1000, 2, 1, '2022-08-21 16:32:20', '2022-08-21 16:32:20'),
(134, 220830008, 136, '2022-08-21', '00:00:00', 0, 90000, 90000, 0, 2, 1, '2022-08-21 16:42:22', '2022-08-21 16:42:22');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_users` int NOT NULL,
  `name` varchar(70) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(70) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `level_user` int NOT NULL COMMENT '2=admin;1=kasir;',
  `active` int NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_delete` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_users`, `name`, `username`, `password`, `level_user`, `active`, `remember_token`, `status_delete`) VALUES
(1, 'Administrator', 'admin', '$2y$10$nmgt9buVP9DBFPJ4D6E7POFNmhoW6Wj7gBhtQCthkJnx4wMYmZ/5K', 2, 1, 'z7cEA9GLCENuUVuFccLcVgGG8obYzo2tW46UiLsnhvq77bvmZi91pllxyST6', 0),
(4, 'kasir depan', 'kasir', '$2y$10$nQ4q5k8ehbfkMPfIY3uIUeiAG1SLW4cvUQWKlfAuK/.thS7FUfyj.', 1, 1, 'Vsbcn4nql83Noq6j5jbuiJoUCEx7JhtwP4sIgFaVYFzEBJCzbpEwKToP5LWC', 0),
(9, 'Ujang', 'ujang', '$2y$10$3mt7ovFVPgHdQuBq.H.Pg.ANG7bSjKzCp7m/j2oJiZ5bBcHyzjEC6', 1, 1, 'PyeWFXi1aAaNGEMQsm8vMfkJVO8UJcGt9ige0a9S2PEsvkucB7rDTRgYjSSR', 0),
(10, 'Mamat', 'mamat', '$2y$10$b4OZPXqzZvoiQhcMf6VvM.U2wYW3vkZx8/uUGaMqM.1lHKHs360QK', 1, 1, NULL, 1),
(12, 'Cob', 'coba', '$2y$10$YZwGYNrF9XjBVPJiefXF1.Ex7p2zfdgo1sYgfQBbHbvg8kPVMretC', 1, 0, 'kNOZ9VCigmQMXSvGoZ0I0E31HdW3uuqjET0KKcyDiC8IwB0s8Y3iLQGoACLk', 1),
(13, 'Coba', 'kasir_cuy', '$2y$10$RIms8hz7u0Gb64CStZITBu9lzz4Lb/cMktlZB5EkHShj7MJmR6/X2', 1, 1, 'F5ypka7EJ1NLXGDOM9gxLb9bk1hSqhpZFj5CczjkJX2PqwCSCoqUgWVxMCmo', 1),
(15, 'Udin', 'udin', '$2y$10$mG9oTanenkHom8JDblzlf.4T.XHJ/M8qdKyoNf0OmMdznvkg0gHM6', 1, 1, 'TZas4zjLG1zjCv9EVs48eDNdERtYzjqH0HoqDumASeWS9AIVDN4dTKSfBtO2', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dokter`
--
ALTER TABLE `dokter`
  ADD PRIMARY KEY (`id_dokter`);

--
-- Indexes for table `golongan_obat`
--
ALTER TABLE `golongan_obat`
  ADD PRIMARY KEY (`id_golongan_obat`);

--
-- Indexes for table `jam_shift`
--
ALTER TABLE `jam_shift`
  ADD PRIMARY KEY (`id_jam_shift`);

--
-- Indexes for table `jenis_obat`
--
ALTER TABLE `jenis_obat`
  ADD PRIMARY KEY (`id_jenis_obat`);

--
-- Indexes for table `kartu_stok`
--
ALTER TABLE `kartu_stok`
  ADD PRIMARY KEY (`id_kartu_stok`),
  ADD KEY `id_obat` (`id_obat`);

--
-- Indexes for table `komposisi_obat`
--
ALTER TABLE `komposisi_obat`
  ADD PRIMARY KEY (`id_komposisi_obat`),
  ADD KEY `komposisi_obat_ibfk_1` (`id_obat`);

--
-- Indexes for table `kredit`
--
ALTER TABLE `kredit`
  ADD PRIMARY KEY (`id_kredit`);

--
-- Indexes for table `kredit_det`
--
ALTER TABLE `kredit_det`
  ADD PRIMARY KEY (`id_kredit_det`),
  ADD KEY `id_kredit` (`id_kredit_faktur`),
  ADD KEY `id_obat` (`id_obat`),
  ADD KEY `id_supplier` (`id_supplier`);

--
-- Indexes for table `kredit_faktur`
--
ALTER TABLE `kredit_faktur`
  ADD PRIMARY KEY (`id_kredit_faktur`),
  ADD KEY `id_kredit` (`id_kredit`),
  ADD KEY `id_jam_shift` (`id_jam_shift`),
  ADD KEY `id_users` (`id_users`);

--
-- Indexes for table `margin_obat`
--
ALTER TABLE `margin_obat`
  ADD PRIMARY KEY (`id_margin_obat`);

--
-- Indexes for table `menu_user`
--
ALTER TABLE `menu_user`
  ADD PRIMARY KEY (`id_menu_user`),
  ADD KEY `id_users` (`id_users`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `obat`
--
ALTER TABLE `obat`
  ADD PRIMARY KEY (`id_obat`),
  ADD KEY `id_jenis_obat` (`id_jenis_obat`),
  ADD KEY `obat_ibfk_2` (`id_golongan_obat`),
  ADD KEY `obat_ibfk_3` (`id_pabrik_obat`);

--
-- Indexes for table `obat_detail`
--
ALTER TABLE `obat_detail`
  ADD PRIMARY KEY (`id_obat_detail`),
  ADD KEY `id_obat` (`id_obat`),
  ADD KEY `obat_detail_ibfk_1` (`id_supplier`);

--
-- Indexes for table `pabrik_obat`
--
ALTER TABLE `pabrik_obat`
  ADD PRIMARY KEY (`id_pabrik_obat`);

--
-- Indexes for table `pasien`
--
ALTER TABLE `pasien`
  ADD PRIMARY KEY (`id_pasien`);

--
-- Indexes for table `pemakaian_obat`
--
ALTER TABLE `pemakaian_obat`
  ADD PRIMARY KEY (`id_pemakaian`),
  ADD KEY `pemakaian_obat_ibfk_1` (`id_dokter`),
  ADD KEY `pemakaian_obat_ibfk_2` (`id_supplier`),
  ADD KEY `pemakaian_obat_ibfk_3` (`id_obat`);

--
-- Indexes for table `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  ADD PRIMARY KEY (`id_pembelian_detail`),
  ADD KEY `id_obat` (`id_obat`),
  ADD KEY `id_pembelian_obat` (`id_pembelian_obat`);

--
-- Indexes for table `pembelian_obat`
--
ALTER TABLE `pembelian_obat`
  ADD PRIMARY KEY (`id_pembelian_obat`),
  ADD KEY `id_supplier` (`id_supplier`),
  ADD KEY `id_users` (`id_users`);

--
-- Indexes for table `persen_ppn`
--
ALTER TABLE `persen_ppn`
  ADD PRIMARY KEY (`id_persen_ppn`);

--
-- Indexes for table `profile_instansi`
--
ALTER TABLE `profile_instansi`
  ADD PRIMARY KEY (`id_profile_instansi`);

--
-- Indexes for table `racik_obat`
--
ALTER TABLE `racik_obat`
  ADD PRIMARY KEY (`id_racik_obat`),
  ADD KEY `id_racik_obat_data` (`id_racik_obat_data`);

--
-- Indexes for table `racik_obat_data`
--
ALTER TABLE `racik_obat_data`
  ADD PRIMARY KEY (`id_racik_obat_data`),
  ADD KEY `id_pasien` (`id_pasien`),
  ADD KEY `id_dokter` (`id_dokter`);

--
-- Indexes for table `racik_obat_detail`
--
ALTER TABLE `racik_obat_detail`
  ADD PRIMARY KEY (`id_racik_obat_detail`),
  ADD KEY `id_racik_obat` (`id_racik_obat`),
  ADD KEY `id_obat` (`id_obat`),
  ADD KEY `racik_obat_detail_ibfk_3` (`id_supplier`);

--
-- Indexes for table `racik_obat_sementara`
--
ALTER TABLE `racik_obat_sementara`
  ADD PRIMARY KEY (`id_racik_obat_sementara`);

--
-- Indexes for table `racik_obat_sementara_detail`
--
ALTER TABLE `racik_obat_sementara_detail`
  ADD PRIMARY KEY (`id_racik_obat_sementara_detail`),
  ADD KEY `id_racik_obat_sementara` (`id_racik_obat_sementara`);

--
-- Indexes for table `retur_barang`
--
ALTER TABLE `retur_barang`
  ADD PRIMARY KEY (`id_retur_barang`);

--
-- Indexes for table `retur_barang_detail`
--
ALTER TABLE `retur_barang_detail`
  ADD PRIMARY KEY (`id_retur_barang_detail`),
  ADD KEY `id_obat` (`id_obat`),
  ADD KEY `id_retur_barang` (`id_retur_barang`);

--
-- Indexes for table `stok_opnem`
--
ALTER TABLE `stok_opnem`
  ADD PRIMARY KEY (`id_stok_opnem`);

--
-- Indexes for table `stok_opnem_detail`
--
ALTER TABLE `stok_opnem_detail`
  ADD PRIMARY KEY (`id_stok_opnem_detail`),
  ADD KEY `id_stok_opnem` (`id_stok_opnem`),
  ADD KEY `id_obat` (`id_obat`);

--
-- Indexes for table `supplier_obat`
--
ALTER TABLE `supplier_obat`
  ADD PRIMARY KEY (`id_supplier`);

--
-- Indexes for table `transaksi_kasir`
--
ALTER TABLE `transaksi_kasir`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `id_user` (`id_users`),
  ADD KEY `id_jam_shift` (`id_jam_shift`);

--
-- Indexes for table `transaksi_kasir_det`
--
ALTER TABLE `transaksi_kasir_det`
  ADD PRIMARY KEY (`id_transaksi_det`),
  ADD KEY `id_kasir` (`id_transaksi`),
  ADD KEY `id_obat` (`id_obat`),
  ADD KEY `transaksi_kasir_det_ibfk_1` (`id_supplier`);

--
-- Indexes for table `transaksi_racik_obat`
--
ALTER TABLE `transaksi_racik_obat`
  ADD PRIMARY KEY (`id_transaksi_racik_obat`),
  ADD KEY `transaksi_racik_obat_ibfk_1` (`id_racik_obat_data`),
  ADD KEY `id_jam_shift` (`id_jam_shift`),
  ADD KEY `id_users` (`id_users`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_users`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dokter`
--
ALTER TABLE `dokter`
  MODIFY `id_dokter` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `golongan_obat`
--
ALTER TABLE `golongan_obat`
  MODIFY `id_golongan_obat` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `jam_shift`
--
ALTER TABLE `jam_shift`
  MODIFY `id_jam_shift` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `jenis_obat`
--
ALTER TABLE `jenis_obat`
  MODIFY `id_jenis_obat` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `kartu_stok`
--
ALTER TABLE `kartu_stok`
  MODIFY `id_kartu_stok` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=291;

--
-- AUTO_INCREMENT for table `komposisi_obat`
--
ALTER TABLE `komposisi_obat`
  MODIFY `id_komposisi_obat` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `kredit`
--
ALTER TABLE `kredit`
  MODIFY `id_kredit` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `kredit_det`
--
ALTER TABLE `kredit_det`
  MODIFY `id_kredit_det` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `kredit_faktur`
--
ALTER TABLE `kredit_faktur`
  MODIFY `id_kredit_faktur` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `margin_obat`
--
ALTER TABLE `margin_obat`
  MODIFY `id_margin_obat` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_user`
--
ALTER TABLE `menu_user`
  MODIFY `id_menu_user` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `obat`
--
ALTER TABLE `obat`
  MODIFY `id_obat` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `obat_detail`
--
ALTER TABLE `obat_detail`
  MODIFY `id_obat_detail` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT for table `pabrik_obat`
--
ALTER TABLE `pabrik_obat`
  MODIFY `id_pabrik_obat` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pasien`
--
ALTER TABLE `pasien`
  MODIFY `id_pasien` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `pemakaian_obat`
--
ALTER TABLE `pemakaian_obat`
  MODIFY `id_pemakaian` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=459;

--
-- AUTO_INCREMENT for table `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  MODIFY `id_pembelian_detail` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT for table `pembelian_obat`
--
ALTER TABLE `pembelian_obat`
  MODIFY `id_pembelian_obat` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `persen_ppn`
--
ALTER TABLE `persen_ppn`
  MODIFY `id_persen_ppn` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `profile_instansi`
--
ALTER TABLE `profile_instansi`
  MODIFY `id_profile_instansi` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `racik_obat`
--
ALTER TABLE `racik_obat`
  MODIFY `id_racik_obat` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=212;

--
-- AUTO_INCREMENT for table `racik_obat_data`
--
ALTER TABLE `racik_obat_data`
  MODIFY `id_racik_obat_data` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;

--
-- AUTO_INCREMENT for table `racik_obat_detail`
--
ALTER TABLE `racik_obat_detail`
  MODIFY `id_racik_obat_detail` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=266;

--
-- AUTO_INCREMENT for table `racik_obat_sementara`
--
ALTER TABLE `racik_obat_sementara`
  MODIFY `id_racik_obat_sementara` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `racik_obat_sementara_detail`
--
ALTER TABLE `racik_obat_sementara_detail`
  MODIFY `id_racik_obat_sementara_detail` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `retur_barang`
--
ALTER TABLE `retur_barang`
  MODIFY `id_retur_barang` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `retur_barang_detail`
--
ALTER TABLE `retur_barang_detail`
  MODIFY `id_retur_barang_detail` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `stok_opnem`
--
ALTER TABLE `stok_opnem`
  MODIFY `id_stok_opnem` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `stok_opnem_detail`
--
ALTER TABLE `stok_opnem_detail`
  MODIFY `id_stok_opnem_detail` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=221;

--
-- AUTO_INCREMENT for table `supplier_obat`
--
ALTER TABLE `supplier_obat`
  MODIFY `id_supplier` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `transaksi_kasir`
--
ALTER TABLE `transaksi_kasir`
  MODIFY `id_transaksi` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=203;

--
-- AUTO_INCREMENT for table `transaksi_kasir_det`
--
ALTER TABLE `transaksi_kasir_det`
  MODIFY `id_transaksi_det` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=208;

--
-- AUTO_INCREMENT for table `transaksi_racik_obat`
--
ALTER TABLE `transaksi_racik_obat`
  MODIFY `id_transaksi_racik_obat` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_users` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `kartu_stok`
--
ALTER TABLE `kartu_stok`
  ADD CONSTRAINT `kartu_stok_ibfk_1` FOREIGN KEY (`id_obat`) REFERENCES `obat` (`id_obat`) ON DELETE RESTRICT ON UPDATE CASCADE;

--
-- Constraints for table `komposisi_obat`
--
ALTER TABLE `komposisi_obat`
  ADD CONSTRAINT `komposisi_obat_ibfk_1` FOREIGN KEY (`id_obat`) REFERENCES `obat` (`id_obat`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `kredit_det`
--
ALTER TABLE `kredit_det`
  ADD CONSTRAINT `kredit_det_ibfk_1` FOREIGN KEY (`id_kredit_faktur`) REFERENCES `kredit_faktur` (`id_kredit_faktur`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kredit_det_ibfk_2` FOREIGN KEY (`id_obat`) REFERENCES `obat` (`id_obat`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kredit_det_ibfk_3` FOREIGN KEY (`id_supplier`) REFERENCES `supplier_obat` (`id_supplier`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `kredit_faktur`
--
ALTER TABLE `kredit_faktur`
  ADD CONSTRAINT `kredit_faktur_ibfk_1` FOREIGN KEY (`id_kredit`) REFERENCES `kredit` (`id_kredit`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kredit_faktur_ibfk_2` FOREIGN KEY (`id_jam_shift`) REFERENCES `jam_shift` (`id_jam_shift`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `kredit_faktur_ibfk_3` FOREIGN KEY (`id_users`) REFERENCES `users` (`id_users`) ON DELETE RESTRICT ON UPDATE CASCADE;

--
-- Constraints for table `menu_user`
--
ALTER TABLE `menu_user`
  ADD CONSTRAINT `menu_user_ibfk_1` FOREIGN KEY (`id_users`) REFERENCES `users` (`id_users`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `obat`
--
ALTER TABLE `obat`
  ADD CONSTRAINT `obat_ibfk_1` FOREIGN KEY (`id_jenis_obat`) REFERENCES `jenis_obat` (`id_jenis_obat`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `obat_ibfk_2` FOREIGN KEY (`id_golongan_obat`) REFERENCES `golongan_obat` (`id_golongan_obat`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `obat_ibfk_3` FOREIGN KEY (`id_pabrik_obat`) REFERENCES `pabrik_obat` (`id_pabrik_obat`) ON DELETE RESTRICT ON UPDATE CASCADE;

--
-- Constraints for table `obat_detail`
--
ALTER TABLE `obat_detail`
  ADD CONSTRAINT `obat_detail_ibfk_1` FOREIGN KEY (`id_supplier`) REFERENCES `supplier_obat` (`id_supplier`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `obat_detail_ibfk_2` FOREIGN KEY (`id_obat`) REFERENCES `obat` (`id_obat`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pemakaian_obat`
--
ALTER TABLE `pemakaian_obat`
  ADD CONSTRAINT `pemakaian_obat_ibfk_1` FOREIGN KEY (`id_dokter`) REFERENCES `dokter` (`id_dokter`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `pemakaian_obat_ibfk_2` FOREIGN KEY (`id_supplier`) REFERENCES `supplier_obat` (`id_supplier`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `pemakaian_obat_ibfk_3` FOREIGN KEY (`id_obat`) REFERENCES `obat` (`id_obat`) ON DELETE RESTRICT ON UPDATE CASCADE;

--
-- Constraints for table `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  ADD CONSTRAINT `pembelian_detail_ibfk_1` FOREIGN KEY (`id_obat`) REFERENCES `obat` (`id_obat`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `pembelian_detail_ibfk_3` FOREIGN KEY (`id_pembelian_obat`) REFERENCES `pembelian_obat` (`id_pembelian_obat`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pembelian_obat`
--
ALTER TABLE `pembelian_obat`
  ADD CONSTRAINT `pembelian_obat_ibfk_1` FOREIGN KEY (`id_supplier`) REFERENCES `supplier_obat` (`id_supplier`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `pembelian_obat_ibfk_2` FOREIGN KEY (`id_users`) REFERENCES `users` (`id_users`) ON DELETE RESTRICT ON UPDATE CASCADE;

--
-- Constraints for table `racik_obat`
--
ALTER TABLE `racik_obat`
  ADD CONSTRAINT `racik_obat_ibfk_1` FOREIGN KEY (`id_racik_obat_data`) REFERENCES `racik_obat_data` (`id_racik_obat_data`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `racik_obat_data`
--
ALTER TABLE `racik_obat_data`
  ADD CONSTRAINT `racik_obat_data_ibfk_1` FOREIGN KEY (`id_pasien`) REFERENCES `pasien` (`id_pasien`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `racik_obat_data_ibfk_2` FOREIGN KEY (`id_dokter`) REFERENCES `dokter` (`id_dokter`) ON DELETE RESTRICT ON UPDATE CASCADE;

--
-- Constraints for table `racik_obat_detail`
--
ALTER TABLE `racik_obat_detail`
  ADD CONSTRAINT `racik_obat_detail_ibfk_1` FOREIGN KEY (`id_racik_obat`) REFERENCES `racik_obat` (`id_racik_obat`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `racik_obat_detail_ibfk_2` FOREIGN KEY (`id_obat`) REFERENCES `obat` (`id_obat`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `racik_obat_detail_ibfk_3` FOREIGN KEY (`id_supplier`) REFERENCES `supplier_obat` (`id_supplier`) ON DELETE RESTRICT ON UPDATE CASCADE;

--
-- Constraints for table `racik_obat_sementara_detail`
--
ALTER TABLE `racik_obat_sementara_detail`
  ADD CONSTRAINT `racik_obat_sementara_detail_ibfk_1` FOREIGN KEY (`id_racik_obat_sementara`) REFERENCES `racik_obat_sementara` (`id_racik_obat_sementara`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `retur_barang_detail`
--
ALTER TABLE `retur_barang_detail`
  ADD CONSTRAINT `retur_barang_detail_ibfk_1` FOREIGN KEY (`id_obat`) REFERENCES `obat` (`id_obat`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `retur_barang_detail_ibfk_2` FOREIGN KEY (`id_retur_barang`) REFERENCES `retur_barang` (`id_retur_barang`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `stok_opnem_detail`
--
ALTER TABLE `stok_opnem_detail`
  ADD CONSTRAINT `stok_opnem_detail_ibfk_1` FOREIGN KEY (`id_stok_opnem`) REFERENCES `stok_opnem` (`id_stok_opnem`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `stok_opnem_detail_ibfk_2` FOREIGN KEY (`id_obat`) REFERENCES `obat` (`id_obat`) ON DELETE RESTRICT ON UPDATE CASCADE;

--
-- Constraints for table `transaksi_kasir`
--
ALTER TABLE `transaksi_kasir`
  ADD CONSTRAINT `id_users` FOREIGN KEY (`id_users`) REFERENCES `users` (`id_users`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `transaksi_kasir_ibfk_1` FOREIGN KEY (`id_jam_shift`) REFERENCES `jam_shift` (`id_jam_shift`) ON DELETE RESTRICT ON UPDATE CASCADE;

--
-- Constraints for table `transaksi_kasir_det`
--
ALTER TABLE `transaksi_kasir_det`
  ADD CONSTRAINT `id_obat` FOREIGN KEY (`id_obat`) REFERENCES `obat` (`id_obat`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `id_transaksi` FOREIGN KEY (`id_transaksi`) REFERENCES `transaksi_kasir` (`id_transaksi`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transaksi_kasir_det_ibfk_1` FOREIGN KEY (`id_supplier`) REFERENCES `supplier_obat` (`id_supplier`) ON DELETE RESTRICT ON UPDATE CASCADE;

--
-- Constraints for table `transaksi_racik_obat`
--
ALTER TABLE `transaksi_racik_obat`
  ADD CONSTRAINT `transaksi_racik_obat_ibfk_1` FOREIGN KEY (`id_racik_obat_data`) REFERENCES `racik_obat_data` (`id_racik_obat_data`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transaksi_racik_obat_ibfk_2` FOREIGN KEY (`id_jam_shift`) REFERENCES `jam_shift` (`id_jam_shift`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `transaksi_racik_obat_ibfk_3` FOREIGN KEY (`id_users`) REFERENCES `users` (`id_users`) ON DELETE RESTRICT ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
